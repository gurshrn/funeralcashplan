(function(jQuery) {
  'use strict';
  
	jQuery(document).ready(function(){
    
		// Wow Js
		var wow = new WOW({
			mobile: false // default
		})
		wow.init();

		// Aos Js
		AOS.init({
			easing: 'ease-in-out-sine'
		});

		
		//News Slider

		var newsslider = jQuery('body').find('.news-slider');
		newsslider.owlCarousel({
			loop:false,
			margin: 30,
			nav: false,
			autoPlay : true,
			dots: true,
			navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsive:{
				1000:{
					items:3,
				},
				0:{
					items:1
				},
				600:{
					items:3,
				}
			}
		});

		jQuery(".header_top ul li .search").click(function(){
			jQuery(".search-form").toggle();
		});

		// Upload Image
    
		function readsURL(input) { 
			console.log(input.files["0"].name);
			if (input.files && input.files[0]) {
			var uimage = new FileReader();
				uimage.readAsDataURL(input.files[0]);
				 jQuery(".upload-image #uploadFile").val(input.files["0"].name);
			}
		}
		jQuery('body').find(".upload-image input[type='file']").on('change',function(){
			readsURL(this);
		});

		//Header
		jQuery(window).on("load resize scroll", function(e) {
			var Win = jQuery(window).height();
			var Header = jQuery("header").height();
			var Footer = jQuery("footer").height();
			var NHF = Header + Footer;
			jQuery('.main').css('min-height', (Win - NHF));
		});

		//Textarea 
		jQuery('.firstCap, textarea').on('keypress', function(event) {
			var jQuerythis = jQuery(this),
			thisVal = jQuerythis.val(),
			FLC = thisVal.slice(0, 1).toUpperCase(),
			con = thisVal.slice(1, thisVal.length);
			jQuery(this).val(FLC + con);
		});

		//Sticky Header
		jQuery(window).scroll(function () {
			var scroll = jQuery(window).scrollTop();
			if (scroll >= 200) {
				jQuery(".header-bottom").addClass("fixed-header");
			} else {
				jQuery(".header-bottom").removeClass("fixed-header");
			}
		});

		//Model
		jQuery(function() {
			//----- OPEN
			jQuery('[data-popup-open]').on('click', function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-open');
			jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);
		
			e.preventDefault();
			});
		
			//----- CLOSE
			jQuery(document).on('click','[data-popup-close]','.cstmClose',function(e) {
			var targeted_popup_class = jQuery(this).attr('data-popup-close');
			jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);
		
			e.preventDefault();
			});
		});

		//Filters
		jQuery(".flipalease-list .filter > a").click(function(){
			jQuery(".flipalease-list .filter").toggleClass("active");
		});
		

	});
  
	//Page Zoom
	document.documentElement.addEventListener('touchstart', function (event) {
		if (event.touches.length > 1) {
			event.preventDefault();
		}
	}, false);

	//Avoid pinch zoom on iOS
	document.addEventListener('touchmove', function(event) {
		if (event.scale !== 1) {
			event.preventDefault();
		}
	}, false);
	
})(jQuery)


jQuery(document).ready(function(){
	// Range Slider
		
	jQuery('#ex2').slider();
			

	//Country Code

	var input = document.querySelector("#telephone");
		window.intlTelInput(input,({
		// options here
	}));
	var input = document.querySelector("#telephone1");
		window.intlTelInput(input,({
		// options here
	}));
	var input = document.querySelector("#telephone2");
		window.intlTelInput(input,({
		// options here
	}));
	var input = document.querySelector("#telephone3");
		window.intlTelInput(input,({
		// options here
	}));

});
