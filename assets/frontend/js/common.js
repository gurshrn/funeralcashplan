/*========================trim space validation=======================*/

	jQuery('body').on('keyup blur', 'input[type = "number"],input[type = "text"],input[type = "email"],input[type = "password"]', function (eve) 
	{
		if ((eve.which != 37) && (eve.which != 38) && (eve.which != 39) && (eve.which != 40)) 
		{
			var text = jQuery(this).val();
			text = text.trim();
			if (text == '') {
				jQuery(this).val('');
			}
			var string = jQuery(this).val();
			if (string != "") {
				string = string.replace(/\s+/g, " ");
				jQuery(this).val(string);
			}
		}
	});
	
/*====================only Alphabet validation====================*/

	jQuery(document).on('keypress', '.alphabets', function (event) 
	{
		var inputValue = event.charCode;
		if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) 
		{
			event.preventDefault();
		}
	});
	
/*================only Numeric validation with tab working====================*/

	jQuery(document).on('keypress', '.onlyvalidNumber', function (eve) 
	{
		if (eve.which == 0) 
		{
			return true;
		}
		else
		{
			if ((eve.which != 46 || $(this).val().indexOf('.') == -1) && (eve.which < 48 || eve.which > 57))
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});

/*===========Check email valid or not============*/

	jQuery(document).on('change','.email',function(){
        var email = $(this).val();
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(email);
        if (!valid && email != '') 
        {
            toastr.error('Invalid email');
            jQuery('.submit-button').prop('disabled',true);
            return false;
        }
        else
        {
			jQuery('.submit-button').prop('disabled',false);
            return true;
        }
    });

/*=============combo datepicker for register page========*/

	jQuery('#date').combodate({
        firstItem: 'name',
        maxYear: new Date().getFullYear(),
    }); 

	var homePhone = document.querySelector(".rPhone");
	window.intlTelInput(homePhone);

	var mobilePhone = document.querySelector(".rMphone");
	window.intlTelInput(mobilePhone);

	
	
    

       

	