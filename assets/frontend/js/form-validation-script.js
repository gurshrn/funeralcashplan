jQuery(document).ready(function(){

/*===================common function for submit form====================*/

	function submitHandler (form) 
	{
		jQuery(form).find('button[type=submit]').attr('disabled',true);
		jQuery(form).find('button[type=submit]').html('Processing...');
		$.ajax({
			url: form.action,
			type: form.method,
			data: new FormData(form),
			contentType: false,       
			cache: false,             
			processData:false, 
			dataType: "json",
			success: function(response) 
			{
				jQuery(form).find('button[type=submit]').removeAttr('disabled');
				jQuery(form).find('button[type=submit]').html('Submit');
				if(response.success == true)
				{
					jQuery("html, body").animate({ scrollTop: 0 }, "slow");
					toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					setTimeout(
						function() 
						{
							if(response.url)
							{
								window.location = response.url;
							}
						}, 2000
					);
				}
				if(response.success == false)
				{
					toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				}
			}
		});
	}

/*======================validation for select country in home page===================*/

	jQuery("#select_country").validate({
		rules:
		{
			country_origin: 
			{
				required: true,
			},
			country_residence: 
			{
				required: true,
			},
			insurance_plan: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			console.log('dsa');
		}
	});

/*======================validation for login in home page===================*/

	jQuery("#frontend_login").validate({
		rules:
		{
			email: 
			{
				required: true,
			},
			password: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin login===================*/

	jQuery("#admin_login").validate({
		rules:
		{
			username: 
			{
				required: true,
			},
			password: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin login===================*/

	jQuery("#add_new_applicant").validate({
		rules:
		{
			select_family_member: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		}
	});

/*======================validation for add family member===================*/

	jQuery("#add_family_member").validate({
		rules:
		{
			title: 
			{
				required: true,
			},
			select_family_member:
			{
				required: true,
			},
			fname: 
			{
				required: true,
			},
			sname: 
			{
				required: true,
			},
			date: 
			{
				required: true,
			},
			month: 
			{
				required: true,
			},
			year: 
			{
				required: true,
			},
			relationship: 
			{
				required: true,
			},
			gender: 
			{
				required: true,
			},
			country_of_residency: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for register form in home page===================*/

	jQuery("#register_user").validate({
		rules:
		{
			country_of_origin:
			{
				required: true,
			},
			country_of_residence:
			{
				required: true,
			},
			insurance_plan:
			{
				required: true,
			},
			title: 
			{
				required: true,
			},
			fname: 
			{
				required: true,
			},
			lname: 
			{
				required: true,
			},
			gender:
			{
				required:true,
			},
			mphone:
			{
				required:true,
			},
			email:
			{
				required: true,
			},
			confirm_email:
			{
				required: true,
				equalTo : "#new_email"
			},
			password:
			{
				required: true,
			},
			confirm_password: 
			{
				required: true,
				equalTo : "#new_password"
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for register form in home page===================*/

	jQuery("#user_quote").validate({
		rules:
		{
			country_of_residency:
			{
				required: true,
			},
			general_cash_cover:
			{
				required: true,
			},
			title: 
			{
				required: true,
			},
			fname: 
			{
				required: true,
			},
			sname: 
			{
				required: true,
			},
			mphone:
			{
				required:true,
			},
			plan_type:
			{
				required: true,
			},
			date:
			{
				required: true,
			},
			month:
			{
				required: true,
			},
			year:
			{
				required: true,
			},

			
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for career form in home page===================*/

	jQuery("#career_form").validate({
		rules:
		{
			fname:
			{
				required: true,
			},
			lname:
			{
				required: true,
			},
			email:
			{
				required: true,
			},
			position: 
			{
				required: true,
			},
			mnumber: 
			{
				required: true,
			},
			country_code: 
			{
				required: true,
			},
			gender:
			{
				required:true,
			},
			message:
			{
				required:true,
			},
			resume:
			{
				required: true,
				extension: "docx|doc|pdf"
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin career page===================*/

	jQuery("#add_career_page_detail").validate({
		rules:
		{
			banner_text:
			{
				required: true,
			},
			banner_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			main_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			block1_title: 
			{
				required: true,
			},
			block1_description: 
			{
				required: true,
			},
			block2_title: 
			{
				required: true,
			},
			career_form_title:
			{
				required:true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for event invite us===================*/

	jQuery("#event_invite_us_form").validate({
		rules:
		{
			event_name:
			{
				required: true,
			},
			event_date:
			{
				required: true,
			},
			venue_address:
			{
				required: true,
			},
			event_organiser: 
			{
				required: true,
			},
			venue_capacity: 
			{
				required: true,
			},
			expected_attendance: 
			{
				required: true,
			},
			email:
			{
				required:true,
			},
			country_code_mobile:
			{
				required:true,
			},
			event_flyer:
			{
				required:true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin event invite us===================*/
	
	jQuery("#add_invite_us_page_detail").validate({
		rules:
		{
			banner_text:
			{
				required: true,
			},
			banner_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			description:
			{
				required: true,
			},
			form_title: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin add medical insurance===================*/

	jQuery("#add_medical_insurance_page_detail").validate({
		rules:
		{
			banner_text:
			{
				required: true,
			},
			banner_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			title:
			{
				required: true,
			},
			left_block_description: 
			{
				required: true,
			},
			left_featured_image: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for medical insurance interest form===================*/

	jQuery("#medical_insurance_interest_form").validate({
		rules:
		{
			title:
			{
				required: true,
			},
			fname:
			{
				required: true,
			},
			sname:
			{
				required: true,
			},
			mobile_country_code: 
			{
				required: true,
			},
			email: 
			{
				required: true,
			},
			country: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for contact us form===================*/

	jQuery("#add_contact_us_form").validate({
		rules:
		{
			title:
			{
				required: true,
			},
			fname:
			{
				required: true,
			},
			lname:
			{
				required: true,
			},
			email:
			{
				required: true,
			},
			pnumber: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin add country origin===================*/

	jQuery("#add_country_origin_form").validate({
		rules:
		{
			country:
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin add country detail===================*/

	jQuery("#add_country_detail").validate({
		rules:
		{
			banner_title:
			{
				required: true,
			},
			banner_sub_title:
			{
				required: true,
			},
			banner_description:
			{
				required: true,
			},
			banner_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			banner_logo:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			country_title:
			{
				required: true,
			},
			country_description:
			{
				required: true,
			},
			country_image:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================validation for admin options===================*/

	jQuery("#header_footer_options").validate({
		rules:
		{
			"phone_number[]":
			{
				required: true,
			},
			"whats_app_number[]":
			{
				required: true,
			},
			header_logo:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
			footer_logo:
			{
				required: true,
				accept:"jpg,png,jpeg"
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

/*======================Apply application form===================*/

	jQuery("#apply_application_form").validate({
		rules:
		{
			marital:
			{
				required: true,
			},
			dependents:
			{
				required: true,
			},
			national_id1:
			{
				required: true,
			},
			busaddress:
			{
				required: true,
			},
			country:
			{
				required: true,
			},
			town_city:
			{
				required: true,
			},
			postcode:
			{
				required: true,
			},
			title1:
			{
				required: true,
			},
			fname1:
			{
				required: true,
			},
			sname1:
			{
				required: true,
			},
			relationship1:
			{
				required: true,
			},
			title2:
			{
				required: true,
			},
			fname2:
			{
				required: true,
			},
			sname2:
			{
				required: true,
			},
			relationship2:
			{
				required: true,
			},
			pastone:
			{
				required: true,
			},
			pastone2:
			{
				required: true,
			},
			termscondition:
			{
				required: true,
			},
		},
		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});
		
		
});

