jQuery(document).on('click','.add-nucleus-family',function(){
	jQuery('.nucleus-family-select').css('display','block');
	jQuery('.other-family-select').css('display','none');
	jQuery('.family-type').val('nucleus-family');
});
jQuery(document).on('click','.add-other-family',function(){
	jQuery('.other-family-select').css('display','block');
	jQuery('.nucleus-family-select').css('display','none');
	jQuery('.family-type').val('other-family');
});

jQuery(document).on('change','.select_family_member',function(){
	var member = jQuery(this).val();
	if(member != '')
	{
		jQuery('#add_new_applicant_form').css('display','block');
		jQuery('.family-title').html('Add'+' '+member+' '+'Details');
		jQuery('.family-relation').val(member);
		if(member == 'Children')
		{
			jQuery('.app_gender').css('display','none');
			jQuery('.app_relationship').css('display','block');
		}
		else if(member == 'Spouse')
		{
			jQuery('.app_relationship').css('display','none');
			jQuery('.app_gender').css('display','block');
		}
		else if(member != 'Children' || member != 'Spouse')
		{
			jQuery('.app_relationship').css('display','none');
			jQuery('.app_gender').css('display','none');
		}
	}
	else
	{
		jQuery('#add_new_applicant_form').css('display','none');
	}
});

/*======Delete family quote=======*/

jQuery(document).on('click','#delete_family_quote',function(){
    var userId = jQuery(this).attr('data-attr');
    jQuery('.family_user_id').val(userId);
    jQuery('#deleteModal').modal('show');
});
jQuery(document).on('click','.yes-delete-quote',function(){
    var familyId = jQuery('.family_user_id').val();
	jQuery.ajax({
		url: site_url+'/delete_family_quote',
		type: "POST",
		data: {familyId:familyId},
		dataType: "json",
		success: function(response) 
		{
			jQuery('#deleteModal').modal('hide');
			if(response.success == true)
			{
				jQuery("html, body").animate({ scrollTop: 0 }, "slow");
				toastr.success(response.msg, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				setTimeout(
					function() 
					{
						if(response.url)
						{
							window.location = response.url;
						}
					}, 2000
				);
			}
			if(response.success == false)
			{
				toastr.error(response.msg, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});
});
