-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 07:55 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diaspora_funeral`
--

-- --------------------------------------------------------

--
-- Table structure for table `df_careers`
--

CREATE TABLE `df_careers` (
  `id` int(11) NOT NULL,
  `banner_text` text NOT NULL,
  `banner_image` text NOT NULL,
  `block1_title` text NOT NULL,
  `block1_description` text NOT NULL,
  `block2_title` text NOT NULL,
  `main_image` text NOT NULL,
  `career_form_title` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_careers`
--

INSERT INTO `df_careers` (`id`, `banner_text`, `banner_image`, `block1_title`, `block1_description`, `block2_title`, `main_image`, `career_form_title`, `created_at`, `updated_at`) VALUES
(1, 'gfddg', 'hqdefault_1561378635.jpg', 'gdfgdfg', '<p>gfdgfgdfg</p>', 'gfdgdfg', 'images_(1)_1561378635.jpg', 'fdgfdgfdgf', '2019-06-24 12:17:15', '2019-06-24 12:17:15'),
(2, 'ffdsf', 'hqdefault_1561378666.jpg', 'fdsfdfds', '', 'fdsfdsf', 'morning-zen-hd-wallpaper-by-tj-holowaychuk_0_1561378666.jpg', 'dsfdsfds', '2019-06-24 12:17:46', '2019-06-24 12:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `df_career_form`
--

CREATE TABLE `df_career_form` (
  `id` int(11) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` text NOT NULL,
  `position` text NOT NULL,
  `mnumber` text NOT NULL,
  `country_code` text NOT NULL,
  `message` text NOT NULL,
  `resume` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_career_form`
--

INSERT INTO `df_career_form` (`id`, `fname`, `lname`, `email`, `position`, `mnumber`, `country_code`, `message`, `resume`, `created_at`, `updated_at`) VALUES
(1, 'fgfdgdg', 'dfgdfgfd', 'gdfgdf@gmail.com', '1', '5454353454', '45', 'gfgfgdg', '', '2019-06-24 05:44:51', '0000-00-00 00:00:00'),
(2, 'fgfdgdg', 'dfgdfgfd', 'gdfgdf@gmail.com', '1', '5454353454', '45', 'gfgfgdg', '', '2019-06-24 06:00:23', '0000-00-00 00:00:00'),
(3, 'fgfdgdg', 'dfgdfgfd', 'gdfgdf@gmail.com', '1', '5454353454', '45', 'gfgfgdg', '', '2019-06-24 06:00:42', '0000-00-00 00:00:00'),
(4, 'fdsfd', 'fdsfdsf', 'fdfds@gmail.com', '1', '42343243243', '43', 'fdfsfdsf', 'Animalprofile-_1561358649.pdf', '2019-06-24 06:44:09', '0000-00-00 00:00:00'),
(5, 'Gurjeevan', 'Kaur', 'gurjeevan.kaur@imarkinfotech.com', '1', '45545453454', 'India', 'hghgh', '', '2019-06-24 06:50:54', '0000-00-00 00:00:00'),
(6, 'fgfg', 'fdgfdgdf', 'gurjeevan.kaur@imarkinfotech.com', '1', '5454353454', 'India', 'fgfgfg', '', '2019-06-24 06:51:27', '0000-00-00 00:00:00'),
(7, 'Gurjeevan', 'Kaur', 'gurjeevan.kaur@imarkinfotech.com', '1', '5454353454', 'India', 'yuytuytuty', 'Animalprofile-_1561359142.pdf', '2019-06-24 06:52:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `df_cash_plan_form`
--

CREATE TABLE `df_cash_plan_form` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `fname` text NOT NULL,
  `sname` text NOT NULL,
  `telephone` text NOT NULL,
  `email` text NOT NULL,
  `dob` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `df_contact_us_form`
--

CREATE TABLE `df_contact_us_form` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` text NOT NULL,
  `pnumber` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_contact_us_form`
--

INSERT INTO `df_contact_us_form` (`id`, `title`, `fname`, `lname`, `email`, `pnumber`, `message`, `created_at`, `updated_at`) VALUES
(1, '', 'fdfdsfd', 'fdfdf', 'fdfdfs@gmail.com', '5454534545', 'fdsfdfdsf', '2019-06-26 12:05:34', '2019-06-26 12:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `df_continent_residence`
--

CREATE TABLE `df_continent_residence` (
  `id` int(11) NOT NULL,
  `continent` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_continent_residence`
--

INSERT INTO `df_continent_residence` (`id`, `continent`, `created_at`, `updated_at`) VALUES
(1, 'Africa', '2019-07-03 10:51:57', '2019-07-03 10:51:57'),
(2, 'Antarctica', '2019-07-03 10:51:57', '2019-07-03 10:51:57'),
(3, 'Asia', '2019-07-03 10:51:59', '2019-07-03 10:52:08'),
(4, 'Australia', '2019-07-03 10:51:59', '2019-07-03 10:52:14'),
(5, 'Europe', '2019-07-03 10:52:30', '2019-07-03 10:52:30'),
(6, 'North America', '2019-07-03 10:52:30', '2019-07-03 10:52:30'),
(7, 'South America', '2019-07-03 10:52:32', '2019-07-03 10:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `df_countries`
--

CREATE TABLE `df_countries` (
  `ID` int(11) NOT NULL,
  `code` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `dial_code` int(11) NOT NULL,
  `currency_name` varchar(20) NOT NULL,
  `currency_symbol` varchar(20) NOT NULL,
  `currency_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `df_countries`
--

INSERT INTO `df_countries` (`ID`, `code`, `name`, `dial_code`, `currency_name`, `currency_symbol`, `currency_code`) VALUES
(1, 'AF', 'Afghanistan', 93, 'Afghan afghani', 'Ø‹', 'AFN'),
(2, 'AL', 'Albania', 355, 'Albanian lek', 'L', 'ALL'),
(3, 'DZ', 'Algeria', 213, 'Algerian dinar', 'Ø¯.Ø¬', 'DZD'),
(4, 'AS', 'American Samoa', 1684, '', '', ''),
(5, 'AD', 'Andorra', 376, 'Euro', 'â‚¬', 'EUR'),
(6, 'AO', 'Angola', 244, 'Angolan kwanza', 'Kz', 'AOA'),
(7, 'AI', 'Anguilla', 1264, 'East Caribbean dolla', '$', 'XCD'),
(8, 'AQ', 'Antarctica', 0, '', '', ''),
(9, 'AG', 'Antigua And Barbuda', 1268, 'East Caribbean dolla', '$', 'XCD'),
(10, 'AR', 'Argentina', 54, 'Argentine peso', '$', 'ARS'),
(11, 'AM', 'Armenia', 374, 'Armenian dram', '', 'AMD'),
(12, 'AW', 'Aruba', 297, 'Aruban florin', 'Æ’', 'AWG'),
(13, 'AU', 'Australia', 61, 'Australian dollar', '$', 'AUD'),
(14, 'AT', 'Austria', 43, 'Euro', 'â‚¬', 'EUR'),
(15, 'AZ', 'Azerbaijan', 994, 'Azerbaijani manat', '', 'AZN'),
(16, 'BS', 'Bahamas The', 1242, '', '', ''),
(17, 'BH', 'Bahrain', 973, 'Bahraini dinar', '.Ø¯.Ø¨', 'BHD'),
(18, 'BD', 'Bangladesh', 880, 'Bangladeshi taka', 'à§³', 'BDT'),
(19, 'BB', 'Barbados', 1246, 'Barbadian dollar', '$', 'BBD'),
(20, 'BY', 'Belarus', 375, 'Belarusian ruble', 'Br', 'BYR'),
(21, 'BE', 'Belgium', 32, 'Euro', 'â‚¬', 'EUR'),
(22, 'BZ', 'Belize', 501, 'Belize dollar', '$', 'BZD'),
(23, 'BJ', 'Benin', 229, 'West African CFA fra', 'Fr', 'XOF'),
(24, 'BM', 'Bermuda', 1441, 'Bermudian dollar', '$', 'BMD'),
(25, 'BT', 'Bhutan', 975, 'Bhutanese ngultrum', 'Nu.', 'BTN'),
(26, 'BO', 'Bolivia', 591, 'Bolivian boliviano', 'Bs.', 'BOB'),
(27, 'BA', 'Bosnia and Herzegovina', 387, 'Bosnia and Herzegovi', 'KM or ÐšÐœ', 'BAM'),
(28, 'BW', 'Botswana', 267, 'Botswana pula', 'P', 'BWP'),
(29, 'BV', 'Bouvet Island', 0, '', '', ''),
(30, 'BR', 'Brazil', 55, 'Brazilian real', 'R$', 'BRL'),
(31, 'IO', 'British Indian Ocean Territory', 246, 'United States dollar', '$', 'USD'),
(32, 'BN', 'Brunei', 673, 'Brunei dollar', '$', 'BND'),
(33, 'BG', 'Bulgaria', 359, 'Bulgarian lev', 'Ð»Ð²', 'BGN'),
(34, 'BF', 'Burkina Faso', 226, 'West African CFA fra', 'Fr', 'XOF'),
(35, 'BI', 'Burundi', 257, 'Burundian franc', 'Fr', 'BIF'),
(36, 'KH', 'Cambodia', 855, 'Cambodian riel', 'áŸ›', 'KHR'),
(37, 'CM', 'Cameroon', 237, 'Central African CFA ', 'Fr', 'XAF'),
(38, 'CA', 'Canada', 1, 'Canadian dollar', '$', 'CAD'),
(39, 'CV', 'Cape Verde', 238, 'Cape Verdean escudo', 'Esc or $', 'CVE'),
(40, 'KY', 'Cayman Islands', 1345, 'Cayman Islands dolla', '$', 'KYD'),
(41, 'CF', 'Central African Republic', 236, 'Central African CFA ', 'Fr', 'XAF'),
(42, 'TD', 'Chad', 235, 'Central African CFA ', 'Fr', 'XAF'),
(43, 'CL', 'Chile', 56, 'Chilean peso', '$', 'CLP'),
(44, 'CN', 'China', 86, 'Chinese yuan', 'Â¥ or å…ƒ', 'CNY'),
(45, 'CX', 'Christmas Island', 61, '', '', ''),
(46, 'CC', 'Cocos (Keeling) Islands', 672, 'Australian dollar', '$', 'AUD'),
(47, 'CO', 'Colombia', 57, 'Colombian peso', '$', 'COP'),
(48, 'KM', 'Comoros', 269, 'Comorian franc', 'Fr', 'KMF'),
(49, 'CG', 'Congo', 242, '', '', ''),
(50, 'CD', 'Congo The Democratic Republic Of The', 242, '', '', ''),
(51, 'CK', 'Cook Islands', 682, 'New Zealand dollar', '$', 'NZD'),
(52, 'CR', 'Costa Rica', 506, 'Costa Rican colÃ³n', 'â‚¡', 'CRC'),
(53, 'CI', 'Cote D''Ivoire (Ivory Coast)', 225, '', '', ''),
(54, 'HR', 'Croatia (Hrvatska)', 385, '', '', ''),
(55, 'CU', 'Cuba', 53, 'Cuban convertible pe', '$', 'CUC'),
(56, 'CY', 'Cyprus', 357, 'Euro', 'â‚¬', 'EUR'),
(57, 'CZ', 'Czech Republic', 420, 'Czech koruna', 'KÄ', 'CZK'),
(58, 'DA', 'Denmark', 45, 'Danish krone', 'kr', 'DKK'),
(59, 'DJ', 'Djibouti', 253, 'Djiboutian franc', 'Fr', 'DJF'),
(60, 'DM', 'Dominica', 1767, 'East Caribbean dolla', '$', 'XCD'),
(61, 'DO', 'Dominican Republic', 1809, 'Dominican peso', '$', 'DOP'),
(62, 'TP', 'East Timor', 670, 'United States dollar', '$', 'USD'),
(63, 'EC', 'Ecuador', 593, 'United States dollar', '$', 'USD'),
(64, 'EG', 'Egypt', 20, 'Egyptian pound', 'Â£ or Ø¬.Ù…', 'EGP'),
(65, 'SV', 'El Salvador', 503, 'United States dollar', '$', 'USD'),
(66, 'GQ', 'Equatorial Guinea', 240, 'Central African CFA ', 'Fr', 'XAF'),
(67, 'ER', 'Eritrea', 291, 'Eritrean nakfa', 'Nfk', 'ERN'),
(68, 'EE', 'Estonia', 372, 'Euro', 'â‚¬', 'EUR'),
(69, 'ET', 'Ethiopia', 251, 'Ethiopian birr', 'Br', 'ETB'),
(70, 'XA', 'External Territories of Australia', 61, '', '', ''),
(71, 'FK', 'Falkland Islands', 500, 'Falkland Islands pou', 'Â£', 'FKP'),
(72, 'FO', 'Faroe Islands', 298, 'Danish krone', 'kr', 'DKK'),
(73, 'FJ', 'Fiji Islands', 679, '', '', ''),
(74, 'FI', 'Finland', 358, 'Euro', 'â‚¬', 'EUR'),
(75, 'FR', 'France', 33, 'Euro', 'â‚¬', 'EUR'),
(76, 'GF', 'French Guiana', 594, '', '', ''),
(77, 'PF', 'French Polynesia', 689, 'CFP franc', 'Fr', 'XPF'),
(78, 'TF', 'French Southern Territories', 0, '', '', ''),
(79, 'GA', 'Gabon', 241, 'Central African CFA ', 'Fr', 'XAF'),
(80, 'GM', 'Gambia The', 220, '', '', ''),
(81, 'GE', 'Georgia', 995, 'Georgian lari', 'áƒš', 'GEL'),
(82, 'DE', 'Germany', 49, 'Euro', 'â‚¬', 'EUR'),
(83, 'GH', 'Ghana', 233, 'Ghana cedi', 'â‚µ', 'GHS'),
(84, 'GI', 'Gibraltar', 350, 'Gibraltar pound', 'Â£', 'GIP'),
(85, 'GR', 'Greece', 30, 'Euro', 'â‚¬', 'EUR'),
(86, 'GL', 'Greenland', 299, '', '', ''),
(87, 'GD', 'Grenada', 1473, 'East Caribbean dolla', '$', 'XCD'),
(88, 'GP', 'Guadeloupe', 590, '', '', ''),
(89, 'GU', 'Guam', 1671, '', '', ''),
(90, 'GT', 'Guatemala', 502, 'Guatemalan quetzal', 'Q', 'GTQ'),
(91, 'XU', 'Guernsey and Alderney', 44, '', '', ''),
(92, 'GN', 'Guinea', 224, 'Guinean franc', 'Fr', 'GNF'),
(93, 'GW', 'Guinea-Bissau', 245, 'West African CFA fra', 'Fr', 'XOF'),
(94, 'GY', 'Guyana', 592, 'Guyanese dollar', '$', 'GYD'),
(95, 'HT', 'Haiti', 509, 'Haitian gourde', 'G', 'HTG'),
(96, 'HM', 'Heard and McDonald Islands', 0, '', '', ''),
(97, 'HN', 'Honduras', 504, 'Honduran lempira', 'L', 'HNL'),
(98, 'HK', 'Hong Kong S.A.R.', 852, '', '', ''),
(99, 'HU', 'Hungary', 36, 'Hungarian forint', 'Ft', 'HUF'),
(100, 'IS', 'Iceland', 354, 'Icelandic krÃ³na', 'kr', 'ISK'),
(101, 'IN', 'India', 91, 'Indian rupee', 'â‚¹', 'INR'),
(102, 'ID', 'Indonesia', 62, 'Indonesian rupiah', 'Rp', 'IDR'),
(103, 'IR', 'Iran', 98, 'Iranian rial', 'ï·¼', 'IRR'),
(104, 'IQ', 'Iraq', 964, 'Iraqi dinar', 'Ø¹.Ø¯', 'IQD'),
(105, 'IE', 'Ireland', 353, 'Euro', 'â‚¬', 'EUR'),
(106, 'IL', 'Israel', 972, 'Israeli new shekel', 'â‚ª', 'ILS'),
(107, 'IT', 'Italy', 39, 'Euro', 'â‚¬', 'EUR'),
(108, 'JM', 'Jamaica', 1876, 'Jamaican dollar', '$', 'JMD'),
(109, 'JP', 'Japan', 81, 'Japanese yen', 'Â¥', 'JPY'),
(110, 'XJ', 'Jersey', 44, 'British pound', 'Â£', 'GBP'),
(111, 'JO', 'Jordan', 962, 'Jordanian dinar', 'Ø¯.Ø§', 'JOD'),
(112, 'KZ', 'Kazakhstan', 7, 'Kazakhstani tenge', '', 'KZT'),
(113, 'KE', 'Kenya', 254, 'Kenyan shilling', 'Sh', 'KES'),
(114, 'KI', 'Kiribati', 686, 'Australian dollar', '$', 'AUD'),
(115, 'KP', 'Korea North', 850, '', '', ''),
(116, 'KR', 'Korea South', 82, '', '', ''),
(117, 'KW', 'Kuwait', 965, 'Kuwaiti dinar', 'Ø¯.Ùƒ', 'KWD'),
(118, 'KG', 'Kyrgyzstan', 996, 'Kyrgyzstani som', 'Ð»Ð²', 'KGS'),
(119, 'LA', 'Laos', 856, 'Lao kip', 'â‚­', 'LAK'),
(120, 'LV', 'Latvia', 371, 'Euro', 'â‚¬', 'EUR'),
(121, 'LB', 'Lebanon', 961, 'Lebanese pound', 'Ù„.Ù„', 'LBP'),
(122, 'LS', 'Lesotho', 266, 'Lesotho loti', 'L', 'LSL'),
(123, 'LR', 'Liberia', 231, 'Liberian dollar', '$', 'LRD'),
(124, 'LY', 'Libya', 218, 'Libyan dinar', 'Ù„.Ø¯', 'LYD'),
(125, 'LI', 'Liechtenstein', 423, 'Swiss franc', 'Fr', 'CHF'),
(126, 'LT', 'Lithuania', 370, 'Euro', 'â‚¬', 'EUR'),
(127, 'LU', 'Luxembourg', 352, 'Euro', 'â‚¬', 'EUR'),
(128, 'MO', 'Macau S.A.R.', 853, '', '', ''),
(129, 'MK', 'Macedonia', 389, '', '', ''),
(130, 'MG', 'Madagascar', 261, 'Malagasy ariary', 'Ar', 'MGA'),
(131, 'MW', 'Malawi', 265, 'Malawian kwacha', 'MK', 'MWK'),
(132, 'MY', 'Malaysia', 60, 'Malaysian ringgit', 'RM', 'MYR'),
(133, 'MV', 'Maldives', 960, 'Maldivian rufiyaa', '.Þƒ', 'MVR'),
(134, 'ML', 'Mali', 223, 'West African CFA fra', 'Fr', 'XOF'),
(135, 'MT', 'Malta', 356, 'Euro', 'â‚¬', 'EUR'),
(136, 'XM', 'Man (Isle of)', 44, '', '', ''),
(137, 'MH', 'Marshall Islands', 692, 'United States dollar', '$', 'USD'),
(138, 'MQ', 'Martinique', 596, '', '', ''),
(139, 'MR', 'Mauritania', 222, 'Mauritanian ouguiya', 'UM', 'MRO'),
(140, 'MU', 'Mauritius', 230, 'Mauritian rupee', 'â‚¨', 'MUR'),
(141, 'YT', 'Mayotte', 269, '', '', ''),
(142, 'MX', 'Mexico', 52, 'Mexican peso', '$', 'MXN'),
(143, 'FM', 'Micronesia', 691, 'Micronesian dollar', '$', ''),
(144, 'MD', 'Moldova', 373, 'Moldovan leu', 'L', 'MDL'),
(145, 'MC', 'Monaco', 377, 'Euro', 'â‚¬', 'EUR'),
(146, 'MN', 'Mongolia', 976, 'Mongolian tÃ¶grÃ¶g', 'â‚®', 'MNT'),
(147, 'MS', 'Montserrat', 1664, 'East Caribbean dolla', '$', 'XCD'),
(148, 'MA', 'Morocco', 212, 'Moroccan dirham', 'Ø¯.Ù….', 'MAD'),
(149, 'MZ', 'Mozambique', 258, 'Mozambican metical', 'MT', 'MZN'),
(150, 'MM', 'Myanmar', 95, 'Burmese kyat', 'Ks', 'MMK'),
(151, 'NA', 'Namibia', 264, 'Namibian dollar', '$', 'NAD'),
(152, 'NR', 'Nauru', 674, 'Australian dollar', '$', 'AUD'),
(153, 'NP', 'Nepal', 977, 'Nepalese rupee', 'â‚¨', 'NPR'),
(154, 'AN', 'Netherlands Antilles', 599, '', '', ''),
(155, 'NL', 'Netherlands The', 31, '', '', ''),
(156, 'NC', 'New Caledonia', 687, 'CFP franc', 'Fr', 'XPF'),
(157, 'NZ', 'New Zealand', 64, 'New Zealand dollar', '$', 'NZD'),
(158, 'NI', 'Nicaragua', 505, 'Nicaraguan cÃ³rdoba', 'C$', 'NIO'),
(159, 'NE', 'Niger', 227, 'West African CFA fra', 'Fr', 'XOF'),
(160, 'NG', 'Nigeria', 234, 'Nigerian naira', 'â‚¦', 'NGN'),
(161, 'NU', 'Niue', 683, 'New Zealand dollar', '$', 'NZD'),
(162, 'NF', 'Norfolk Island', 672, '', '', ''),
(163, 'MP', 'Northern Mariana Islands', 1670, '', '', ''),
(164, 'NO', 'Norway', 47, 'Norwegian krone', 'kr', 'NOK'),
(165, 'OM', 'Oman', 968, 'Omani rial', 'Ø±.Ø¹.', 'OMR'),
(166, 'PK', 'Pakistan', 92, 'Pakistani rupee', 'â‚¨', 'PKR'),
(167, 'PW', 'Palau', 680, 'Palauan dollar', '$', ''),
(168, 'PS', 'Palestinian Territory Occupied', 970, '', '', ''),
(169, 'PA', 'Panama', 507, 'Panamanian balboa', 'B/.', 'PAB'),
(170, 'PG', 'Papua new Guinea', 675, 'Papua New Guinean ki', 'K', 'PGK'),
(171, 'PY', 'Paraguay', 595, 'Paraguayan guaranÃ­', 'â‚²', 'PYG'),
(172, 'PE', 'Peru', 51, 'Peruvian nuevo sol', 'S/.', 'PEN'),
(173, 'PH', 'Philippines', 63, 'Philippine peso', 'â‚±', 'PHP'),
(174, 'PN', 'Pitcairn Island', 0, '', '', ''),
(175, 'PL', 'Poland', 48, 'Polish zÅ‚oty', 'zÅ‚', 'PLN'),
(176, 'PT', 'Portugal', 351, 'Euro', 'â‚¬', 'EUR'),
(177, 'PR', 'Puerto Rico', 1787, '', '', ''),
(178, 'QA', 'Qatar', 974, 'Qatari riyal', 'Ø±.Ù‚', 'QAR'),
(179, 'RE', 'Reunion', 262, '', '', ''),
(180, 'RO', 'Romania', 40, 'Romanian leu', 'lei', 'RON'),
(181, 'RU', 'Russia', 70, 'Russian ruble', '', 'RUB'),
(182, 'RW', 'Rwanda', 250, 'Rwandan franc', 'Fr', 'RWF'),
(183, 'SH', 'Saint Helena', 290, 'Saint Helena pound', 'Â£', 'SHP'),
(184, 'KN', 'Saint Kitts And Nevis', 1869, 'East Caribbean dolla', '$', 'XCD'),
(185, 'LC', 'Saint Lucia', 1758, 'East Caribbean dolla', '$', 'XCD'),
(186, 'PM', 'Saint Pierre and Miquelon', 508, '', '', ''),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784, 'East Caribbean dolla', '$', 'XCD'),
(188, 'WS', 'Samoa', 684, 'Samoan tÄlÄ', 'T', 'WST'),
(189, 'SM', 'San Marino', 378, 'Euro', 'â‚¬', 'EUR'),
(190, 'ST', 'Sao Tome and Principe', 239, 'SÃ£o TomÃ© and PrÃ­n', 'Db', 'STD'),
(191, 'SA', 'Saudi Arabia', 966, 'Saudi riyal', 'Ø±.Ø³', 'SAR'),
(192, 'SN', 'Senegal', 221, 'West African CFA fra', 'Fr', 'XOF'),
(193, 'RS', 'Serbia', 381, 'Serbian dinar', 'Ð´Ð¸Ð½. or din.', 'RSD'),
(194, 'SC', 'Seychelles', 248, 'Seychellois rupee', 'â‚¨', 'SCR'),
(195, 'SL', 'Sierra Leone', 232, 'Sierra Leonean leone', 'Le', 'SLL'),
(196, 'SG', 'Singapore', 65, 'Brunei dollar', '$', 'BND'),
(197, 'SK', 'Slovakia', 421, 'Euro', 'â‚¬', 'EUR'),
(198, 'SI', 'Slovenia', 386, 'Euro', 'â‚¬', 'EUR'),
(199, 'XG', 'Smaller Territories of the UK', 44, '', '', ''),
(200, 'SB', 'Solomon Islands', 677, 'Solomon Islands doll', '$', 'SBD'),
(201, 'SO', 'Somalia', 252, 'Somali shilling', 'Sh', 'SOS'),
(202, 'ZA', 'South Africa', 27, 'South African rand', 'R', 'ZAR'),
(203, 'GS', 'South Georgia', 0, '', '', ''),
(204, 'SS', 'South Sudan', 211, 'South Sudanese pound', 'Â£', 'SSP'),
(205, 'ES', 'Spain', 34, 'Euro', 'â‚¬', 'EUR'),
(206, 'LK', 'Sri Lanka', 94, 'Sri Lankan rupee', 'Rs or à¶»à·”', 'LKR'),
(207, 'SD', 'Sudan', 249, 'Sudanese pound', 'Ø¬.Ø³.', 'SDG'),
(208, 'SR', 'Suriname', 597, 'Surinamese dollar', '$', 'SRD'),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47, '', '', ''),
(210, 'SZ', 'Swaziland', 268, 'Swazi lilangeni', 'L', 'SZL'),
(211, 'SE', 'Sweden', 46, 'Swedish krona', 'kr', 'SEK'),
(212, 'CH', 'Switzerland', 41, 'Swiss franc', 'Fr', 'CHF'),
(213, 'SY', 'Syria', 963, 'Syrian pound', 'Â£ or Ù„.Ø³', 'SYP'),
(214, 'TW', 'Taiwan', 886, 'New Taiwan dollar', '$', 'TWD'),
(215, 'TJ', 'Tajikistan', 992, 'Tajikistani somoni', 'Ð…Ðœ', 'TJS'),
(216, 'TZ', 'Tanzania', 255, 'Tanzanian shilling', 'Sh', 'TZS'),
(217, 'TH', 'Thailand', 66, 'Thai baht', 'à¸¿', 'THB'),
(218, 'TG', 'Togo', 228, 'West African CFA fra', 'Fr', 'XOF'),
(219, 'TK', 'Tokelau', 690, '', '', ''),
(220, 'TO', 'Tonga', 676, 'Tongan paÊ»anga', 'T$', 'TOP'),
(221, 'TT', 'Trinidad And Tobago', 1868, 'Trinidad and Tobago ', '$', 'TTD'),
(222, 'TN', 'Tunisia', 216, 'Tunisian dinar', 'Ø¯.Øª', 'TND'),
(223, 'TR', 'Turkey', 90, 'Turkish lira', '', 'TRY'),
(224, 'TM', 'Turkmenistan', 7370, 'Turkmenistan manat', 'm', 'TMT'),
(225, 'TC', 'Turks And Caicos Islands', 1649, 'United States dollar', '$', 'USD'),
(226, 'TV', 'Tuvalu', 688, 'Australian dollar', '$', 'AUD'),
(227, 'UG', 'Uganda', 256, 'Ugandan shilling', 'Sh', 'UGX'),
(228, 'UA', 'Ukraine', 380, 'Ukrainian hryvnia', 'â‚´', 'UAH'),
(229, 'AE', 'United Arab Emirates', 971, 'United Arab Emirates', 'Ø¯.Ø¥', 'AED'),
(230, 'GB', 'United Kingdom', 44, 'British pound', 'Â£', 'GBP'),
(231, 'US', 'United States', 1, 'United States dollar', '$', 'USD'),
(232, 'UM', 'United States Minor Outlying Islands', 1, '', '', ''),
(233, 'UY', 'Uruguay', 598, 'Uruguayan peso', '$', 'UYU'),
(234, 'UZ', 'Uzbekistan', 998, 'Uzbekistani som', '', 'UZS'),
(235, 'VU', 'Vanuatu', 678, 'Vanuatu vatu', 'Vt', 'VUV'),
(236, 'VA', 'Vatican City State (Holy See)', 39, '', '', ''),
(237, 'VE', 'Venezuela', 58, 'Venezuelan bolÃ­var', 'Bs F', 'VEF'),
(238, 'VN', 'Vietnam', 84, 'Vietnamese Ä‘á»“ng', 'â‚«', 'VND'),
(239, 'VG', 'Virgin Islands (British)', 1284, '', '', ''),
(240, 'VI', 'Virgin Islands (US)', 1340, '', '', ''),
(241, 'WF', 'Wallis And Futuna Islands', 681, '', '', ''),
(242, 'EH', 'Western Sahara', 212, '', '', ''),
(243, 'YE', 'Yemen', 967, 'Yemeni rial', 'ï·¼', 'YER'),
(244, 'YU', 'Yugoslavia', 38, '', '', ''),
(245, 'ZM', 'Zambia', 260, 'Zambian kwacha', 'ZK', 'ZMW'),
(246, 'ZW', 'Zimbabwe', 263, 'Botswana pula', 'P', 'BWP');

-- --------------------------------------------------------

--
-- Table structure for table `df_country_detail`
--

CREATE TABLE `df_country_detail` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `country_meta_key` text NOT NULL,
  `country_meta_value` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_country_detail`
--

INSERT INTO `df_country_detail` (`id`, `country_id`, `country_meta_key`, `country_meta_value`, `created_at`, `updated_at`) VALUES
(44, 13, 'banner_logo', 'follow5_1562065988.png', '2019-07-02 11:13:11', '2019-07-02 11:13:11'),
(45, 13, 'country_image', 'footer-logo_1562065988.png', '2019-07-02 11:13:11', '2019-07-02 11:13:11'),
(73, 9, 'countryId', '9', '2019-07-02 12:26:05', '2019-07-02 12:26:05'),
(74, 9, 'banner_title', 'DIASPORA INSURANCE', '2019-07-02 12:26:05', '2019-07-02 12:26:05'),
(75, 9, 'banner_sub_title', 'For Zimbabweans Worldwide', '2019-07-02 12:26:05', '2019-07-02 12:26:05'),
(76, 9, 'banner_description', '<p><span style="font-family: Poppins, sans-serif; font-size: 16px; font-style: italic; text-align: center;">Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</span></p>', '2019-07-02 12:26:05', '2019-07-05 10:26:06'),
(77, 9, 'country_title', 'Zimbabweans worldwide this bespoke cover is for you:', '2019-07-02 12:26:05', '2019-07-02 12:26:05'),
(78, 9, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Zimbabwean by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-02 12:26:05', '2019-07-02 12:37:28'),
(79, 9, 'banner_image', 'banner_1562070365.jpg', '2019-07-02 12:26:06', '2019-07-02 12:26:06'),
(80, 9, 'banner_logo', 'banner-logo_1562070365.png', '2019-07-02 12:26:06', '2019-07-02 12:26:06'),
(81, 9, 'country_image', 'Zim-flag_1562070365.jpg', '2019-07-02 12:26:06', '2019-07-02 12:26:06'),
(82, 11, 'countryId', '11', '2019-07-05 09:41:31', '2019-07-05 09:41:31'),
(83, 11, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 09:41:31', '2019-07-05 09:41:31'),
(84, 11, 'banner_sub_title', 'For Ghanaians Worldwide', '2019-07-05 09:41:31', '2019-07-08 12:28:55'),
(85, 11, 'banner_description', '<p>Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</p>', '2019-07-05 09:41:31', '2019-07-05 09:49:15'),
(86, 11, 'country_title', 'Ghanaians worldwide this bespoke cover is for you:', '2019-07-05 09:41:31', '2019-07-08 12:28:58'),
(87, 11, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Ghanaians by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 09:41:31', '2019-07-08 12:31:42'),
(88, 11, 'banner_image', 'Ghana_1562319690.jpg', '2019-07-05 09:41:31', '2019-07-05 09:41:31'),
(89, 11, 'banner_logo', 'banner-logo_1562319691.png', '2019-07-05 09:41:31', '2019-07-05 09:41:31'),
(90, 11, 'country_image', 'Ghana.jpg', '2019-07-05 09:41:31', '2019-07-08 13:17:36'),
(91, 12, 'countryId', '12', '2019-07-05 09:56:27', '2019-07-05 09:56:27'),
(92, 12, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 09:56:27', '2019-07-05 09:56:27'),
(93, 12, 'banner_sub_title', 'For Kenyans Worldwide', '2019-07-05 09:56:27', '2019-07-08 11:26:16'),
(94, 12, 'banner_description', '<p>Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</p>', '2019-07-05 09:56:27', '2019-07-05 09:57:31'),
(95, 12, 'country_title', 'Kenyans worldwide this bespoke cover is for you:', '2019-07-05 09:56:27', '2019-07-08 11:26:35'),
(96, 12, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Kenyans by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 09:56:27', '2019-07-08 11:27:11'),
(97, 12, 'banner_image', 'Kenya_1562320587.jpg', '2019-07-05 09:56:27', '2019-07-05 09:56:27'),
(98, 12, 'banner_logo', 'banner-logo_1562320587.png', '2019-07-05 09:56:27', '2019-07-05 09:56:27'),
(99, 12, 'country_image', 'Kenya-Flag.png', '2019-07-05 09:56:28', '2019-07-05 12:49:42'),
(100, 13, 'countryId', '13', '2019-07-05 10:00:52', '2019-07-05 10:00:52'),
(101, 13, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 10:00:52', '2019-07-05 10:00:52'),
(102, 13, 'banner_sub_title', 'For South Africans Worldwide', '2019-07-05 10:00:52', '2019-07-08 12:36:43'),
(103, 13, 'banner_description', '<p>Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</p>', '2019-07-05 10:00:52', '2019-07-05 10:06:44'),
(104, 13, 'country_title', 'South Africans worldwide this bespoke cover is for you:', '2019-07-05 10:00:52', '2019-07-08 12:36:48'),
(105, 13, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are South Africans by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 10:00:52', '2019-07-08 12:37:23'),
(106, 13, 'banner_image', 'South-Africa-banner.jpg', '2019-07-05 10:00:52', '2019-07-08 13:26:53'),
(107, 13, 'banner_logo', 'banner-logo_1562320852.png', '2019-07-05 10:00:52', '2019-07-05 10:00:52'),
(108, 13, 'country_image', 'South-Africa.png', '2019-07-05 10:00:53', '2019-07-08 13:16:20'),
(109, 14, 'countryId', '14', '2019-07-05 10:14:43', '2019-07-05 10:14:43'),
(110, 14, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 10:14:44', '2019-07-05 10:14:44'),
(111, 14, 'banner_sub_title', 'For Tanzanians Worldwide', '2019-07-05 10:14:44', '2019-07-08 11:27:45'),
(112, 14, 'banner_description', '<p>Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</p>', '2019-07-05 10:14:44', '2019-07-05 10:15:57'),
(113, 14, 'country_title', 'Tanzanians worldwide this bespoke cover is for you:', '2019-07-05 10:14:44', '2019-07-08 11:27:51'),
(114, 14, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Tanzanians by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 10:14:44', '2019-07-08 11:28:09'),
(115, 14, 'banner_image', 'Tanzania_1562321683.jpg', '2019-07-05 10:14:44', '2019-07-05 10:14:44'),
(116, 14, 'banner_logo', 'banner-logo_1562321683.png', '2019-07-05 10:14:44', '2019-07-05 10:14:44'),
(117, 14, 'country_image', 'Tanzania flag.png', '2019-07-05 10:14:44', '2019-07-05 12:51:05'),
(118, 15, 'countryId', '15', '2019-07-05 10:18:21', '2019-07-05 10:18:21'),
(119, 15, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 10:18:21', '2019-07-05 10:18:21'),
(120, 15, 'banner_sub_title', 'For Ugandans Worldwide', '2019-07-05 10:18:21', '2019-07-08 11:29:53'),
(121, 15, 'banner_description', '<p>Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</p>', '2019-07-05 10:18:21', '2019-07-05 10:19:01'),
(122, 15, 'country_title', 'Ugandans worldwide this bespoke cover is for you:', '2019-07-05 10:18:21', '2019-07-08 11:29:56'),
(123, 15, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Ugandans by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 10:18:21', '2019-07-08 11:30:08'),
(124, 15, 'banner_image', 'Uganda_1562321901.jpg', '2019-07-05 10:18:21', '2019-07-05 10:18:21'),
(125, 15, 'banner_logo', 'banner-logo_1562321901.png', '2019-07-05 10:18:21', '2019-07-05 10:18:21'),
(126, 15, 'country_image', 'Uganda.jpg', '2019-07-05 10:18:21', '2019-07-05 12:51:25'),
(136, 10, 'countryId', '10', '2019-07-05 10:22:25', '2019-07-05 10:23:58'),
(137, 10, 'banner_title', 'DIASPORA INSURANCE', '2019-07-05 10:22:26', '2019-07-05 10:22:58'),
(138, 10, 'banner_sub_title', 'For Zambians Worldwide', '2019-07-05 10:22:26', '2019-07-08 11:28:56'),
(139, 10, 'banner_description', '<p><span style="font-family: Poppins, sans-serif; font-size: 16px; font-style: italic; text-align: center;">Protect Yourself! protect Your Loved Ones! Protect Your Dignity!</span></p>', '2019-07-05 10:22:26', '2019-07-05 10:24:29'),
(140, 10, 'country_title', ' Zambians worldwide this bespoke cover is for you:', '2019-07-05 10:22:26', '2019-07-08 11:29:00'),
(141, 10, 'country_description', '<p>We now live in an era of transnationalism wherein as transnational citizens we are essentially global villagers who belong to more than one society. When it comes to your global family protection needs you need flexible and versatile risk management solutions. Talk of worldwide protection without boards, we got you covered. At Diaspora Insurance we specialise in crafting bespoke risk solutions to make sure that you can cover not just yourself in the diaspora but also your loved ones in your native country if you wish.</p>  <p>Whether you are Zambians by descent, registry, birth, marriage you name it the Diaspora Insurance is for You even if you have naturalised by taking other citizenship.</p>', '2019-07-05 10:22:26', '2019-07-08 11:29:10'),
(142, 10, 'banner_image', 'Zambia_1562322145.jpg', '2019-07-05 10:22:26', '2019-07-05 10:23:18'),
(143, 10, 'banner_logo', 'banner-logo_1562322145.png', '2019-07-05 10:22:26', '2019-07-05 10:23:39'),
(144, 10, 'country_image', 'Zambia.png', '2019-07-05 10:22:26', '2019-07-05 12:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `df_country_origin`
--

CREATE TABLE `df_country_origin` (
  `id` int(11) NOT NULL,
  `country` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_country_origin`
--

INSERT INTO `df_country_origin` (`id`, `country`, `created_at`, `updated_at`) VALUES
(9, 'Zimbabwe', '2019-06-27 10:28:20', '2019-06-27 10:28:50'),
(10, 'Zambia', '2019-06-27 10:29:01', '2019-06-27 10:29:01'),
(11, 'Ghana', '2019-07-02 11:08:34', '2019-07-02 11:08:34'),
(12, 'Kenya', '2019-07-02 11:08:47', '2019-07-02 11:08:47'),
(13, 'South Africa', '2019-07-02 11:08:58', '2019-07-02 11:08:58'),
(14, 'Tanzania', '2019-07-02 11:09:06', '2019-07-02 11:09:06'),
(15, 'Uganda', '2019-07-02 11:09:14', '2019-07-02 11:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `df_country_residence`
--

CREATE TABLE `df_country_residence` (
  `id` int(11) NOT NULL,
  `country_residence` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_country_residence`
--

INSERT INTO `df_country_residence` (`id`, `country_residence`, `created_at`, `updated_at`) VALUES
(1, 'Asia', '2019-06-20 07:33:27', '2019-06-20 07:33:27'),
(2, 'Africa', '2019-06-20 07:33:29', '2019-06-20 07:33:40'),
(3, 'Europe', '2019-06-20 07:33:49', '2019-06-20 07:33:49'),
(4, 'Australia', '2019-06-20 07:33:54', '2019-06-20 07:34:01'),
(5, 'North America', '2019-06-20 07:34:19', '2019-06-20 07:34:19'),
(6, 'South America', '2019-06-20 07:34:21', '2019-06-20 07:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `df_currency_cover`
--

CREATE TABLE `df_currency_cover` (
  `id` int(11) NOT NULL,
  `currency_cover` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `df_details_principal_beneficiary`
--

CREATE TABLE `df_details_principal_beneficiary` (
  `benef_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title1` varchar(20) DEFAULT NULL,
  `fname1` varchar(200) DEFAULT NULL,
  `sname1` varchar(200) DEFAULT NULL,
  `date_of_birth1` date DEFAULT NULL,
  `national_id1` varchar(255) DEFAULT NULL,
  `relationship1` varchar(255) DEFAULT NULL,
  `title2` varchar(20) DEFAULT NULL,
  `fname2` varchar(255) DEFAULT NULL,
  `sname2` varchar(255) DEFAULT NULL,
  `date_of_birth2` date DEFAULT NULL,
  `national_id2` varchar(255) DEFAULT NULL,
  `relationship2` varchar(255) DEFAULT NULL,
  `payments` varchar(100) DEFAULT NULL,
  `datec` date DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `ac` varchar(255) DEFAULT NULL,
  `pastone` varchar(20) DEFAULT NULL,
  `pastone1` varchar(20) DEFAULT NULL,
  `pastone2` varchar(20) DEFAULT NULL,
  `payment_date` varchar(50) DEFAULT NULL,
  `trans_id` varchar(255) DEFAULT NULL,
  `trans_status` varchar(255) DEFAULT NULL,
  `tras_date` datetime DEFAULT NULL,
  `PaymentMethod` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `df_details_principal_beneficiary`
--

INSERT INTO `df_details_principal_beneficiary` (`benef_id`, `user_id`, `title1`, `fname1`, `sname1`, `date_of_birth1`, `national_id1`, `relationship1`, `title2`, `fname2`, `sname2`, `date_of_birth2`, `national_id2`, `relationship2`, `payments`, `datec`, `bank`, `branch`, `ac`, `pastone`, `pastone1`, `pastone2`, `payment_date`, `trans_id`, `trans_status`, `tras_date`, `PaymentMethod`) VALUES
(2, 2, 'Mrs', 'gfdgdfgd', 'fgdfgf', NULL, 'Yes', 'gdfgdfgfd', 'Rev', 'gfdggfd', 'gfgdf', NULL, NULL, 'gdfgdf', NULL, NULL, NULL, NULL, NULL, 'No', NULL, 'no', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `df_gender`
--

CREATE TABLE `df_gender` (
  `id` int(11) NOT NULL,
  `gender_type` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_gender`
--

INSERT INTO `df_gender` (`id`, `gender_type`, `created_at`, `updated_at`) VALUES
(1, 'Female', '2019-06-20 09:27:21', '2019-06-20 09:27:21'),
(2, 'Male', '2019-06-20 09:27:24', '2019-06-20 09:27:29');

-- --------------------------------------------------------

--
-- Table structure for table `df_insurance_plan`
--

CREATE TABLE `df_insurance_plan` (
  `id` int(11) NOT NULL,
  `insurance_plan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_insurance_plan`
--

INSERT INTO `df_insurance_plan` (`id`, `insurance_plan`, `created_at`, `updated_at`) VALUES
(1, 'Diaspora Funeral Cash Plan', '2019-06-20 07:35:06', '2019-06-20 07:35:06'),
(2, 'Diaspora Insurance Plan ', '2019-07-03 10:56:27', '2019-07-03 10:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `df_invite_us_form`
--

CREATE TABLE `df_invite_us_form` (
  `id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_date` date NOT NULL,
  `venue_address` text NOT NULL,
  `event_organiser` text NOT NULL,
  `venue_capacity` int(11) NOT NULL,
  `expected_attendance` int(11) NOT NULL,
  `email` text NOT NULL,
  `country_code_mobile` text NOT NULL,
  `event_flyer` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `df_languages`
--

CREATE TABLE `df_languages` (
  `id` int(11) NOT NULL,
  `language` text NOT NULL,
  `lang_code` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_languages`
--

INSERT INTO `df_languages` (`id`, `language`, `lang_code`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', '2019-07-03 12:55:18', '0000-00-00 00:00:00'),
(2, 'Lang 1', 'lang', '2019-07-03 12:55:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `df_medical_insurance_interest_form`
--

CREATE TABLE `df_medical_insurance_interest_form` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `fname` text NOT NULL,
  `sname` text NOT NULL,
  `mobile_country_code` text NOT NULL,
  `email` text NOT NULL,
  `country` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_medical_insurance_interest_form`
--

INSERT INTO `df_medical_insurance_interest_form` (`id`, `title`, `fname`, `sname`, `mobile_country_code`, `email`, `country`, `created_at`, `updated_at`) VALUES
(1, 'fggfdgf', 'gdfgdfg', 'gdfgfgfd', 'g545435', 'gfggfd@gmail.com', 2, '2019-06-26 11:09:04', '2019-06-26 11:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `df_options`
--

CREATE TABLE `df_options` (
  `id` int(11) NOT NULL,
  `meta_key` text NOT NULL,
  `meta_value` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_options`
--

INSERT INTO `df_options` (`id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(10, 'phone_number', '["+44 121 295 1116","+27 115 495 606"]', '2019-07-02 10:46:01', '0000-00-00 00:00:00'),
(11, 'whats_app_number', '["+44 770 3838 304","+27 730 702 701"]', '2019-07-02 10:46:01', '0000-00-00 00:00:00'),
(12, 'header_logo', 'logo_1562139651.png', '2019-07-03 07:40:51', '0000-00-00 00:00:00'),
(13, 'footer_logo', 'footer-logo_1562139651.png', '2019-07-03 07:40:51', '0000-00-00 00:00:00'),
(14, 'call_me_btn_text', 'Call Me Back', '2019-07-03 07:19:59', '0000-00-00 00:00:00'),
(15, 'call_me_btn_link', 'contact_us\r\n', '2019-07-03 07:40:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `df_pages`
--

CREATE TABLE `df_pages` (
  `id` int(11) NOT NULL,
  `page_title` text NOT NULL,
  `page_url` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_pages`
--

INSERT INTO `df_pages` (`id`, `page_title`, `page_url`, `created_at`, `updated_at`) VALUES
(1, 'careers', 'career', '2019-06-25 05:22:19', '2019-06-25 08:10:02'),
(2, 'event invite us', 'invite_us', '2019-06-25 08:07:29', '2019-06-25 08:11:24'),
(3, 'diaspora insurance', 'diaspora_insurance', '2019-06-25 12:49:29', '2019-06-25 13:09:18'),
(4, 'medical insurance', 'medical_insurance', '2019-06-26 07:22:01', '2019-06-26 07:31:28'),
(5, 'contact us', 'contact_us\r\n', '2019-06-26 12:09:22', '2019-06-26 12:09:22'),
(6, 'plan benefits', 'plan_benefits', '2019-06-26 13:01:39', '2019-06-26 13:05:39'),
(7, 'plan features', 'plan_features', '2019-06-27 07:03:18', '2019-06-27 07:13:31');

-- --------------------------------------------------------

--
-- Table structure for table `df_page_detail`
--

CREATE TABLE `df_page_detail` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_meta_key` text NOT NULL,
  `page_meta_value` text NOT NULL,
  `lang` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_page_detail`
--

INSERT INTO `df_page_detail` (`id`, `page_id`, `page_meta_key`, `page_meta_value`, `lang`, `created_at`, `updated_at`) VALUES
(8, 2, 'banner_text', 'INVITE US', 'en', '2019-06-25 10:12:19', '2019-07-03 13:06:49'),
(9, 2, 'description', '<p style="box-sizing: border-box; margin: 0px 0px 1.5em; padding: 0px; font-kerning: none; font-family: Poppins, sans-serif; font-size: 1.25em; line-height: 1.8; word-break: break-word; overflow-wrap: break-word; text-align: center;">We want to work with you event organisers because our products are essentially Community oriented so let''s do more together. We can help with sponsorship as long as we see the value value proposition.</p><p style="box-sizing: border-box; margin: 0px 0px 1.5em; padding: 0px; font-kerning: none; font-family: Poppins, sans-serif; font-size: 1.25em; line-height: 1.8; word-break: break-word; overflow-wrap: break-word; text-align: center;">invite us to your upconing event. You can also join our lucarative and rewarding&nbsp;<a style="box-sizing: border-box; margin: 0px; padding: 0px; font-kerning: none; color: #cd7644; text-decoration-line: none; background-color: transparent; transition: all 0.5s ease-in-out 0s; display: inline-block;" href="http://customer-devreview.com/diasporafuneralcashplan/partners&amp;affiliates.html">Partners &amp; Affiliate</a><br style="box-sizing: border-box; margin: 0px; padding: 0px; font-kerning: none;" />programme</p>', 'en', '2019-06-25 10:12:19', '2019-07-03 13:06:51'),
(10, 2, 'form_title', 'Your Event - Invite Us', 'en', '2019-06-25 10:12:19', '2019-07-03 13:06:54'),
(12, 3, 'banner_text', 'DIASPORA INSURANCE', 'en', '2019-06-25 13:07:39', '2019-07-03 13:06:59'),
(13, 3, 'left_block_description', '<p style="box-sizing: border-box; margin: 0px 0px 1.5em; padding: 0px; font-kerning: none; font-family: Poppins, sans-serif; font-size: 14px; color: #03213e; line-height: 1.8; word-break: break-word; overflow-wrap: break-word;">Destiny Finance Ltd T/A Diaspora Insurance is a global consultancy firm headquartered in Birmingham, UK and Sandton, RSA. We specialise in the designing, marketing and distribution of insurance products and risk management solutions targeted at Expatriates and Diasporans living and working in developed markets like UK, Australia, EU, North America & Canada. Our global risk management solutions enables the Diasporans not only to cover themselves but also their families in their countries of origin. In most cases we holistically transfer risk from developing countries to global financial markets.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 1.5em; padding: 0px; font-kerning: none; font-family: Poppins, sans-serif; font-size: 14px; color: #03213e; line-height: 1.8; word-break: break-word; overflow-wrap: break-word;">In the United Kingdom Destiny Finance Ltd T/A Diaspora Insurance is regulated and authorised by the Financial Conduct Authority (FCA) under Registration Number: 795897</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 1.5em; padding: 0px; font-kerning: none; font-family: Poppins, sans-serif; font-size: 14px; color: #03213e; line-height: 1.8; word-break: break-word; overflow-wrap: break-word;">n the South Africa Destiny Finance (Pty) Ltd T/A Diaspora Insurance Consultancy is regulated and authorised by the Financial Sector Conduct Authority (FSCA) under <br style="box-sizing: border-box; margin: 0px; padding: 0px; font-kerning: none;" />Registration Number: 48996</p>', 'en', '2019-06-25 13:07:39', '2019-07-03 13:07:01'),
(15, 1, 'banner_text', '', 'en', '2019-06-26 05:01:19', '2019-07-04 04:44:45'),
(16, 1, 'block1_title', '', 'en', '2019-06-26 05:01:19', '2019-07-04 04:44:45'),
(17, 1, 'block1_description', '', 'en', '2019-06-26 05:01:19', '2019-07-04 04:44:45'),
(18, 1, 'block2_title', '', 'en', '2019-06-26 05:01:19', '2019-07-04 04:44:45'),
(19, 1, 'career_form_title', '', 'en', '2019-06-26 05:01:19', '2019-07-04 04:44:45'),
(20, 1, 'banner_image', 'banner_1561525462.jpg', 'en', '2019-06-26 05:01:19', '2019-07-03 13:06:44'),
(21, 1, 'main_image', 'careers_1561543576.jpg', 'en', '2019-06-26 05:01:19', '2019-07-03 13:06:46'),
(22, 2, 'banner_image', 'banner_1561543555.jpg', 'en', '2019-06-26 06:43:48', '2019-07-03 13:06:57'),
(23, 3, 'right_featured_image', 'insurance_1561532973.jpg', 'en', '2019-06-26 07:03:13', '2019-07-03 13:07:03'),
(24, 3, 'banner_image', 'inner-banner_1561543535.jpg', 'en', '2019-06-26 07:03:27', '2019-07-03 13:07:05'),
(25, 4, 'banner_image', 'inner-banner_1561544112.jpg', 'en', '2019-06-26 07:26:10', '2019-07-03 13:07:07'),
(26, 4, 'banner_text', 'DIASPORA MEDICAL INSURANCE', 'en', '2019-06-26 07:26:12', '2019-07-03 13:07:11'),
(27, 4, 'title', 'Coming Soon', 'en', '2019-06-26 07:26:33', '2019-07-03 13:07:13'),
(28, 4, 'left_block_description', '<p><span style="color: #03213e; font-family: Poppins, sans-serif; font-size: 14px;">We will soon be offering our tailor-made Diaspora Medical Insurance to help you the Diasporan cover your loved ones in Africa on a world-class medical aid scheme.</span></p>', 'en', '2019-06-26 07:26:35', '2019-07-03 13:07:15'),
(29, 4, 'left_featured_image', 'comingsoon_1561544112.png', 'en', '2019-06-26 07:27:11', '2019-07-03 13:07:18'),
(30, 4, 'form_title', 'Register Your Interest', 'en', '2019-06-26 10:13:54', '2019-07-03 13:07:20'),
(31, 5, 'banner_text', 'CONTACT US', 'en', '2019-06-26 12:47:55', '2019-07-03 13:07:22'),
(32, 5, 'banner_image', 'inner-banner_1561553580.jpg', 'en', '2019-06-26 12:47:59', '2019-07-03 13:07:24'),
(33, 5, 'title', 'We are here to support you', 'en', '2019-06-26 12:48:32', '2019-07-03 13:07:26'),
(34, 5, 'form_title', 'Call Me Back', 'en', '2019-06-26 12:48:34', '2019-07-03 13:07:29'),
(35, 6, 'banner_text', 'PLAN BENEFITS', 'en', '2019-06-26 13:04:57', '2019-07-03 13:08:00'),
(36, 6, 'banner_image', 'inner-banner_1561618782.jpg', 'en', '2019-06-26 13:04:57', '2019-07-03 13:08:02'),
(37, 6, 'title', 'The uniquely customised Diaspora Funeral Cash Plan offers a host of benefits which include:', 'en', '2019-06-26 13:05:00', '2019-07-03 13:08:04'),
(38, 6, 'left_featured_image', 'plan_1561618782.png', 'en', '2019-06-26 13:05:00', '2019-07-03 13:08:06'),
(39, 6, 'background_image', 'plan-bg_1561618782.jpg', 'en', '2019-06-27 05:25:23', '2019-07-03 13:08:10'),
(40, 6, 'features', '[{"features":"Immediate US$ pay-out on claim, guaranteed!"},{"features":"Protection without borders, guaranteed!"},{"features":"US$ cover for life, guaranteed!"},{"features":"US$s pay-out on claim, guaranteed!"},{"features":"Free Cover in the future, guaranteed!"},{"features":"Dignified send-off, guaranteed!"},{"features":"Peace of Mind, guaranteed!"},{"features":"Celebrated life, guaranteed!"},{"features":"Bereaving and begging, never again!"},{"features":"Personal and Family''s Dignity, protected!"}]', 'en', '2019-06-27 06:45:05', '2019-07-03 13:08:12'),
(41, 7, 'banner_text', 'PLAN FEATURES', 'en', '2019-06-27 07:05:21', '2019-07-03 13:07:35'),
(42, 7, 'banner_image', 'inner-banner_1561619708.jpg', 'en', '2019-06-27 07:05:21', '2019-07-03 13:07:38'),
(43, 7, 'title', 'The following are some of the key features of the bespoke Diaspora Funeral Cash Plan:', 'en', '2019-06-27 07:05:24', '2019-07-03 13:07:40'),
(44, 7, 'left_featured_image', 'plan1_1561619708.png', 'en', '2019-06-27 07:05:24', '2019-07-03 13:07:43'),
(45, 7, 'background_image', 'plan-bg1_1561619708.jpg', 'en', '2019-06-27 07:06:25', '2019-07-03 13:07:53'),
(46, 7, 'features', '[{"features":"Guaranteed acceptance with no medicals at all,"},{"features":"Immediate US$ cash pay-out on proof of death,"},{"features":"Worldwide cover, it''s a protection without borders,"},{"features":"Permanently US$ denominated policy,"},{"features":"Up to US$20, 000 cover per life,"},{"features":"Body repatriation not mandatory,"},{"features":"Cover as Individual, Family or Group"},{"features":"Whole of life cover, it''s a cover for life,"},{"features":"Cover yourself, Cover your loved ones!"},{"features":"Guaranteed gifted FREE cover in future,"},{"features":"Protect Yourself! Protect Your Family! Your dignified send off - Guaranteed!"}]', 'en', '2019-06-27 07:06:25', '2019-07-03 13:07:57'),
(49, 5, 'address', '', 'en', '2019-06-27 07:26:33', '2019-07-03 13:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `df_plan_type`
--

CREATE TABLE `df_plan_type` (
  `id` int(11) NOT NULL,
  `plan_type` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_plan_type`
--

INSERT INTO `df_plan_type` (`id`, `plan_type`, `created_at`, `updated_at`) VALUES
(1, 'Single', '2019-07-04 10:07:17', '2019-07-04 11:00:07'),
(2, 'Family', '2019-07-04 10:07:18', '2019-07-04 11:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `df_users`
--

CREATE TABLE `df_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_dd` int(11) NOT NULL,
  `date_mm` int(11) NOT NULL,
  `date_yyyy` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mphone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `usertype` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8_unicode_ci,
  `user_country_residency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_country_residency_other` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_general_cash_cover` varchar(100) COLLATE utf8_unicode_ci DEFAULT '0',
  `age` int(11) NOT NULL DEFAULT '0',
  `premium_amount` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `marital` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `dependents` int(11) NOT NULL DEFAULT '0',
  `nationality` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nationality_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `passportnum` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `qualifications` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `income` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `busaddress` longtext COLLATE utf8_unicode_ci,
  `plan_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apply_quote` int(1) NOT NULL DEFAULT '0',
  `introducer_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apply_quote_date` datetime DEFAULT NULL,
  `manager_id` int(11) DEFAULT '0',
  `user_pass_word` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_id` int(11) NOT NULL DEFAULT '0',
  `ref_id_from_registration` int(11) NOT NULL DEFAULT '0' COMMENT 'ref id from registration page',
  `ref_id_assigned_by_aff` int(11) DEFAULT '0',
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_so_interested` int(1) NOT NULL DEFAULT '1' COMMENT '1-interested, 0- Not Interested'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `df_users`
--

INSERT INTO `df_users` (`id`, `name`, `username`, `email`, `password`, `title`, `mname`, `sname`, `date_dd`, `date_mm`, `date_yyyy`, `phone`, `mphone`, `zipcode`, `gender`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`, `user_country_residency`, `user_country_residency_other`, `user_general_cash_cover`, `age`, `premium_amount`, `marital`, `dependents`, `nationality`, `nationality_id`, `passportnum`, `qualifications`, `income`, `currency`, `busaddress`, `plan_type`, `apply_quote`, `introducer_name`, `apply_quote_date`, `manager_id`, `user_pass_word`, `ref_id`, `ref_id_from_registration`, `ref_id_assigned_by_aff`, `address1`, `address2`, `town`, `post_code`, `is_so_interested`) VALUES
(1, 'DFCP', 'superadmin', 'superadmin@gmail.com', '$2y$10$uGBlRUKgbk16FEWpHra7ne/fk7fL6fRE500e/NGK1m76XWjgt/u1u', 'Miss', '', 'SSA', 5, 1, 1945, '', '01212951116', '', '', 'Super Administrator', 0, 1, 25, '2012-02-17 00:11:46', '2019-06-07 13:30:32', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n', 'United Kingdom', '', '5000', 72, '69.05', '', 0, '', '', '', '', '', '', '', 'Family', 0, '', '0000-00-00 00:00:00', 0, '', 0, 0, 0, '', '', '', '', 1),
(2, 'Gurjeevan', 'gurjeevan.kaur@imarkinfotech.com', 'gurjeevan.kaur@imarkinfotech.com', '$2y$10$uGBlRUKgbk16FEWpHra7ne/fk7fL6fRE500e/NGK1m76XWjgt/u1u', 'Miss', '', 'Mutemaringa', 5, 7, 1980, '', '00263784326174', '', 'Male', 'Registered', 0, 1, 18, '2012-06-04 10:48:48', '2012-06-04 10:50:40', '', '\n', 'South Africa', '', '5000', 39, '10.9', 'Single', 10, '', '', '', '', '', '', 'gdfgfdg', 'Family', 1, 'fgfdfg', '2019-07-08 00:00:00', 0, '', 0, 0, 0, '', '', 'gfdgg', 'dfgdfgdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `df_users_quotes`
--

CREATE TABLE `df_users_quotes` (
  `userq_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `policy_id` text NOT NULL,
  `family_type` text,
  `user_type` int(11) NOT NULL,
  `child_id` int(11) DEFAULT '0',
  `relationship` varchar(100) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `sname` varchar(100) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `mphone` varchar(100) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `country_of_residency` varchar(100) NOT NULL,
  `country_of_residency_other` varchar(200) NOT NULL,
  `general_cash_cover` varchar(100) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `premium_amount` varchar(200) NOT NULL,
  `nationality_id` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `df_users_quotes`
--

INSERT INTO `df_users_quotes` (`userq_id`, `user_id`, `policy_id`, `family_type`, `user_type`, `child_id`, `relationship`, `title`, `fname`, `mname`, `sname`, `date_of_birth`, `gender`, `mphone`, `email`, `country_of_residency`, `country_of_residency_other`, `general_cash_cover`, `age`, `premium_amount`, `nationality_id`) VALUES
(1, 2, 'DI2079148', NULL, 0, 0, NULL, 'Miss', 'Gurjeevan', '', 'Mutemaringa', '1980-07-05', 'Male', '00263784326174', 'gurjeevan.kaur@imarkinfotech.com', 'South Africa', '', '10,000', 39, '', '0'),
(9, 2, 'DI2079148', 'nucleus-family', 0, 0, 'Daughter', 'Miss', 'gfdgdf', 'gdfgdfgfd', 'gdfgdf', '1820-03-02', 'female', NULL, NULL, 'Albania', '', '', 199, '', '0'),
(10, 2, 'DI2079148.1', 'other-family', 0, 0, 'Father', 'Mr', 'dfdasf', 'dfdsf', 'fdsfdsf', '1819-03-01', '', NULL, NULL, 'Bahamas The', '', '', 200, '', '0'),
(12, 2, 'DI2079148.2', 'other-family', 0, 0, 'Mother-in-law', 'Miss', 'fdsfdf', 'dff', 'dfsdfsdf', '1820-03-04', '', NULL, NULL, 'Azerbaijan', '', '', 199, '', '0'),
(13, 2, 'DI2079148.3', 'other-family', 0, 0, 'Mother-in-law', 'Miss', 'dfdsf', 'fdf', 'dfdfd', '1821-03-03', '', NULL, NULL, 'Bahamas The', '', '', 198, '', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `df_careers`
--
ALTER TABLE `df_careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_career_form`
--
ALTER TABLE `df_career_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_cash_plan_form`
--
ALTER TABLE `df_cash_plan_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_contact_us_form`
--
ALTER TABLE `df_contact_us_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_continent_residence`
--
ALTER TABLE `df_continent_residence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_countries`
--
ALTER TABLE `df_countries`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `df_country_detail`
--
ALTER TABLE `df_country_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_country_origin`
--
ALTER TABLE `df_country_origin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_country_residence`
--
ALTER TABLE `df_country_residence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_currency_cover`
--
ALTER TABLE `df_currency_cover`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_details_principal_beneficiary`
--
ALTER TABLE `df_details_principal_beneficiary`
  ADD PRIMARY KEY (`benef_id`);

--
-- Indexes for table `df_gender`
--
ALTER TABLE `df_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_insurance_plan`
--
ALTER TABLE `df_insurance_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_invite_us_form`
--
ALTER TABLE `df_invite_us_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_languages`
--
ALTER TABLE `df_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_medical_insurance_interest_form`
--
ALTER TABLE `df_medical_insurance_interest_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_options`
--
ALTER TABLE `df_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_pages`
--
ALTER TABLE `df_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_page_detail`
--
ALTER TABLE `df_page_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_plan_type`
--
ALTER TABLE `df_plan_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_users`
--
ALTER TABLE `df_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usertype` (`usertype`),
  ADD KEY `idx_name` (`name`),
  ADD KEY `gid_block` (`gid`,`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `df_users_quotes`
--
ALTER TABLE `df_users_quotes`
  ADD PRIMARY KEY (`userq_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `df_careers`
--
ALTER TABLE `df_careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_career_form`
--
ALTER TABLE `df_career_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `df_cash_plan_form`
--
ALTER TABLE `df_cash_plan_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `df_contact_us_form`
--
ALTER TABLE `df_contact_us_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `df_continent_residence`
--
ALTER TABLE `df_continent_residence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `df_countries`
--
ALTER TABLE `df_countries`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `df_country_detail`
--
ALTER TABLE `df_country_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `df_country_origin`
--
ALTER TABLE `df_country_origin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `df_country_residence`
--
ALTER TABLE `df_country_residence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `df_currency_cover`
--
ALTER TABLE `df_currency_cover`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `df_details_principal_beneficiary`
--
ALTER TABLE `df_details_principal_beneficiary`
  MODIFY `benef_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_gender`
--
ALTER TABLE `df_gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_insurance_plan`
--
ALTER TABLE `df_insurance_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_invite_us_form`
--
ALTER TABLE `df_invite_us_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `df_languages`
--
ALTER TABLE `df_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_medical_insurance_interest_form`
--
ALTER TABLE `df_medical_insurance_interest_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `df_options`
--
ALTER TABLE `df_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `df_pages`
--
ALTER TABLE `df_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `df_page_detail`
--
ALTER TABLE `df_page_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `df_plan_type`
--
ALTER TABLE `df_plan_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `df_users`
--
ALTER TABLE `df_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16736;
--
-- AUTO_INCREMENT for table `df_users_quotes`
--
ALTER TABLE `df_users_quotes`
  MODIFY `userq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
