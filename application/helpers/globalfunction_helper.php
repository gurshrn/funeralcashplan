<?php

	if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*=========Send email========*/
	
	function sendEmail($param) 
	{
	    $ci = & get_instance();
        $ci->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'smtp.sendgrid.net',
          'smtp_user' => 'gurjeevan',
          'smtp_pass' => 'im@rk123#@',
          'smtp_port' => 587,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));
                
        $ci->email->from('gurjeevan.kaur@imarkinfotech.com', 'Diaspora Funeral');
        $ci->email->to($param['email']); 
        //$ci->email->to('gurjeevan.kaur@imarkinfotech.com'); 

        $ci->email->subject('Registeration verification');
        $ci->email->message($param['url']);
        $result = $ci->email->send();
        return $result;
    }

/*============hash and encrypt password==========*/


	
	function hashPassword($password)
	{
		$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);
        $hash = crypt($password, '$2y$10$' . $salt . '$');
        return $hash;
	}

	function getUserDetailById($userId)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_users');
	    $ci->db->where('id',$userId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}

	function getUserQuoteDetailById($userId)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_users_quotes');
	    $ci->db->where('user_id',$userId);
	    $ci->db->where('relationship',NULL);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}

	function getFamilyQuote($userId)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_users_quotes');
	    $ci->db->where('user_id',$userId);
	    $ci->db->where('family_type','other-family');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get country origin list for home page and register page======*/

	function getCountryOrigin() 
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_country_origin');
	    $ci->db->order_by('df_country_origin.country','ASC');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get country continent list for home page and register page======*/

	function getCountryContinent() 
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_continent_residence');
	    $ci->db->order_by('continent','ASC');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get country residence list for home page and register page======*/

	function getCountries() 
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_countries');
	    $ci->db->order_by('name','ASC');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get Country residence list for home page and register page======*/
	
	function getCountryResidence()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_country_residence');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get Country residence list for home page and register page======*/
	
	function getPlanType()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_plan_type');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=======get insurance plan list for home page and register page======*/
	
	function getInsurancePlan()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_insurance_plan');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*=========get gender===========*/

	function getGender()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_gender');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*==========Check new email exist or not========*/
	
	function checkEmailExist($email)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_users');
	    $ci->db->where('email',$email);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

/*==========Admin add page==========*/

	function addPage($param,$data)
	{
		$ci = & get_instance();
		$ci->db->where('id',$data['pageId']);
		$ci->db->update('df_pages',$param);
		$db_error = $ci->db->error();
		if ($db_error['code'] != 0) 
        {
            return false;
        } 
        else 
        {
        	return true;
        }
    }

/*=====Admin get header,footer and sidebar=====*/

	function headerFooter($id = NULL)
	{
		$data['pageId'] = $id;
		$data['header'] = Modules::run('admin/common/header/index');
		$data['sidebar'] = Modules::run('admin/common/sidebar/index');
		$data['footer'] = Modules::run('admin/common/footer/index');
		return $data;
	}

/*======Admin add page detail when edit page=====*/

    function addPageDetail($param)
    {
    	$ci = & get_instance();
    	foreach($param as $key=>$val)
    	{
    		$data = array(
    			'page_meta_key' => $key,
    			'page_id' => $param['pageId'],
    			'page_meta_value' => $val,
    		);
    		$ci->db->where('page_id',$param['pageId']);
    		$ci->db->where('page_meta_key',$key);
    		$ci->db->update('df_page_detail',$data);
    	}
    }

    function getOptionDetail($metaKey)
	{
		$ci = & get_instance();
		$ci->db->select('meta_value');
	    $ci->db->from('df_options');
	    $ci->db->where('meta_key',$metaKey);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}

	function getPageDetail($metaKey,$pageId,$lang)
	{
		$ci = & get_instance();
		$ci->db->select('page_meta_value');
	    $ci->db->from('df_page_detail');
	    $ci->db->where('page_id',$pageId);
	    $ci->db->where('page_meta_key',$metaKey);
	    $ci->db->where('page_meta_key',$metaKey);
	    $ci->db->where('lang',$lang);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}

	function getPagesInfo($pageName)
	{
		$ci = & get_instance();
		$ci->db->select('df_pages.page_title,df_page_detail.page_meta_key,df_page_detail.page_meta_value');
	    $ci->db->from('df_pages');
	    $ci->db->join('df_page_detail','df_pages.id = df_page_detail.page_id','left');
	    $ci->db->where('page_title',$pageName);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

	function getAllCountryOrigin()
	{
		$ci = & get_instance();
		$ci->db->select('df_country_origin.*');
	    $ci->db->from('df_country_origin');
	    $ci->db->join('df_country_detail','df_country_detail.country_id = df_country_origin.id','inner');
	    $ci->db->group_by('df_country_origin.id');
	    $ci->db->order_by('df_country_origin.country','ASC');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

	function getPagesList()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_pages');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

	function getLanguageList()
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('df_languages');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}

	
?>