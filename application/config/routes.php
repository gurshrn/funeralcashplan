<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$config['modules_locations'] = array(
    APPPATH . 'modules/' => '../modules/',
);
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*===Register module===*/

$route['confirm_email'] = 'register/confirmEmail';

/*====Career module====*/

$route['add_career_form'] = 'career/addCareerForm';

/*===Medical insurance module===*/

$route['add_medical_insurance_interest'] = 'medicalInsurance/addMedicalInsuranceInterest';

/*===Contact Us module===*/

$route['contact_us'] = 'contactUs/index';
$route['add_contact_us_form_detail'] = 'contactUs/addContactUsForm';

/*===Country Module===*/

$route['country-detail/(:any)'] = 'country/index/$1';

/*====Admin(Backend) Route=====*/

$route['admin'] = 'admin/login/login';
$route['admin/logout'] = 'admin/login/login/logout';

/*====admin quote apply routing====*/

$route['blog_detail'] = 'blog/blogDetail';


/*=====User quote routing=======*/

$route['quote_summary'] = 'userQuote/index';
$route['apply_application_form'] = 'userQuote/applyApplicationForm';
$route['add_family_quote'] = 'userQuote/addFamilyQuote';
$route['edit_quote/(:any)'] = 'userQuote/editUserQuote/$1';
$route['edit_quote'] = 'userQuote/editUserQuote';
$route['delete_family_quote'] = 'userQuote/deleteFamilyQuote';
$route['edit_family_quote'] = 'userQuote/editFamilyQuote';
$route['edit_family_quote/(:any)'] = 'userQuote/editFamilyQuote/$1';

$route['admin/applicant'] = 'admin/applicant/applicant';
$route['admin/career/(:any)'] = 'admin/career/career/index/$1';
$route['admin/invite_us/(:any)'] = 'admin/inviteUs/inviteUs/index/$1';
$route['admin/pages'] = 'admin/pages/pages';
$route['add_career_page_detail'] = 'admin/career/career/addCareerPage';
$route['admin/add_invite_us_page_detail'] = 'admin/inviteUs/inviteUs/addEventInvitePage';
$route['admin/diaspora_insurance/(:any)'] = 'admin/diasporaInsurance/diasporaInsurance/index/$1';
$route['admin/add_diaspora_insurance'] = 'admin/diasporaInsurance/diasporaInsurance/addDiasporaInsurance';
$route['admin/medical_insurance/(:any)'] = 'admin/medicalInsurance/medicalInsurance/index/$1';
$route['admin/add_medical_insurance'] = 'admin/medicalInsurance/medicalInsurance/addMedicalInsurance';
$route['admin/contact_us/(:any)'] = 'admin/contactUs/contactUs/index/$1';
$route['admin/add_contact_us'] = 'admin/contactUs/contactUs/addContactUs';
$route['admin/plan_benefits/(:any)'] = 'admin/planBenefits/planBenefits/index/$1';
$route['admin/add_plan_benefits'] = 'admin/planBenefits/planBenefits/addPlanBenefits';
$route['admin/plan_features/(:any)'] = 'admin/planFeatures/planFeatures/index/$1';
$route['admin/add_plan_features'] = 'admin/planFeatures/planFeatures/addPlanFeatures';
$route['admin/country_origin'] = 'admin/countryOrigin/countryOrigin';
$route['admin/add_country_origin'] = 'admin/countryOrigin/countryOrigin/addCountryOrigin';
$route['admin/get-country-by-id'] = 'admin/countryOrigin/countryOrigin/getCountryById';
$route['admin/country-detail/(:any)'] = 'admin/countryOrigin/countryOrigin/countryDetail/$1';
$route['admin/add-country-detail'] = 'admin/countryOrigin/countryOrigin/countryDetail';
$route['admin/options'] = 'admin/options/options';
$route['admin/quote-detail'] = 'admin/quoteDetail/quoteDetail';
$route['admin/quote-listing'] = 'admin/quoteListing/quoteListing';
$route['admin/review'] = 'admin/review/review';
