<!--Forgot Password Popup -->

	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Forgot Password ?</h4>
				</div>
				<div class="modal-body">
					<p>Enter your e-mail address below to reset your password.</p>
					<input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
					<button class="btn btn-theme" type="button">Submit</button>
				</div>
			</div>
		</div>
	</div>
	
<!-- Start Script -->
	
	<script src="<?php echo site_url().'/assets/backend/lib/jquery/jquery.min.js';?>"></script>
	<script src="<?php echo site_url().'/assets/backend/lib/bootstrap/js/bootstrap.min.js';?>"></script>
	<script type="text/javascript" src="<?php echo site_url().'/assets/backend/lib/jquery.backstretch.min.js';?>"></script>
	<script>
		$.backstretch("<?php echo site_url().'/assets/backend/img/login-bg.jpg';?>", {
		  speed: 500
		});
	</script>
</body>
</html>