<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title></title>

  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="<?php echo site_url().'/assets/backend/lib/bootstrap/css/bootstrap.min.css';?>" rel="stylesheet">
  <link href="<?php echo site_url().'/assets/backend/lib/font-awesome/css/font-awesome.css';?>" rel="stylesheet" />
  <link href="<?php echo site_url().'/assets/backend/css/style.css';?>" rel="stylesheet">
  <link href="<?php echo site_url().'/assets/backend/css/style-responsive.css';?>" rel="stylesheet">
</head>
<body>