<?php $this->load->view('backend/include/header.php');?>
	<div id="login-page">
		<div class="container">
			<form class="form-login" action="index.html">
				<h2 class="form-login-heading">sign in now</h2>
				<div class="login-wrap">
					<input type="text" class="form-control" placeholder="User ID" autofocus>
					<br>
					<input type="password" class="form-control" placeholder="Password">
					<label class="checkbox">
						<span class="pull-right">
						<a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
						</span>
					</label>
					<button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
					<hr>
				</div>
			</form>
		</div>
	</div>
<?php $this->load->view('backend/include/footer.php');?>

