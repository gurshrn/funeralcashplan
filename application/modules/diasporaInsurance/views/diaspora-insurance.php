<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');  
    $pageDetail = getPagesInfo('diaspora insurance');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    }
?> 
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/diaspora-insurance/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Diaspora Insurance</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="diaspora-insurance-wrapper section">
            <div class="container">
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-12">
                            <div class="left_sec">
                                <figure>
                                    <img src="<?php echo base_url().'assets/upload/diaspora-insurance/'.$pageDetails['right_featured_image'];?>" alt="featured-thumb" />
                                </figure>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-12">
                            <div class="right_sec"> 
                                <?php echo $pageDetails['left_block_description'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="insurance-tab-wrapper">
                    <ul class="nav nav-tabs">
                        <li>
                            <a href="<?php echo base_url('register');?>">Get A Quote</a>
                        </li>
                        <li>
                            <a class="active" data-toggle="tab" href="#globalrisksolutions">Global Risk Solutions</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#reputableunderwriters">Reputable Underwriters</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#ourpeople">Our People</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#diasporafuneralcashplan">Diaspora Funeral Cash Plan</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        
                        <div id="globalrisksolutions" class="tab-pane fade active show">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-12">
                                    <div class="left_sec">
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/insurance1.jpg';?>" alt="featured-thumb" />
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <div class="right_sec">
                                        <p>As Diaspora Insurance we pride ourselves in providing specialist and bespoke risk solutions to transnational citizens otherwise called Expatriates or Diasporans. United Nations estimates that over 250 million people live outside their countries of birth. Diasporans’ personal and family protection needs be it funeral cover, medical insurance or specialist international students’ medical insurance are diverse and complex. At Diaspora Insurance, your global family protection needs are our sole focus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="reputableunderwriters" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-12">
                                    <div class="left_sec">
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/insurance1.jpg';?>" alt="featured-thumb" />
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <div class="right_sec">
                                        <p>Diaspora Insurance is the distributor of Diaspora Funeral Cash Plan.</p> 

                                        <p>   
                                            Global migration invariably creates peculiar family protection needs. For years the insurance industry has chosen to focus on the mass market ignoring the specialist Diasporas niche market. At Diaspora Insurance we chose to be different and continue to create bespoke risk management solutions that are anchored on the global strengths of our underwriters and re-insurers. Our global capacity means that we can holistically mitigate and transfer from excluded and marginalised communities into global financial markets. We continue to be encouraged and motivated by the impact that our products continue to have in solving some age-old community problems like diaspora deaths.
                                        </p>

                                        <p> 

                                            The Diaspora Funeral Cash Plan is underwritten by Guardrisk International Limited based in Port Louis, Mauritius and re-insured under the banner of the world’s bigger re-insurer, Munich Re Africa based in Johannesburg, South Africa.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="ourpeople" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-12">
                                    <div class="left_sec">
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/insurance1.jpg';?>" alt="featured-thumb" />
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <div class="right_sec">
                                        <p>Diaspora Insurance is structured to work differently. Our focus and remit is to positively impact and change people’s realities through bespoke and global risk management solutions. With two fully operational offices in U.K and South Africa support is never far away. Our people are trained to deliver customer value at every point of contact.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="diasporafuneralcashplan" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-12">
                                    <div class="left_sec">
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/insurance1.jpg';?>" alt="featured-thumb" />
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <div class="right_sec">
                                        <p>DFCP is a bespoke funeral or final expenses policy structured for the Diasporas market. Under the Diaspora Funeral Cash Plan one can cover themselves in the diaspora and cover their loved in their countries of origin. The Diaspora Funeral Cash Plan cover comes as a guaranteed acceptance policy with no intrusive medicals at all. As a no medicals policy, the Diaspora Funeral Cash Plan guarantees claim payout within 24 hours of proof of death. <a href="<?php echo base_url('diasporaFuneralCashPlan');?>">Read More</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');  ;?>