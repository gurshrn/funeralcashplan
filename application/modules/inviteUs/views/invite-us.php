<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index'); 
    $pageDetail = getPagesInfo('event invite us');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    } 
?> 
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/invite-us/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Invite Us</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="inviteus-wrapper section">
            <div class="container">
                <div class="inviteus-content-sec">
                    <p>We want to work with you event organisers because our products are essentially Community oriented so let's do more together. We can help with sponsorship as long as we see the value value proposition.</p>
                    <p>invite us to your upconing event. You can also join our lucarative and rewarding <a href="partners&affiliates.html">Partners & Affiliate</a><br> programme</p>
                </div>
                <div class="custom-form-wrapper">
                    <div class="title-sec">
                        <h4>Your Event  -  Invite Us</h4>
                    </div>
                    <form id="event_invite_us_form" method="post" action="">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap" name="event_name" type="text" value="" placeholder="Name Of event" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <!-- <select>
                                        <option>Date of Event</option>
                                        <option>Date of Event</option>
                                    </select> -->
                                    <input class="form-control" name="event_date" type="text" value="" placeholder="Event Date" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap" name="venue_address" type="text" value="" placeholder="Venue Address" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap" name="event_organiser" type="text" value="" placeholder="Event Organiser" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap" name="venue_capacity" type="text" value="" placeholder="Venue Capacity" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap" name="expected_attendance" type="text" value="" placeholder="Expected Attendance" />
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <label>Contact Details</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" type="email" value="" placeholder="Email" name="email"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control inviteTelPhone" name="country_code_mobile" id="telephone" type="tel" value="" placeholder="Country Code/Mobile" />
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12 upload-image">
                                <div class="form-group relative">
                                    <input type="text" id="uploadFile" placeholder="Attach event Flyer">
                                    <input type="file" name="event_flyer">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Message: e.g. Sponsonship options" name="message" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="button-wrap">
                                    <div class="captcha">
                                        <img src="assets/images/captcha.jpg" alt="captcha" />
                                    </div>
                                    <button type="submit" value="submit">submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
<?php echo Modules::run('common/footer/index');  ;?>

<script>
    var inviteTelPhone = document.querySelector(".inviteTelPhone");
    window.intlTelInput(inviteTelPhone);
</script>