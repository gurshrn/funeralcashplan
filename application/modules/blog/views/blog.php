<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
?>
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/inner-banner.jpg';?>')">
        <div class="container">
            <div class="banner-content">
                <h1>Blog</h1>
            </div>
            <!-- banner-content -->
        </div>
        <!-- container -->
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                </nav>
            </div>
            <!-- container -->
        </div>
        <!-- breadcrumb-wrap -->
        <section class="blog-wrapper section">
            <div class="container">
                <div class="sec-title">
                    <h4>Latest Blogs</h4>
                    <p>We Blog to keep you informed and updated:</p>
                </div>
                <!-- sec-title -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog1.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="blog-single.html">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="<?php echo base_url('blog_detail');?>" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog2.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="blog-single.html">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="<?php echo base_url('blog_detail');?>" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog3.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="blog-single.html">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="<?php echo base_url('blog_detail');?>" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog4.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="<?php echo base_url('blog_detail');?>">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="blog-single.html" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog5.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="blog-single.html">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="<?php echo base_url('blog_detail');?>" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog6.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>22</span>May</span>
                                </div>
                            </div>
                            <h4>
                                <a href="<?php echo base_url('blog_detail');?>">Lorem ipsum dolor sit amet consectetur</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. <a href="<?php echo base_url('blog_detail');?>" class="read-more">Read More...</a></p>
                        </div>
                    </div>
                </div>
                <!-- row -->
                <!-- <div class="more-wrap">
                    <a href="#" class="theme-btn">More Blog</a>
                </div> -->
            </div>
            <!-- container -->
        </section>
<?php echo Modules::run('common/footer/index'); ?>

