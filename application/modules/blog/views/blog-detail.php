<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
?>

	<section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/inner-banner.jpg';?>');">
        <div class="container">
            <div class="banner-content">
                <h1>Blog</h1>
            </div>
            <!-- banner-content -->
        </div>
        <!-- container -->
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog Single</li>
                    </ol>
                </nav>
            </div>
            <!-- container -->
        </div>
        <!-- breadcrumb-wrap -->
        <section class="blog-single-wrapper section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-8">
                        <div class="grid-item">
                            <div class="featured-thumb">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/blog1.jpg';?>');"></figure>
                                <div class="meta-info">
                                    <span><span>11</span>July</span>
                                </div>
                            </div>
                            <h4>Lorem ipsum dolor sit amet consectetur</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere. Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>
                            <p>Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere.</p>
                            <p>Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>
                            <p>Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus. </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12- col-lg-4">
                        <div class="sidebar-sec">
                            <div class="widget widget_search clearfix" id="search-3">
                                <div class="widget-panel-info">
                                    <form id="searchform1" method="get" role="search">
                                        <div class="form-group relative">
                                            <input type="text" value="" id="s" name="s" placeholder="Search" class="form-control">
                                            <button type="submit" class="show" id="searchsubmit">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="widget widget_popularpost">
                                <div class="widget-panel-info">
                                    <h4 class="widget-title">Recent Posts</h4>
                                    <ul>
                                        <li class="recent-post-grid">
                                            <figure class="featured-thumb" style="background-image: url('<?php echo base_url().'assets/frontend/images/widget1.jpg';?>');"></figure>
                                            <div class="content-sec">
                                                <h6>
                                                    <a href="#/">Suspendisse purus</a>
                                                </h6>
                                                <p>Lorem ipsum dolor sit amet, cetur adipiscing...</p>
                                            </div>
                                        </li>
                                        <li class="recent-post-grid">
                                            <figure class="featured-thumb" style="background-image: url('<?php echo base_url().'assets/frontend/images/widget2.jpg';?>');"></figure>
                                            <div class="content-sec">
                                                <h6>
                                                    <a href="#/">Dignissim at nisi</a>
                                                </h6>
                                                <p>Lorem ipsum dolor sit amet, cetur adipiscing...</p>
                                            </div>
                                        </li>
                                        <li class="recent-post-grid">
                                            <figure class="featured-thumb" style="background-image: url('<?php echo base_url().'assets/frontend/images/widget3.jpg';?>');"></figure>
                                            <div class="content-sec">
                                                <h6>
                                                    <a href="#/">Vivamus hendrerit</a>
                                                </h6>
                                                <p>Lorem ipsum dolor sit amet, cetur adipiscing...</p>
                                            </div>
                                        </li>
                                        <li class="recent-post-grid">
                                            <figure class="featured-thumb" style="background-image: url('<?php echo base_url().'assets/frontend/images/widget4.jpg';?>');"></figure>
                                            <div class="content-sec">
                                                <h6>
                                                    <a href="#/">Donec aliquam</a>
                                                </h6>
                                                <p>Lorem ipsum dolor sit amet, cetur adipiscing...</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- row -->
            </div>
            <!-- container -->
        </section>

<?php echo Modules::run('common/footer/index'); ?>