<?php 
	echo Modules::run('common/header/index');
	echo Modules::run('common/sidebar/index');
	$countryOrigin = getCountryOrigin();
	$countryResidence = getCountryResidence();
	$insurancePlan = getInsurancePlan();
	$countryContinent = getCountryContinent();
	$countries = getCountries();
?>

	<section class="banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/banner.jpg';?>');">
		<div class="container">
			<div class="banner-content">
				<div class="banner_left">
					<h1>Diaspora Insurance</h1>
					<h3>Your Peace Of Mind, Guaranteed!</h3>
					<p>We pride ourselves in providing specialist and bespoke risk solutions to transnational citizens otherwise called Expatriates or Diasporans. Your global family protection needs is our business.</p>
					<ul>
						<li>
							<a href="#">
								<img src="<?php echo base_url().'assets/frontend/images/banner-logo.png';?>" alt="logo" />
							</a>
						</li>
						<li>
							<a href="<?php echo base_url('register');?>" class="theme-btn">Get A Quote</a>
						</li>
					</ul>
				</div>
				<div class="banner_right" style="background-image: url('<?php echo base_url()."assets/frontend/assets/images/form-bg.png";?>');">
					<h3>Global Risk Solutions, Locally!</h3>

					<form id="select_country" method="POST" action="<?php echo base_url();?>">
                        <div class="form-group">
                            <select name="country_origin" class="country_origin_select">
                                <option value="">Select county of Origin</option>
                                <?php 
                                    if(count($countryOrigin) > 0)
                                    {
                                        foreach($countryOrigin as $origin)
                                        {
                                            echo '<option value="'.$origin->id.'">'.$origin->country.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group continent_residence_select" style="display:none;">
                            <select name="country_residence" class="continent_residence_option">
                                <option value="">Select Continent of Residence</option>
                                <?php 
                                    if(count($countryResidence) > 0)
                                    {
                                        foreach($countryResidence as $res)
                                        {
                                            echo '<option value="'.$res->id.'">'.$res->country_residence    .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group country_residence_select" style="display:none;">
                            <select name="country_residence" class="country_residence_option">
                                <option value="">Select county of Residence</option>
                                <?php 
                                    if(count($countryResidence) > 0)
                                    {
                                        foreach($countryResidence as $res)
                                        {
                                            echo '<option value="'.$res->id.'">'.$res->country_residence    .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group insurance_plan_select" style="display:none;">
                           <select name="insurance_plan" class="insurance_plan_option">
                                <option value="">Select Insurance Plan</option>
                                <?php 
                                    if(count($insurancePlan) > 0)
                                    {
                                        foreach($insurancePlan as $plan)
                                        {
                                            echo '<option value="'.$plan->id.'">'.$plan->insurance_plan .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="button-wrap quote_select_btn" style="display:none;">
                            <button type="submit" value="submit">Get A Quote</button> 
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</section>
	<div class="main">
		<section class="testimonial-wrapper section testimonial">
			<div class="container">
				<div class="sec-wrap">
					<div class="sec-title">
						<h2>Endorsements & Testimonials</h2>
					</div>
					<p>Our work speeaks for Us. Diaspora Insurance is positively impacting ordinary people's realities.</p>
				</div>
				<div class="row">
					<div class="col-lg-8 col-md-8 col-12">
						<div class="video-wrap">
							<iframe src="https://www.youtube.com/embed/4Xhp8rj440Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-12">
						<div class="right_sec">
							<div class="testimonial-post">
								<div class="video">
									<iframe src="https://www.youtube.com/embed/9F75XajPPIw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<h6>
									<a href="endorsement-and-testimonials.html">Diaspora Funeral Cash Plan - Mr Mumba Chomba endorsement</a>
								</h6>
							</div>
							<div class="testimonial-post">
								<div class="video">
									<iframe src="https://www.youtube.com/embed/ac-e123o728" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<h6>
									<a href="endorsement-and-testimonials.html">Diaspora Funeral Cash Plan - Your Dignified Send-off, Guaranteed</a>
								</h6>
							</div>
							<div class="testimonial-post">
								<div class="video">
									<iframe src="https://www.youtube.com/embed/-Q6AU50dv_0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<h6>
									<a href="endorsement-and-testimonials.html">Mrs Daisy Semba - Diaspora Funeral Cash Plan testimonial</a>
								</h6>
							</div>
							<div class="testimonial-post">
								<div class="video">
									<iframe src="https://www.youtube.com/embed/YPOMQexMIPg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
								<h6>
									<a href="endorsement-and-testimonials.html">Diaspora Funeral Cash Plan Tackles the Diaspora Funeral Protection Crisis</a>
								</h6>
							</div>
							<div class="button-wrap">
								<a href="endorsement-and-testimonials.html" class="theme-btn">Endorsements & Testimonials</a>
							</div>
						</div>
					</div>
			   </div>
			</div>
		</section>
		<section class="news-wrap section">
			<div class="container">
				<div class="sec-wrap">
					<div class="sec-title">
						<h2>In The News</h2>
					</div>
				</div>
				<div class="news-slider owl-carousel">
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news1.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news2.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news3.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news1.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news2.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news3.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
					<div class="news-grid">
						<figure style="background-image: url('<?php echo base_url()."assets/frontend/images/news1.jpg";?>')"></figure>
						<div class="content-sec">
							<h5>
								<a href="news-single.html">AU$75,000 to repatriate Myama from Australia to Zimbabwe?</a>
							</h5>
							<ul>
								<li>
									<a href="#">Tose Gava</a>
								</li>
								<li>
									<a href="#">Thursday 22 June 2017</a>
								</li>
							</ul>
							<p>AUSTRALIA: BEFORE he left for work last Tuesday, Andrew Myama had already laid out a fresh uniform, ready for... <a href="#" class="read-more">Read More</a></p>
						</div>
					</div>
				</div>
				<div class="more-wrap">
					<a href="news-list.html" class="theme-btn">More News</a>
				</div>
			</div>
		</section>
	
<?php echo Modules::run('common/footer/index');?>
    