<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class RegisterModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function addUser($param)
		{
			$this->db->insert('df_users',$param);
			$userId = $this->db->insert_id();
	        $db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            $result['success'] = false;
	        } 
	        else 
	        {
	        	$result['success'] = true;
            	$result['user_id'] = $userId;
            	$result['msg'] = 'Thanks for registering with us. Please check and verify your email to login.';
        	}
        	return $result;
		}
		public function addUserDetail($param)
		{
			$this->db->insert('df_user_details',$param);
			$db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
        	}
        }

        public function login($param)
	    {
	        $this->db->select('*');
	        $this->db->from('df_users');
	        $this->db->where('email', $param['email']);
	        $result = $this->db->get();
	        $result = $result->row();
	        return $result;
	    }
	}
?>

	