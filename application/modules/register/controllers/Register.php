<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {

	function __construct()
	{
		$this->load->model('registerModel','register');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

/*======register user=======*/

	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$email = $_POST['email'];

			/*check email exist or not*/

			$checkEmail = checkEmailExist($email);
			if(count($checkEmail) > 0)
			{
				$result['success'] = false;
            	$result['msg'] = 'Email already exist.';
			}
			else
			{
				/*add user in df_user table*/

				$data = array(
					'username' => $email,
					'email' => $email,
					'password' => hashPassword($_POST['password']),
					'user_type' => 'user'
				);
				$result = $this->register->addUser($data);
				if($result)
				{
					/*add userdetail in df_user_details table*/

					$userdetail = array(
						'user_id' => $result['user_id'],
						'title' => $_POST['title'],
						'fname' => $_POST['fname'],
						'mname' => $_POST['mname'],
						'lname' => $_POST['lname'],
						'phone' => $_POST['phone'],
						'mphone' => $_POST['mphone'],
						'gender' => $_POST['gender'],
						'country_of_origin' => $_POST['country_of_origin'],
						'country_of_residence' => $_POST['country_of_residence'],
						'insurance_plan' => $_POST['insurance_plan'],
					);
					$this->register->addUserDetail($userdetail);

					/*send email for verification*/

					$encryptId = urlencode(base64_encode($result['user_id']));
					$emailParam = array(
						'toEmail' => $email,
						'url' => base_url('/confirm_email?userId='.$encryptId),
					);
					sendEmail($emailParam);
					
					$result['success'] = true;
	            	$result['user_id'] = $result['user_id'];
	            	$result['url'] = base_url('/home');
	            	$result['msg'] = 'Thanks for registering with us. Please check and verify your email to login.';
				}
				else
				{
					$result['success'] = false;
	            	$result['msg'] = 'Error while register.';
				}
			}
			echo json_encode($result);exit;
		}
		$this->load->view('register');
	}

/*============confirm email after register=============*/

	public function confirmEmail()
	{
		if(isset($_GET['userId']))
		{
			$userId = base64_decode($_GET['userId']);
			if($userId != '')
			{
				//$verifyEmail = 
			}
		}
	}

/*===========login user after register============*/

	public function userLogin()
	{
		$results = $this->register->login($_POST);
		if (count($results) > 0) 
		{
			if (password_verify($_POST['password'], $results->password)) 
			{
				$this->session->set_userdata('userId', $results->id);
				$result['success'] = true;
	            $result['success_message'] = 'Login Successfully';
	            $result['url'] = base_url('quote_summary');
			}
			else
			{
				$result['success'] = false;
            	$result['error_message'] = 'Password is wrong,Please check';
			}
		}
		else
		{
			$result['success'] = false;
            $result['error_message'] = 'Username or Password is wrong,Please check';
		}
		echo json_encode($result);exit;
	}
}
