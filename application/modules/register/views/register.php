<?php 
	echo Modules::run('common/header/index');
	echo Modules::run('common/sidebar/index');
    $countryOrigin = getCountryOrigin();
    $countryResidence = getCountryResidence();
    $insurancePlan = getInsurancePlan();
    $gender = getGender();
    $countryContinent = getCountryContinent();
    $countries = getCountries();
?>
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/inner-banner.jpg';?>');">
        <div class="container">
            <div class="banner-content">
                <h1>Register</h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Register</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="register-wrapper section">
            <div class="container">
                <div class="custom-form-wrapper">
                    <div class="title-sec">
                        <h4>Get A Quote</h4>
                    </div>
                    <form id="register_user" method="post" action="<?php echo base_url('register');?>" autocomplete="off">
                        <p>Generate A Quote Now! It's Simple, Easy & Quick! You Could Be Covered In Minutes!</p>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <select name="country_of_origin">
                                        <option value="">Select county of Origin</option>
                                        <?php 
                                            if(count($countryOrigin) > 0)
                                            {
                                                foreach($countryOrigin as $origin)
                                                {
                                                    echo '<option value="'.$origin->id.'">'.$origin->country.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <select name="country_of_residence">
                                        <option value="">Select continent of Residence</option>
                                        <?php 
                                            if(count($countryContinent) > 0)
                                            {
                                                foreach($countryContinent as $res)
                                                {
                                                    echo '<option value="'.$res->id.'">'.$res->continent    .'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <select name="country_of_residence">
                                        <option value="">Select country of Residence</option>
                                        <?php 
                                            if(count($countries) > 0)
                                            {
                                                foreach($countries as $coun)
                                                {
                                                    echo '<option value="'.$coun->name.'">'.$coun->name    .'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <select name="insurance_plan">
                                        <option value="">Select Insurance Plan</option>
                                        <?php 
                                            if(count($insurancePlan) > 0)
                                            {
                                                foreach($insurancePlan as $plan)
                                                {
                                                    echo '<option value="'.$plan->id.'">'.$plan->insurance_plan .'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <select name="title">
                                        <option value="">Title*</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap alphabets" type="text" value="" placeholder="First Name*" name="fname" maxlength="25"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap alphabets" type="text" value="" placeholder="Middle Name" name="mname" maxlength="25"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap alphabets" type="text" value="" placeholder="Last Name*" name="lname" maxlength="25"/>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <label>Date of birth ( dd/mm/yyyy)*</label>
                                <div class="row">
                                   
                                    <div class="col-lg-4 col-md-4 col-12">
                                        <div class="form-group">
                                             <input type="text" id="date" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="date" >
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <select name="gender">
                                        <option value="">Select Gender*</option>
                                        <?php 
                                            if(count($gender) > 0)
                                            {
                                                foreach($gender as $type)
                                                {
                                                    echo '<option value="'.$type->id.'">'.$type->gender_type.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control rPhone" name="phone" type="tel" id="telephone" value="" placeholder="Enter vaild Phone Number" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control rMphone" name="mphone" type="tel" id="telephone1" value="" placeholder="Enter vaild Mobile Number*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control email" id="new_email" name="email" type="email" value="" placeholder="Enter valid Email*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" name="confirm_email" type="email" value="" placeholder="Confirm Email*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" id="new_password" name="password" type="password" value="" placeholder="Enter Password*" minlength="8" maxlength="10"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" name="confirm_password" type="password" value="" placeholder="Confirm Password*" minlength="8" maxlength="10"/>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="button-wrap">
                                    <!--div class="captcha">
										<img src="<?php //echo base_url().'assets/frontend/images/captcha.jpg';?>" alt="captcha" />
                                    </div-->
                                    <div class="g-recaptcha" data-sitekey="6LfZIKoUAAAAAJNP49mvlaYry8SzyLEVaCnhXxbZ" data-badge="inline" data-size="invisible" data-callback="setResponse"></div>
    
                                     <input type="hidden" id="captcha-response" name="captcha-response" />
                                    <button type="submit" value="submit" class="submit-button">submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');?> 
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback" async defer></script> 
<script>
var onloadCallback = function() {
    grecaptcha.execute();
};

function setResponse(response) { 
    document.getElementById('captcha-response').value = response; 
}
</script>