<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserQuote extends MX_Controller {

	function __construct()
	{
		$this->load->model('userQuoteModel','quote');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

/*==========User quote summary============*/

	public function index()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data = headerFooter();
			$data['quoteSummary'] =  $this->quote->getQuoteSummary($sess_id);
			$this->load->view('user-quote',$data);
		}
	}

/*==========Add family member Quote========*/

	public function addFamilyQuote()
	{
		$sess_id = $this->session->userdata('userId');
		$loginUserQuote = getUserQuoteDetailById($sess_id);
		$familyQuote = getFamilyQuote($sess_id);
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if(isset($_POST['relationship']))
			{
				if($_POST['relationship'] == '')
				{
					$relationship = $_POST['family_relation'];
					$gender = $_POST['gender'];
				}
				else
				{
					$relationship = $_POST['relationship'];
					if($relationship == 'Son')
					{
						$gender = 'male';
					}
					else
					{
						$gender = 'female';
					}
				}
			}
			else
			{
				$relationship = $_POST['family_relation'];
				if($_POST['family_relation'] == 'Mother' || $_POST['family_relation'] == 'Mother-in-law')
				{
					$gender='female';
				}
				else
				{
					$gender = 'male';
				}
			}
			
			$dob = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['date'];
			$from = new DateTime($dob);
			$to   = new DateTime('today');
			$age = $from->diff($to)->y;
			$param = array(
				'user_id' => $sess_id,
				'relationship' => $relationship,
				'title' => $_POST['title'],
				'fname' => $_POST['fname'],
				'mname' => $_POST['mname'],
				'sname' => $_POST['sname'],
				'date_of_birth' => $dob,
				'gender' => $gender,
				'country_of_residency' => $_POST['country_of_residency'],
				'age' => $age,
				'family_type' => $_POST['family_type'],
			);
			if($_POST['id'] == '')
			{
				if($_POST['family_type'] == 'nucleus-family')
				{
					$policyId = $loginUserQuote->policy_id;
				}
				else
				{
					if(empty($familyQuote))
					{
						$policyId = $loginUserQuote->policy_id.'.1';
					}
					else
					{
						$countId = count($familyQuote)+1;
						$policyId = $loginUserQuote->policy_id.'.'.$countId;
					}
				}
				$param['policy_id'] = $policyId;
			}
			$id = $_POST['id'];
			$results = $this->quote->addFamily($param,$id);
			if ($results) 
			{
				$result['success'] = true;
	            $result['success_message'] = 'Family member added successfully.';
	            $result['url'] = base_url('quote_summary');
			}
			else
			{
				$result['success'] = false;
	            $result['error_message'] = 'Family member not added successfully.';
			}
			echo json_encode($result);exit; 
		}
		$data = headerFooter();
		$this->load->view('add-family-quote',$data);
	}

	public function editUserQuote($id = NULL)
	{

		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$dob = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['date'];
				$from = new DateTime($dob);
				$to   = new DateTime('today');
				$age = $from->diff($to)->y;
				$param = array(
					'user_id' => $sess_id,
					'title' => $_POST['title'],
					'fname' => $_POST['fname'],
					'sname' => $_POST['sname'],
					'mname' => $_POST['mname'],
					'mphone' => $_POST['mphone'],
					'date_of_birth' => $dob,
					'gender' => $_POST['gender'],
					'email' => $_POST['email'],
					'country_of_residency' => $_POST['country_of_residency'],
					'general_cash_cover' => $_POST['user_general_cash_cover'],
					'age' => $age,
				);
				$response = $this->quote->editUserQuoteForm($param);
				if($response)
				{
					$result['success'] = true;
			    	$result['url'] = base_url('quote_summary');
			    	$result['msg'] = 'Quote updated successfully.';
				}
				else
				{
					$result['success'] = false;
			    	$result['msg'] = 'Quote not updated successfully.';
				}
				echo json_encode($result);exit;
			}
			$data = headerFooter();
			$data['userQuoteDetail'] = $this->quote->getUserDetail($id);
			$this->load->view('edit-user-quote',$data);
		}
	}

	public function editFamilyQuote($id = NULL)
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data = headerFooter();
			$data['familyQuoteDetail'] = $this->quote->getFamilyQuoteDetail($id);
			$this->load->view('edit-family-quote',$data);
		}
	}

	public function deleteFamilyQuote()
	{
		$familyMemId = $_POST['familyId'];
		$response = $this->quote->deleteQuote($familyMemId);
		if($response)
		{
			$result['success'] = true;
	    	$result['url'] = base_url('/quote_summary');
	    	$result['msg'] = 'Quote deleted successfully.';
		}
		else
		{
			$result['success'] = false;
	    	$result['msg'] = 'Quote not deleted successfully.';
		}
		echo json_encode($result);exit;
	}

	public function applyApplicationForm()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data = headerFooter();
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$applicantDetails = array(
					'introducer_name' => $_POST['introducer_name'],
					'marital' => $_POST['marital'],
					'dependents' => $_POST['dependents'],
					'busaddress' => $_POST['busaddress'],
					'address2' => $_POST['addressline_2'],
					'town' => $_POST['town_city'],
					'post_code' => $_POST['postcode'],
					'apply_quote' => 1,
					'apply_quote_date' => date('Y-m-d'),
				);
				$results = $this->quote->addApplicantDetails($applicantDetails);
				if ($results) 
				{
					$principalBenDetails = array(
						'user_id' => $sess_id,
						'title1' => $_POST['title1'],
						'fname1' => $_POST['fname1'],
						'sname1' => $_POST['sname1'],
						'national_id1' => $_POST['national_id1'],
						'relationship1' => $_POST['relationship1'],
						'title2' => $_POST['title2'],
						'fname2' => $_POST['fname2'],
						'sname2' => $_POST['sname2'],
						'relationship2' => $_POST['relationship2'],
						'pastone' => $_POST['pastone'],
						'pastone2' => $_POST['pastone2'],
					);
					$this->quote->addPrincipalBenDetails($principalBenDetails);
					$result['success'] = true;
		            $result['success_message'] = 'Application form submitted successfully.';
		            //$result['url'] = base_url('quote_summary');
				}
				else
				{
					$result['success'] = false;
		            $result['error_message'] = 'Application form not submitted successfully.';
				}
				echo json_encode($result);exit;
			}
			$data['quoteSummary'] =  $this->quote->getQuoteSummary($sess_id);
			$this->load->view('apply-application-form',$data);
		}
	}
	
}
