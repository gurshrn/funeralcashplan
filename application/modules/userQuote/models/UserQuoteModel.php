<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class UserQuoteModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function editUserQuoteForm($param)
		{
			$this->db->select('*');
			$this->db->from('df_users_quotes');
			$this->db->where('user_id',$param['user_id']);
			$this->db->where('relationship',NULL);
			$query = $this->db->get();
		    $result = $query->result();
		    if(count($result) > 0)
		    {
		    	$this->db->where('user_id',$param['user_id']);
		    	$this->db->where('relationship',NULL);
				$this->db->update('df_users_quotes',$param);
		    }
		    else
		    {
		    	$length = 7;
        		$randomString = substr(str_shuffle("0123456789"), 0, $length);
				$policyId = 'DI'.$randomString;
				$param['policy_id'] = $policyId;
		    	$this->db->insert('df_users_quotes',$param);
		    }
			$db_error = $this->db->error();
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
		}

		public function getQuoteSummary($userId)
		{
			$this->db->select('*');
			$this->db->from('df_users_quotes');
			$this->db->where('user_id',$userId);
			$query = $this->db->get();
		    $result = $query->result();
		    return $result;
		}
		public function addFamily($param,$id)
		{
			if($id == '')
			{
				$this->db->insert('df_users_quotes',$param);
			}
			else
			{
				$this->db->where('userq_id',$id);
				$this->db->update('df_users_quotes',$param);
			}
			
			$db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            $result['success'] = false;
	        } 
	        else 
	        {
	        	$result['success'] = true;
            }
        	return $result;
		}
		public function getUserDetail($userId)
		{
			$this->db->select('*');
			$this->db->from('df_users');
			$this->db->where('id',$userId);
			$result = $this->db->get();		
			$result = $result->row();
			return $result;
		}

		public function getFamilyQuoteDetail($id)
		{
			$this->db->select('*');
			$this->db->from('df_users_quotes');
			$this->db->where('userq_id',$id);
			$result = $this->db->get();		
			$result = $result->row();
			return $result;
		}

		public function deleteQuote($familyMemId)
		{
			$this->db->where('userq_id',$familyMemId);
			$this->db->delete('df_users_quotes');
			$db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            $result['success'] = false;
	        } 
	        else 
	        {
	        	$result['success'] = true;
            }
        	return $result;
		}

		public function addApplicantDetails($param)
		{
			$sess_id = $this->session->userdata('userId');
			$this->db->where('id',$sess_id);
			$this->db->update('df_users',$param);
			$db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            $result['success'] = false;
	        } 
	        else 
	        {
	        	$result['success'] = true;
            }
        	return $result;
		}

		public function addPrincipalBenDetails($param)
		{
			$sess_id = $this->session->userdata('userId');
			$this->db->insert('df_details_principal_beneficiary',$param);
			$db_error = $this->db->error();
	        if ($db_error['code'] != 0) 
	        {
	            $result['success'] = false;
	        } 
	        else 
	        {
	        	$result['success'] = true;
            }
        	return $result;
		}

	}
?>

	