<?php 
    echo $header;
    echo $sidebar;
?>
	<section id="main-content">
		<section class="wrapper">
			<div class="row">
				<div class="col-lg-9 main-chart">
                    <div class="border-head">
                        <h3>Application form</h3>
                    </div>
                    <div class="content-sec">
                        <p>As the statements in this application constitute warranties, complete and accurate information must be given. Where there is doubt whether certain facts are material they should be disclosed on this application. Non-disclosure may lead to the company repudiating a claim arising under this contract.</p>
                    </div>
                   <form id="apply_application_form" method="POST" action="<?php echo base_url('apply_application_form');?>">
                        <div class="title">
                            <h3>Introducer Name/Code</h3>
                        </div>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <label>Introducer Name / Code, if any</label>
                                    <input type="text" placeholder="Introducer Name / Code, if any" name="introducer_name"/>
                                </div>
                             </div>
                        </div>
                        <div class="title">
                            <h3>Main/Principal Applicant Details</h3>
                        </div>
                        <div class="row">
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <span class="text">Mr Sandeep  Soni</span>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Marital Status</label>
                                    <select name="marital">
                                        <option value="">-</option>
                                        <option value="Single">Single</option>
							            <option value="Married">Married</option>
							            <option value="Divorced">Divorced</option>
							            <option value="Widowed">Widowed</option>
										<option value="Seperated">Seperated</option>
	                                </select>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>No. of dependent children</label>
                                    
                                    <select name="dependents" class="required">
							            <option value="">--</option>
							            <option value="0">0</option>
							            <option value="1">1</option>
							            <option value="2">2</option>
							            <option value="3">3</option>
							            <option value="4">4</option>
							            <option value="5">5</option>
							            <option value="6">6</option>
							            <option value="7">7</option>
							            <option value="8">8</option>
							            <option value="9">9</option>
							            <option value="10">10</option>
    								</select>
                                   
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Are you Zambian or Foreign living in Zambia?</label>
                                    <select name="national_id1" class="required">
							           <option value="">--</option>
							           <option value="Yes">Yes</option>
							           <option value="No">No</option>
						           </select>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Address Line1</label>
                                    <input type="text" name="busaddress" placeholder="Address Line1" />
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label>Address Line2</label>
                                    <input type="text" name="addressline_2" placeholder="Address Line2" />
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Town/City</label>
                                    <input type="text" placeholder="Town/City" name="town_city"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Country</label>
                                    <input type="text" placeholder="Country" name="country"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Post Code</label>
                                    <input type="text" placeholder="Post Code" name="postcode"/>
                                </div>
                             </div>
                        </div>
                        <?php 
                        	if(!empty($quoteSummary)){
                    			foreach($quoteSummary as $val){
                    				if($val->relationship != ''){
                    	?>
                    					<div class="title">
				                            <h3>
				                            	<?php 
				                            		if($val->relationship == 'Son' || $val->relationship == 'Daughter')
				                            		{
				                            			echo 'Children Details';
				                            		}
				                            		else
				                            		{
				                            			echo $val->relationship.' '.'Details';
				                            		}

				                            	?>
				                            </h3>
				                        </div>
				                        <div class="row">
				                            <div class="col-lg-6 col-md-6 col-12">
				                                <div class="form-group">
				                                    <label>Name</label>
				                                    <span class="text">
				                                    	<?php echo $val->title.' '.ucfirst($val->fname).' '.ucfirst($val->sname); ?>
				                                    </span>
				                                </div>
				                            </div>
				                            <div class="col-lg-6 col-md-6 col-12">
				                                <div class="form-group">
				                                    <label>Date of birth ( dd/mm/yyyy )</label>
				                                    <span class="text">
				                                    	<?php echo date('d/m/Y',strtotime($val->date_of_birth));?>
				                                    </span>
				                                </div>
				                            </div>
				                            <div class="col-lg-6 col-md-6 col-12">
				                                <div class="form-group">
				                                    <label>Relationship</label>
				                                    <span class="text"><?php echo $val->relationship;?></span>
				                                </div>
				                            </div>
				                            <div class="col-lg-6 col-md-6 col-12">
				                                <div class="form-group">
				                                    <label>Country of Residence</label>
				                                    <span class="text"><?php echo $val->country_of_residency;?></span>
				                                </div>
				                            </div>
				                            <div class="col-lg-6 col-md-6 col-12">
				                                <div class="form-group">
				                                    <label>Amount of Funeral Cash Cover</label>
				                                    <span class="text"><?php echo $val->general_cash_cover;?></span>
				                                </div>
				                            </div>
				                        </div>
				        <?php } } } ?>

				        <div class="title">
                            <h3>DETAILS OF PRINCIPAL BENEFICIARY</h3>
                            <p>(to receive proceeds after death or incapacitation of principal life assured)</p>
                        </div>
                        <div class="row">
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Title</label>
                                    <select name="title1">
						            	<option value="">--</option>
						                <option value="Mr">Mr</option>
						                <option value="Mrs">Mrs</option>
						                <option value="Miss">Miss</option>
						                <option value="Ms">Ms</option>
						                <option value="Prof">Prof</option>
						                <option value="Dr">Dr</option>
						                <option value="Rev">Rev</option>
						                <option value="The Hon">The Hon</option>
						            </select>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>First name</label>
                                    <input type="text" placeholder="First Name" name="fname1"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Surname</label>
                                    <input type="text" placeholder="Surname" name="sname1"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Relationship</label>
                                    <input type="text" placeholder="Relationship" name="relationship1"/>
                                </div>
                             </div>
                        </div>
                        
                        
                       	<div class="title">
                            <h3>DETAILS OF SECONDARY BENEFICIARY</h3>
                            <p>(to receive proceeds after death or incapacitation of both principal life assured and principal beneficiary)</p>
                        </div>
                        <div class="row">
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Title</label>
                                    <select name="title2">
						            	<option value="">--</option>
						                <option value="Mr">Mr</option>
						                <option value="Mrs">Mrs</option>
						                <option value="Miss">Miss</option>
						                <option value="Ms">Ms</option>
						                <option value="Prof">Prof</option>
						                <option value="Dr">Dr</option>
						                <option value="Rev">Rev</option>
						                <option value="The Hon">The Hon</option>
						            </select>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>First name</label>
                                    <input type="text" placeholder="First Name" name="fname2"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Surname</label>
                                    <input type="text" placeholder="Surname" name="sname2"/>
                                </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Relationship</label>
                                    <input type="text" placeholder="Relationship" name="relationship2"/>
                                </div>
                             </div>
                        </div>
                        <div class="title">
                            <h3>DETAILS OF PAST FUNERAL/LIFE ASSURANCE</h3>
                        </div>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <label>1. Have you or any of the proposed lives ever proposed or have funeral policies or any other policies with MLife? *</label>
                                    <select name="pastone">
                                        <option name="no">No</option>
                                        <option name="yes">Yes</option>
                                    </select>
                                </div>
                             </div>
                             <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <label>2. Are any proposals for funeral assurance pending with MLife? </label>
                                    <select name="pastone2">
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </div>
                             </div>
                        </div>
                        <div class="title">
                            <h3>DECLARATION BY PRINCIPAL LIFE ASSURED</h3>
                        </div>
                        <div class="content-sec">
                            <p>I declare that this application and any additional statements are true and complete and shall be the basis of the contract between the Company and myself.</p>
                            <p>I, the undersigned fully understand the terms, conditions and benefits of the policy sold to me and acknowledge the advice given and indemnify Zambia on any misrepresentation of such advice.</p>
                            <p>I, the undersigned, fully understand that the Company will not honor any claim arising from war and any act of terrorism.</p>
                            <p>I, the person proposing to affect this policy agree that the declaration made by the Life Assured, together with all other statements and answers are deemed to form part of the proposal and declaration shall be the basis of the Contract of Assurance.</p>
                        </div>
                        <div class="form-check-label check-box-sec checkbox-btn">
                            <label for="checkNow">
                                <input type="checkbox" value="2" id="checkNow" name="termscondition">
                                <span class="new-checkbox"></span>           
                                I understand and accept the <a href="#">terms and conditions</a>
                            </label>
                        </div>
                        <div class="title">
                            <h3>MODE OF PAYMENT</h3>
                        </div>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Premium Payment Method</label>
                                    <select>
                                        <option>--</option>
                                        <option>--</option>
                                    </select>
                                </div>
                             </div>
                             <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <button type="submit">Proceed To Setup Payment Plan</button>
                                </div>
                            </div>
                        </div>
                   </form>
                </div>
            </div>
        </section>
    </section>
<?php echo $footer; ?>