<?php 
    echo $header;
    echo $sidebar;
    $countryRes = getCountries();
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">

				<div class="border-head">
					<h3>Add Family Member/s</h3>
                </div>
                
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="add-nucleus-family">
                            <button class="theme-btn">Nucleus Family</button>
                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="form-group add-other-family">
                            <button class="theme-btn">Other Family</button>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-4 col-md-4 col-12 nucleus-family-select" style="display:none;">
                        <select class="select_family_member">
                            <option value="">Select family member...</option>
                            <option value="Spouse">Add Spouse</option>
                            <option value="Children">Add Children</option>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 other-family-select" style="display:none;">
                        <select class="select_family_member">
                            <option value="">Select family member...</option>
                            <option value="Mother">Mother</option>
                            <option value="Father">Father</option>
                            <option value="Mother-in-law">Mother-in-law</option>
                            <option value="Father-in-law">Father-in-law</option>
                            <option value="Sister">Sister</option>
                            <option value="Brother">Brother</option>
                            <option value="Sister-in-law">Sister-in-law</option>
                            <option value="Brother-in-law">Brother-in-law</option>
                            <option value="Niece">Niece</option>
                            <option value="Nephew">Nephew</option>
                            <option value="Uncle">Uncle</option>
                            <option value="Aunt">Aunt</option>
                            <option value="Other Specify">Other Specify</option>
                        </select>
                    </div>
                </div>
                <form id="add_family_member" method="POST" action="<?php echo base_url('add_family_quote'); ?>">
                    <input type="hidden" name="family_relation" class="family-relation">
                    <input type="hidden" name="family_type" class="family-type">
                	<input type="hidden" name="id">
                   	<div id="add_new_applicant_form" style="display:none">
	                    <div class="title">
	                        <h3 class="family-title"></h3>
	                    </div>
	                    <div class="row">
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Title</label>
	                                <select name="title">
	                                    <option value="">Select title...</option>
	                                    <option value="Mr">Mr</option>
	                                    <option value="Miss">Miss</option>
	                                    <option value="Mrs">Mrs</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>First Name</label>
	                                <input type="text" placeholder="First Name" name="fname">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label>Middle Name</label>
	                                <input type="text" placeholder="Middle Name" name="mname">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Surname Name</label>
	                                <input type="text" placeholder="Surname" name="sname">
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <label><span>*</span>Date Of Birth</label>
	                            <div class="row">
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="date">
                                            <option value="">Select date...</option>
                                            <?php 
                                                for($i=1;$i<=31;$i++){ 
                                                    if($userDetail->date_dd == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="month">
                                            <option value="">Select month...</option>
                                            <?php 
                                                for($m=1; $m<=12; ++$m)
                                                {
                                                    if($userDetail->date_mm == $m)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $m;?>" <?php echo $sel;?>><?php echo date('F', mktime(0, 0, 0, $m, 1));?></option>
                                            <?php  } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                       <select name="year">
                                            <option value="">Select year...</option>
                                            <?php
                                                $firstYear = (int)date('Y') - 200;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    if($userDetail->date_yyyy == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12 app_relationship">
	                            <div class="form-group">
	                                <label><span>*</span>Relationship</label>
	                                <select name="relationship">
	                                    <option value="">Select relationship...</option>
	                                    <option value="Son">Son</option>
	                                    <option value="Daughter">Daughter</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12 app_gender">
	                            <div class="form-group">
	                                <label><span>*</span>Gender</label>
	                                <select name="gender">
	                                    <option value="">Select Gender...</option>
	                                    <option value="female">Female</option>
	                                    <option value="male">Male</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Country Of Residence</label>
	                                <select name="country_of_residency">
                                    <option value="">Select Country of Residence...</option>
                                    <?php 
                                        if(!empty($countryRes))
                                        { 
                                            foreach($countryRes as $val)
                                            {
                                                if($userDetail->user_country_residency == $val->name)
                                                {
                                                    $sel = 'selected="selected"';
                                                }
                                                else
                                                {
                                                    $sel = '';
                                                }
                                                echo '<option value="'.$val->name.'" '.$sel.'>'.$val->name.'</option>';
                                            }
                                        }

                                    ?>
                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <div class="form-group">
	                                <button type="submit">Update Quote</button>
	                            </div>  
	                        </div>
	                    </div>
                    </div>
                </form>
				<br>
                <br>
                <br>
                <div class="border-head">
					<h3>Family plan - Quote summary</h3>
                </div>
                <div class="table-wrapper">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Name</th>
                                <th scope="col">DOB</th>
                                <th scope="col">Relationship</th>
                                <th scope="col">Amount US$</th>
                                <th scope="col">Monthly Premium Amount US$</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		if(!empty($quoteSummary))
                        		{
                        			$i =1;
                        			foreach($quoteSummary as $val)
                        			{
                            ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $val->fname; ?></td>
                                            <td><?php echo $val->date_of_birth; ?></td>
                                            <td>
                                                <?php 
                                                    if($val->relationship != '')
                                                    { 
                                                        echo $val->relationship;
                                                    }
                                                    else
                                                    {
                                                        echo 'Myself';
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo $val->general_cash_cover; ?></td>
                                            <td></td>
                                            <td>
                                                <?php if($val->relationship != ''){ ?>
                                                    <a href="<?php echo base_url('edit_family_quote/'.$val->userq_id);?>">Edit</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo base_url('edit_quote/'.$val->user_id);?>">Edit</a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if($val->relationship != ''){ ?>
                                                    <a href="javscript:void(0);" data-attr="<?php echo $val->userq_id;?>" id="delete_family_quote">Delete</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        </tr>
                        	<?php $i++; } } ?>
                        </tbody>
                    </table>
                    <div class="buttons-wrap">
                        <button type="button"><a href="<?php echo base_url('apply_application_form');?>">Apply Now</a></button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<!-- Delete quote popup -->

<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Quote</h4>
            </div>
            <input type="hidden" class="family_user_id">
            <div class="modal-body">
                <p>Are you sure you want to delete this quote?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success yes-delete-quote">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>

