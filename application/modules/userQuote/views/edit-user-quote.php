<?php 
    echo $header;
    echo $sidebar;
    $countryRes = getCountries();
    $plantype = getPlanType();
    $userDetail = $userQuoteDetail;
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">
				<div class="border-head">
					<h3>Quotes & Apply</h3>
				</div>
				<form id="user_quote" method="POST" action="<?php echo base_url('edit_quote');?>">

                    <input type="hidden" name="email" value="<?php echo $userDetail->email;?>">
                    <input type="hidden" name="gender" value="<?php echo $userDetail->gender;?>">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>Title</label>
                                <select name="title">
                                    <option value="">Select Title...</option>
                                    <option value="Mr" <?php if($userDetail->title == 'Mr'){ echo 'selected="selected"';}?>>Mr</option>
                                    <option value="Miss" <?php if($userDetail->title == 'Miss'){ echo 'selected="selected"';}?>>Miss</option>
                                    <option value="Mrs" <?php if($userDetail->title == 'Mrs'){ echo 'selected="selected"';}?>>Mrs</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>First Name</label>
                                <input type="text" name="fname" placeholder="First Name" value="<?php echo $userDetail->name;?>"/>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" name="mname" placeholder="Middle Name" value="<?php echo $userDetail->mname;?>"/>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>Sur Name</label>
                                <input type="text" name="sname" placeholder="Sur Name" value="<?php echo $userDetail->sname;?>"/>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12">
                            <label><span>*</span>Date Of Birth</label>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select name="date">
                                            <option value="">Select date...</option>
                                            <?php 
                                                for($i=1;$i<=31;$i++){ 
                                                    if($userDetail->date_dd == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select name="month">
                                            <option value="">Select month...</option>
                                            <?php 
                                                for($m=1; $m<=12; ++$m)
                                                {
                                                    if($userDetail->date_mm == $m)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $m;?>" <?php echo $sel;?>><?php echo date('F', mktime(0, 0, 0, $m, 1));?></option>
                                            <?php  } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select name="year">
                                            <option value="">Select year...</option>
                                            <?php
                                                $firstYear = (int)date('Y') - 200;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    if($userDetail->date_yyyy == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label>Home ( include country code )</label>
                                <input type="text" name="phone" placeholder="Phone" value="<?php echo $userDetail->phone;?>"/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label><span>*</span>Mobile ( include country code )</label>
                                <input type="text" name="mphone" placeholder="Mobile" value="<?php echo $userDetail->mphone;?>"/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label><span>*</span>Country Of Residence</label>
                                <select name="country_of_residency">
                                    <option value="">Select Country of Residence...</option>
                                    <?php 
                                        if(!empty($countryRes))
                                        { 
                                            foreach($countryRes as $val)
                                            {
                                                if($userDetail->user_country_residency == $val->name)
                                                {
                                                    $sel = 'selected="selected"';
                                                }
                                                else
                                                {
                                                    $sel = '';
                                                }
                                                echo '<option value="'.$val->name.'" '.$sel.'>'.$val->name.'</option>';
                                            }
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label><span>*</span>Amount Of Funeral cash Cover</label>
                                <select name="user_general_cash_cover">
                                    <option value="">Select Amount Of Funeral cash Cover...</option>
                                    <option value="5000" <?php if($userDetail->user_general_cash_cover == '5000'){ echo 'selected="selected"';}?>>5000</option>
                                    <option value="10,000" <?php if($userDetail->user_general_cash_cover == '10,000'){ echo 'selected="selected"';}?>>10,000</option>
                                    <option value="15,000" <?php if($userDetail->user_general_cash_cover == '15,000'){ echo 'selected="selected"';}?>>15,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label><span>*</span>Plan Type </label>
                                <select name="plan_type">
                                    <option value="">Select Plan type...</option>
                                    <?php 
                                        if(!empty($plantype))
                                        {
                                            foreach($plantype as $plan)
                                            {
                                                if($userDetail->plan_type == $plan->plan_type)
                                                {
                                                    $sel = 'selected="selected"';
                                                }
                                                else
                                                {
                                                    $sel = '';
                                                }
                                                echo '<option value="'.$plan->plan_type.'" '.$sel.'>'.$plan->plan_type.'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            (on family plan you can add Partner, Children up to age of 25, Parents and/or Parents-in-law)
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <button type="submit">Update Quote</button>
                            </div>  
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</section>
</section>
    
<?php echo $footer; ?>

  
