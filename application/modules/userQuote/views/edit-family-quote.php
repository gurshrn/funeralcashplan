<?php 
    echo $header;
    echo $sidebar;
    $countryRes = getCountries();
    if($familyQuoteDetail->relationship == 'Son' || $familyQuoteDetail->relationship == 'Daughter')
    {
        $familyRelation = 'Children';
    }
    else
    {
        $familyRelation = $familyQuoteDetail->relationship;
    }
    $explodeDob = explode("-",$familyQuoteDetail->date_of_birth);
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">

				<div class="border-head">
					<h3>Edit Family Member/s</h3>
                </div>
                <form id="add_family_member" method="POST" action="<?php echo base_url('add_family_quote'); ?>">
                    <input type="hidden" name="family_relation" class="family-relation" value="<?php echo $familyRelation;?>">
                    <input type="hidden" name="family_type" class="family-type" value="<?php echo $familyQuoteDetail->family_type;?>">
                	<input type="hidden" name="id" value="<?php echo $familyQuoteDetail->userq_id;?>">
                   	<div id="add_new_applicant_form">
	                    <div class="title">
	                        <h3 class="family-title"></h3>
	                    </div>
	                    <div class="row">
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Title</label>
	                                <select name="title">
	                                    <option value="">Select title...</option>
	                                    <option value="Mr" <?php if($familyQuoteDetail->title == 'Mr'){ echo 'selected="selected"';}?>>Mr</option>
	                                    <option value="Miss" <?php if($familyQuoteDetail->title == 'Miss'){ echo 'selected="selected"';}?>>Miss</option>
	                                    <option value="Mrs" <?php if($familyQuoteDetail->title == 'Mrs'){ echo 'selected="selected"';}?>>Mrs</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>First Name</label>
	                                <input type="text" placeholder="First Name" name="fname" value="<?php echo $familyQuoteDetail->fname;?>">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label>Middle Name</label>
	                                <input type="text" placeholder="Middle Name" name="mname" value="<?php echo $familyQuoteDetail->mname;?>">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Surname Name</label>
	                                <input type="text" placeholder="Surname" name="sname" value="<?php echo $familyQuoteDetail->sname;?>">
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <label><span>*</span>Date Of Birth</label>
	                            <div class="row">
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="date">
                                            <option value="">Select date...</option>
                                            <?php 
                                                for($i=1;$i<=31;$i++){ 
                                                    if($explodeDob[2] == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="month">
                                            <option value="">Select month...</option>
                                            <?php 
                                                for($m=1; $m<=12; ++$m)
                                                {
                                                    if($explodeDob[1]  == $m)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $m;?>" <?php echo $sel;?>><?php echo date('F', mktime(0, 0, 0, $m, 1));?></option>
                                            <?php  } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                       <select name="year">
                                            <option value="">Select year...</option>
                                            <?php
                                                $firstYear = (int)date('Y') - 200;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    if($explodeDob[0] == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                            
	                        <div class="col-lg-6 col-md-6 col-12 app_relationship" <?php if($familyQuoteDetail->relationship == 'Spouse' || $familyQuoteDetail->relationship == 'Father-in-law' || $familyQuoteDetail->relationship == 'Mother-in-law' || $familyQuoteDetail->relationship == 'Mother' || $familyQuoteDetail->relationship == 'Father'){ echo 'style="display:none"';}?>>
	                            <div class="form-group">
	                                <label><span>*</span>Relationship</label>
	                                <select name="relationship">
	                                    <option value="">Select relationship...</option>
	                                    <option value="Son" <?php if($familyQuoteDetail->relationship == 'Son'){ echo 'selected="selected"';}?>>Son</option>
	                                    <option value="Daughter" <?php if($familyQuoteDetail->relationship == 'Daughter'){ echo 'selected="selected"';}?>>Daughter</option>
	                                </select>
	                            </div>
	                        </div>
                            
                           
	                        <div class="col-lg-6 col-md-6 col-12 app_gender" <?php if($familyQuoteDetail->relationship == 'Son' || $familyQuoteDetail->relationship == 'Daughter' || $familyQuoteDetail->relationship == 'Father-in-law' || $familyQuoteDetail->relationship == 'Mother-in-law' || $familyQuoteDetail->relationship == 'Mother' || $familyQuoteDetail->relationship == 'Father'){ echo 'style="display:none"';}?>>
	                            <div class="form-group">
	                                <label><span>*</span>Gender</label>
	                                <select name="gender">
	                                    <option value="">Select Gender...</option>
	                                    <option value="female" <?php if($familyQuoteDetail->gender == 'female'){ echo 'selected="selected"';}?>>Female</option>
	                                    <option value="male" <?php if($familyQuoteDetail->gender == 'male'){ echo 'selected="selected"';}?>>Male</option>
	                                </select>
	                            </div>
	                        </div>
                            <div class="col-lg-6 col-md-6 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Country Of Residence</label>
	                                <select name="country_of_residency">
                                    <option value="">Select Country of Residence...</option>
                                    <?php 
                                        if(!empty($countryRes))
                                        { 
                                            foreach($countryRes as $val)
                                            {
                                                if($familyQuoteDetail->country_of_residency == $val->name)
                                                {
                                                    $sel = 'selected="selected"';
                                                }
                                                else
                                                {
                                                    $sel = '';
                                                }
                                                echo '<option value="'.$val->name.'" '.$sel.'>'.$val->name.'</option>';
                                            }
                                        }

                                    ?>
                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <div class="form-group">
	                                <button type="submit">Update Quote</button>
	                            </div>  
	                        </div>
	                    </div>
                    </div>
                </form>
				<br>
                <br>
                <br>
            </div>
        </div>
    </section>
</section>
<?php echo $footer; ?>

