<?php 
    echo $header;
    echo $sidebar;
    $countryRes = getCountries();
   
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">

				<div class="border-head">
					<h3>Add Family Member/s</h3>
                </div>
                <form id="add_family_member" method="POST" action="<?php echo base_url('add_family_quote'); ?>">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <select name="select_family_member" class="select_family_member">
                                    <option value="">Select family member...</option>
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Father-in-law">Father-in-law</option>
                                    <option value="Mother-in-law">Mother-in-law</option>
                                </select>
                            </div>
                        </div>
                    </div>
                
                    <input type="hidden" name="family_type" class="family_type">
                    <input type="hidden" name="id">
                    <div id="add_new_applicant_form">
                        <div class="title">
                            <h3 class="family-title"></h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Title</label>
                                    <select name="title">
                                        <option value="">Select title...</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="form-group">
                                    <label><span>*</span>First Name</label>
                                    <input type="text" placeholder="First Name" name="fname">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input type="text" placeholder="Middle Name" name="mname">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Surname Name</label>
                                    <input type="text" placeholder="Surname" name="sname">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <label><span>*</span>Date Of Birth</label>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-12">
                                        <div class="form-group">
                                            <select name="date">
                                            <option value="">Select date...</option>
                                            <?php 
                                                for($i=1;$i<=31;$i++){ 
                                                    if($userDetail->date_dd == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-12">
                                        <div class="form-group">
                                            <select name="month">
                                            <option value="">Select month...</option>
                                            <?php 
                                                for($m=1; $m<=12; ++$m)
                                                {
                                            ?>
                                                    <option value="<?php echo $m;?>"><?php echo date('F', mktime(0, 0, 0, $m, 1));?></option>
                                            <?php  } ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-12">
                                        <div class="form-group">
                                           <select name="year">
                                            <option value="">Select year...</option>
                                            <?php
                                                $firstYear = (int)date('Y') - 200;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                           ?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <label><span>*</span>Country Of Residence</label>
                                    <select name="country_of_residency">
                                    <option value="">Select Country of Residence...</option>
                                    <?php 
                                        if(!empty($countryRes))
                                        { 
                                            foreach($countryRes as $val)
                                            {
                                                echo '<option value="'.$val->name.'">'.$val->name.'</option>';
                                            }
                                        }

                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <button type="submit">Update Quote</button>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				<br>
                <br>
                <br>
            </div>
        </div>
    </section>
</section>
<?php echo $footer; ?>

