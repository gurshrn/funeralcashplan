<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
   
?>
<section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/inner-banner.jpg';?>')">
        <div class="container">
            <div class="banner-content">
                <h1>FAQs</h1>
            </div>
            <!-- banner-content -->
        </div>
        <!-- container -->
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Faqs</li>
                    </ol>
                </nav>
            </div>
            <!-- container -->
        </div>
        <!-- breadcrumb-wrap -->
        <section class="frequantly-wrapper section">
            <div class="container">
                <div class="frequantly-section">
                    <div class="content-sec">
                        <p>Whilst our global risk management solutions are quite simplified to maximise your benefits, we appreciate that you may have a few questions. If your question is still not answered, support is just a call, WhatsApp, text or email away. We are here to support you so please <a href="<?php echo base_url('contactUs');?>">Contact Us</a> anytime.</p>
                    </div>
                    <ul class="nav nav-tabs">
                        <li>
                            <a data-toggle="tab" href="#diasporainsurance">Diaspora Insurance</a>
                        </li>
                        <li>
                            <a class="active" data-toggle="tab" href="#diasporafuneralcashplan">Diaspora Funeral Cash Plan</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="diasporainsurance" class="tab-pane fade">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseOne">
                                                <span><i class="more-less"></i></span>
                                                What Is Diaspora Funeral Cash Plan?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                <span><i class="more-less"></i></span>
                                                Why US$ Cash Cover? 
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                <span><i class="more-less"></i></span>
                                                Why Is Funeral Cash Plan So Important? 
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                <span><i class="more-less"></i></span>
                                                Will The Currency Of Cover (US$) Ever Be Changed?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                <span><i class="more-less"></i></span>
                                                Who Needs Funeral Cash Plan Insurance?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                <span><i class="more-less"></i></span>
                                                Why Should I Have Funeral Cash Plan When I Already Have Life Cover?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- accordian -->
                        </div>
                        <div id="diasporafuneralcashplan" class="tab-pane fade active show">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeveen">
                                                <span><i class="more-less"></i></span>
                                                What Is Diaspora Funeral Cash Plan?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseSeveen" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                <span><i class="more-less"></i></span>
                                                Why US$ Cash Cover? 
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseEight" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                                <span><i class="more-less"></i></span>
                                                Why Is Funeral Cash Plan So Important? 
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTen">
                                                <span><i class="more-less"></i></span>
                                                Will The Currency Of Cover (US$) Ever Be Changed?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseEleeven">
                                                <span><i class="more-less"></i></span>
                                                Who Needs Funeral Cash Plan Insurance?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseEleeven" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="panel-title">
                                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTweleev">
                                                <span><i class="more-less"></i></span>
                                                Why Should I Have Funeral Cash Plan When I Already Have Life Cover?
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTweleev" class="collapse" data-parent="#accordion">
                                        <div class="panel-body">
                                            <p>Diaspora Funeral Cash Plan is a funeral insurance cover that was developed with special consideration of the diaspora communities and it pays out immediate cash to cover funeral and/or body repatriation costs. Upon a valid claim cash is released within 24hrs to the appointed beneficiary. This helps your family to cover the costs of your funeral including repatriation of your body if that is your wish. The average funeral can cost several thousand dollars, and with a funeral plan, your family will have the money on hand to help pay for all these high expenses.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- accordian -->
                        </div>
                    </div>
                    <!-- tab-content -->
                    <div class="more-wrap">
                        <a href="#" class="theme-btn">More FAQs</a>
                    </div>
                </div>
                <!-- frequantly-section -->
            </div>
            <!-- container -->
        </section>
        <!-- frequantly-wrapper -->
       
    </div>
<?php echo Modules::run('common/footer/index'); ?>

