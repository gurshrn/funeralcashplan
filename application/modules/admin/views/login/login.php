<?php echo $header; ?>

<div class="admin-login-wrapper" style="background-image: url('<?php echo base_url().'assets/backend/img/login-bg.jpg';?>')">
    <div class="container">
        <div class="login-content-wrap">
            <h3>Log In</h3>
            <div class="login-form">
                <form method="POST" id="admin_login" action="<?php echo base_url('admin');?>">
                    <div class="form-group">
                        <input type="text" placeholder="Username" name="username"/>
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" name="password"/>
                    </div>
                    <div class="button-wrap">
                        <button type="submit" value="submit">Log In</button>
                    </div>
                </form>
            </div><!-- login-form -->
        </div>
    </div><!-- container -->
</div><!-- admin-login-wrapper -->

<?php echo $footer; ?>