<?php 
    echo $header;
    echo $sidebar;
?>
	<section id="main-content">
		<section class="wrapper">
			<div class="row">
				<div class="col-lg-9 col-md-12 main-chart">
					<div class="border-head">
						<h3>Event Invite Us</h3>
					</div>
					<div class="tab-wrapper">
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#lang">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#lang1">Menu 1</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#lang2">Menu 2</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="lang">
								<form id="add_invite_us_page_detail" method="post" action=<?php echo base_url('admin/add_invite_us_page_detail');?>>
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/invite-us/'.$bannerImage->page_meta_value;?>" alt="your image" />
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php
												$description = getPageDetail('description',$pageId);
												?>
												<label for="exampleFormControlInput1">Description</label>
												<textarea class="form-control description" id="exampleFormControlTextarea1" rows="3" name="block1_description" placeholder="Description"><?php echo $description->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $form_title = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" class="form-control" value="<?php echo $form_title->page_meta_value;?>" name="form_title" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="lang1">
								<form id="add_invite_us_page_detail" method="post" action=<?php echo base_url('admin/add_invite_us_page_detail');?>>
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/invite-us/'.$bannerImage->page_meta_value;?>" alt="your image" />
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php
												$description = getPageDetail('description',$pageId);
												?>
												<label for="exampleFormControlInput1">Description</label>
												<textarea class="form-control description" id="exampleFormControlTextarea1" rows="3" name="block1_description" placeholder="Description"><?php echo $description->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $form_title = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" class="form-control" value="<?php echo $form_title->page_meta_value;?>" name="form_title" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="lang2">
								<form id="add_invite_us_page_detail" method="post" action=<?php echo base_url('admin/add_invite_us_page_detail');?>>
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/invite-us/'.$bannerImage->page_meta_value;?>" alt="your image" />
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php
												$description = getPageDetail('description',$pageId);
												?>
												<label for="exampleFormControlInput1">Description</label>
												<textarea class="form-control description" id="exampleFormControlTextarea1" rows="3" name="block1_description" placeholder="Description"><?php echo $description->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $form_title = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" class="form-control" value="<?php echo $form_title->page_meta_value;?>" name="form_title" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- tab-wrapper -->
				</div>
			</div>
		</section>
	</section>
<?php echo $footer; ?>
<script>
    tinymce.init({ 
      selector:'.description',
      height: 200
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#banner_image').css('display','block');
          $('#banner_image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".banner_image").change(function() {
      readURL(this);
    });
</script>
  
