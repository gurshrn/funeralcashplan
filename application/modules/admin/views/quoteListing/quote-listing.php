<?php 
    echo $header;
    echo $sidebar;
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">
				<div class="border-head">
					<h3>Quotes & Apply</h3>
				</div>
				<form>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>Title</label>
                                <select>
                                    <option>Mr</option>
                                    <option>Miss</option>
                                    <option>Mrs</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>First Name</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="form-group">
                                <label><span>*</span>Surname Name</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12">
                            <label><span>*</span>Date Of Birth</label>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select>
                                            <option>19</option>
                                            <option>20</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select>
                                            <option>June</option>
                                            <option>July</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-12">
                                    <div class="form-group">
                                        <select>
                                            <option>1991</option>
                                            <option>1992</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label>Home</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label><span>*</span>Mobile</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <label>Country Of Residence</label>
                                <select>
                                    <option>United kingdom</option>
                                    <option>United kingdom</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Ammount Of Funeral cash Cover</label>
                                <select>
                                    <option>10,000</option>
                                    <option>15,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Plan Type</label>
                                <select>
                                    <option>Family Plan</option>
                                    <option>Family Plan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <button type="submit">Update Quote</button>
                            </div>  
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</section>
</section>
    
<?php echo $footer; ?>

  
