<?php 
    echo $header;
    echo $sidebar;
    $countryRes = getCountries();
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">

				<div class="border-head">
					<h3>Add Family Member/s</h3>
                </div>
                <form id="add_new_applicant">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="add-new-applicant">
                                <button class="theme-btn">Add New Applicant</button>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="form-group">
                                <select name="select_family_member" class="select_family_member">
                                    <option value="">Select family member...</option>
                                    <option value="Spouse">Add Spouse</option>
                                    <option value="Children">Add Children</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                <form id="add_family_member" method="POST" action="<?php echo base_url('admin/add_family_member'); ?>">
                	<input type="hidden" name="family_type" class="family_type">
                   	<div id="add_new_applicant_form" style="display:none">
	                    <div class="title">
	                        <h3 class="family-title"></h3>
	                    </div>
	                    <div class="row">
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Title</label>
	                                <select name="title">
	                                    <option value="">Select title...</option>
	                                    <option value="Mr">Mr</option>
	                                    <option value="Miss">Miss</option>
	                                    <option value="Mrs">Mrs</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>First Name</label>
	                                <input type="text" placeholder="First Name" name="fname">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label>Middle Name</label>
	                                <input type="text" placeholder="Middle Name" name="mname">
	                            </div>
	                        </div>
	                        <div class="col-lg-3 col-md-3 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Surname Name</label>
	                                <input type="text" placeholder="Surname" name="sname">
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <label><span>*</span>Date Of Birth</label>
	                            <div class="row">
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="date">
                                            <option value="">Select date...</option>
                                            <?php 
                                                for($i=1;$i<=31;$i++){ 
                                                    if($userDetail->date_dd == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                        <select name="month">
                                            <option value="">Select month...</option>
                                            <?php 
                                                for($m=1; $m<=12; ++$m)
                                                {
                                                    if($userDetail->date_mm == $m)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $m;?>" <?php echo $sel;?>><?php echo date('F', mktime(0, 0, 0, $m, 1));?></option>
                                            <?php  } ?>
                                        </select>
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-4 col-12">
	                                    <div class="form-group">
	                                       <select name="year">
                                            <option value="">Select year...</option>
                                            <?php
                                                $firstYear = (int)date('Y') - 200;
                                                $lastYear = date('Y');
                                                for($i=$firstYear;$i<=$lastYear;$i++)
                                                {
                                                    if($userDetail->date_yyyy == $i)
                                                    {
                                                        $sel = 'selected="selected"';
                                                    }
                                                    else
                                                    {
                                                        $sel = '';
                                                    }
                                            ?>
                                                    <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?></option>
                                            <?php } ?>
                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12 app_relationship">
	                            <div class="form-group">
	                                <label><span>*</span>Relationship</label>
	                                <select name="relationship">
	                                    <option value="">Select relationship...</option>
	                                    <option value="Son">Son</option>
	                                    <option value="Daughter">Daughter</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12 app_gender">
	                            <div class="form-group">
	                                <label><span>*</span>Gender</label>
	                                <select name="gender">
	                                    <option value="">Select Gender...</option>
	                                    <option value="Female">Female</option>
	                                    <option value="Male">Male</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-6 col-md-6 col-12">
	                            <div class="form-group">
	                                <label><span>*</span>Country Of Residence</label>
	                                <select name="country_of_residency">
                                    <option value="">Select Country of Residence...</option>
                                    <?php 
                                        if(!empty($countryRes))
                                        { 
                                            foreach($countryRes as $val)
                                            {
                                                if($userDetail->user_country_residency == $val->name)
                                                {
                                                    $sel = 'selected="selected"';
                                                }
                                                else
                                                {
                                                    $sel = '';
                                                }
                                                echo '<option value="'.$val->name.'" '.$sel.'>'.$val->name.'</option>';
                                            }
                                        }

                                    ?>
                                </select>
	                            </div>
	                        </div>
	                        <div class="col-lg-12 col-md-12 col-12">
	                            <div class="form-group">
	                                <button type="submit">Update Quote</button>
	                            </div>  
	                        </div>
	                    </div>
                    </div>
                </form>
				<br>
                <br>
                <br>
                <div class="border-head">
					<h3>Family plan - Quote summary</h3>
                </div>
                <div class="table-wrapper">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Name</th>
                                <th scope="col">DOB</th>
                                <th scope="col">Amount US$</th>
                                <th scope="col">Monthly Premium Amount US$</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		if(!empty($quoteSummary))
                        		{
                        			$i =1;
                        			foreach($quoteSummary as $val)
                        			{
                        				echo '<tr>
				                                <td>'.$i.'</td>
				                                <td>'.$val->fname.'</td>
				                                <td>'.date("d/m/Y",strtotime($val->date_of_birth)).'</td>
				                                <td>'.$val->general_cash_cover.'</td>
				                                <td></td>
				                                <td>
				                                    <a href="'.base_url('admin/edit_quote/'.$val->user_id).'">Edit</a>
				                                </td>
				                                <td></td>
				                            </tr>';
				                        $i++;
                        			}
                        		}
							?>
                        </tbody>
                    </table>
                    <div class="buttons-wrap">
                        <button type="submit">Apply Now</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
    

<?php echo $footer; ?>

<script>
	jQuery(document).on('change','.select_family_member',function(){
		var member = jQuery(this).val();
		if(member != '')
		{
			jQuery('#add_new_applicant_form').css('display','block');
			jQuery('.family-title').html('Add'+' '+member+' '+'Details');
			jQuery('.family_type').val(member);
			if(member == 'Children')
			{
				jQuery('.app_gender').css('display','none');
				jQuery('.app_relationship').css('display','block');
			}
			if(member == 'Spouse')
			{
				jQuery('.app_relationship').css('display','none');
				jQuery('.app_gender').css('display','block');
			}
		}
		else
		{
			jQuery('#add_new_applicant_form').css('display','none');
		}
	});
</script>
