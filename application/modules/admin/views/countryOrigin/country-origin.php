<?php 
	echo $header;
	echo $sidebar;
	$country = getCountryOrigin();
?>
	<section id="main-content">
      	<section class="wrapper">
	        <div class="row">
	          	<div class="col-lg-9 main-chart">
	            	<div class="border-head">
	              		<h3>Country Origin</h3>
	            	</div>
	            	<div class="container">
						<input type="button" value="Add" id="add_country_origin">
	             		<table class="table table-bordered">
						    <thead>
						      	<tr>
						        	<th>Country</th>
						        	<th>Action</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if(!empty($country))
						    		{
						    			foreach($country as $val)
						    			{
						    				echo '<tr>';
						    				echo '<td>'.$val->country.'</td>';
						    				echo '<td>
						    						<a href="javascript:void(0)" class="edit_country" data-attr="'.$val->id.'">Edit</a>
						    						<a href="'.base_url('admin/country-detail/'.$val->id).'">Add Country Info</a></td>';
						    				echo '</tr>';
						    			}
						    		}
						    		else
						    		{
						    			echo '<tr><td colspan="2">No country found...</td></tr>';
						    		}
						    	?>
						    </tbody>
				  		</table>
					</div>
	      		</div>
	        </div>
  		</section>
  	</section>
<!-- Add country Modal -->

	<div id="country_origin" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
			<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal">&times;</button>
	        		<h4 class="modal-title country-title"></h4>
	      		</div>
	      		<form id="add_country_origin_form" method="POST" action="<?php echo base_url('admin/add_country_origin');?>">
	      			<input type="hidden" name="id" class="country-id">
					<div class="modal-body">
		        		<input type="text" class="form-control country-name" placeholder="Country Origin" name="country">
		      		</div>
		      	
		      		<div class="modal-footer">
		        		<input type="submit" class="btn btn-default country-btn" value="Add">
		      		</div>
		      	</form>
	    	</div>
		</div>
	</div>
<!-- Call footer -->

<?php echo $footer;?>
<script>
	jQuery(document).on('click','#add_country_origin',function(){
		jQuery('#country_origin').modal('show');
		jQuery('.country-title').html('Add Country Origin');
		jQuery('.country-btn').val('Add');
	});
	jQuery(document).on('click','.edit_country',function(){
		jQuery('#country_origin').modal('show');
		jQuery('.country-title').html('Update Country Origin');
		jQuery('.country-btn').val('Update');
		var countryId = jQuery(this).attr('data-attr');
		jQuery.ajax({
            url: site_url+'admin/get-country-by-id',
            type: "POST",
            data:{countryId:countryId},
            dataType: "json",
            success: function(response) 
            {
               jQuery('.country-name').val(response.country);
               jQuery('.country-id').val(response.id);
            }
        });
	});
</script>