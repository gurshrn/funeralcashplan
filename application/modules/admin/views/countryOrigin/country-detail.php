<?php 
    echo $header;
    echo $sidebar;
?>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-9 main-chart">
            
            <div class="border-head">
              <h3>Country Detail</h3>
            </div>

            <form id="add_country_detail" method="post" action=<?php echo base_url('admin/add-country-detail');?> enctype="multipart/form-data">

                <input type="hidden" name="countryId" value="<?php echo $pageId;?>">
                <div class="form-group">

                    <?php //$bannerText = getPageDetail('banner_text',$pageId); ?>

                    <label for="exampleFormControlInput1">Banner Title</label>
                    <input type="text" name="banner_title" class="form-control" value="<?php //echo $bannerText->page_meta_value;?>" placeholder="Banner Title">
                </div>
                <div class="form-group">

                    <?php //$bannerText = getPageDetail('banner_text',$pageId); ?>

                    <label for="exampleFormControlInput1">Banner Sub Title</label>
                    <input type="text" name="banner_sub_title" class="form-control" value="<?php //echo $bannerText->page_meta_value;?>" placeholder="Banner Sub Title">
                </div>
                <div class="form-group">

                    <?php //$title = getPageDetail('title',$pageId); ?>

                    <label for="exampleFormControlInput1">Banner Description</label>
                    <textarea name="title" class="form-control" name="banner_description" placeholder="Banner Description"></textarea>
                </div>
                <div class="form-group">

                  <?php //$bannerImage = getPageDetail('banner_image',$pageId); ?>

                  <label for="exampleFormControlSelect1">Banner Image</label>
                  <input type="file" class="banner_image" name="banner_image">
                  <img id="banner_image" src="<?php //echo base_url().'assets/upload/contact-us/'.$bannerImage->page_meta_value;?>" alt="your image" style="display:none;">
                </div>
                <div class="form-group">

                  <?php //$bannerImage = getPageDetail('banner_image',$pageId); ?>

                  <label for="exampleFormControlSelect1">Banner Logo</label>
                  <input type="file" class="banner_image" name="banner_logo">
                  <img id="banner_image" src="<?php //echo base_url().'assets/upload/contact-us/'.$bannerImage->page_meta_value; ?>" alt="your image" style="display:none;">
                </div>
				<div class="form-group">

                    <?php //$title = getPageDetail('title',$pageId); ?>

                    <label for="exampleFormControlInput1">Country Title</label>
                    <input type="text" name="country_title" class="form-control" value="<?php //echo $title->page_meta_value;?>" placeholder="Country Title">
                </div>
				<div class="form-group">

                    <?php //$formTitle = getPageDetail('form_title',$pageId); ?>

                    <label for="exampleFormControlInput1">Country Description</label>
                    <textarea name="country_description" class="form-control" placeholder="Country Description"></textarea>
                </div>
                <div class="form-group">

                  <?php //$bannerImage = getPageDetail('banner_image',$pageId); ?>

                  <label for="exampleFormControlSelect1">Country Image</label>
                  <input type="file" class="banner_image" name="country_image">
                  <img id="banner_image" src="<?php //echo base_url().'assets/upload/contact-us/'.$bannerImage->page_meta_value;?>" alt="your image" style="display:none;">
                </div>
			 	<div class="form-group">
                    <button type="submit" class="form-control">Submit</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </section>
<?php echo $footer; ?>
<script>
    tinymce.init({ 
      ///selector:'.left_block_description',
      mode : "textareas",
      height: 200
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#banner_image').css('display','block');
          $('#banner_image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".banner_image").change(function() {
      readURL(this);
    });
    $(".banner_image").change(function() {
      readURL(this);
    });
</script>
  
