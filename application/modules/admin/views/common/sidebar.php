<?php 
  $userId = $_SESSION['userId']; 
  $userRole = getUserDetailById($userId);
?>
<body>
  <section id="container">
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <a href="index.html" class="logo">
          <img src="<?php echo base_url().'assets/backend/img/logo.png';?>" />
        </a>
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<?php echo base_url('admin/logout');?>">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse ">
      
        <ul class="sidebar-menu" id="nav-accordion">

          <?php if($userRole->usertype == 'Super Administrator'){ ?>
        
              <li>
                <a class="active" href="<?php echo base_url('admin/pages');?>">
                  <i class="fa fa-file-text-o"></i>
                  <span>Pages</span>
                  </a>
              </li>
              <li>
                <a href="<?php echo base_url('admin/country_origin');?>">

    				      <i class="fa fa-globe"></i>
                  <span>Country of origin</span>
                </a>

              </li>
              <li>
                <a href="<?php echo base_url('admin/options');?>">
    				      <i class="fa fa-cog"></i>
                  <span>Options</span>

                </a>
              </li>

          <?php } if($userRole->usertype == 'Registered'){ ?>
            <li>
              <a href="<?php echo base_url('quote_summary');?>">
                <i class="fa fa-cog"></i>
                <span>Quote Summary</span>
              </a>
            </li>
            
            <!-- <li>
              <a href="<?php echo base_url('admin/applicant');?>">
                <i class="fa fa-cog"></i>
                <span>Applicant</span>
              </a>

            </li>
            <li>
              <a href="<?php echo base_url('admin/quote-detail');?>">
                <i class="fa fa-cog"></i>
                <span>Quote Detail</span>
              </a>

            </li>
            <li>
              <a href="<?php echo base_url('admin/quote-listing');?>">
                <i class="fa fa-cog"></i>
                <span>Quote Listing</span>
              </a>

            </li>
            <li>
              <a href="<?php echo base_url('admin/review');?>">
                <i class="fa fa-cog"></i>
                <span>Review</span>
              </a>

            </li> -->
          <?php } ?>
      </ul>
    </div>
</aside>