
</section>
    <script src="<?php echo base_url().'assets/backend/lib/jquery/jquery.min.js';?>"></script>

    <script src="<?php echo base_url().'assets/backend/lib/bootstrap/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/backend/js/tinymce/js/tinymce/tinymce.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/jquery.validate.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/form-validation-script.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/additional-methods.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/toastr.js';?>"></script>
    <script src="<?php echo base_url().'assets/backend/js/user-quote.js';?>"></script>
    <script src="<?php echo site_url(); ?>assets/plugin/repeater/jquery.repeater.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url().'assets/backend/lib/jquery.dcjqaccordion.2.7.js';?>"></script>
    <script src="<?php echo base_url().'assets/backend/lib/jquery.scrollTo.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/backend/lib/jquery.nicescroll.js';?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/backend/lib/jquery.sparkline.js';?>"></script>
  
    <script src="<?php echo base_url().'assets/backend/lib/common-scripts.js';?>"></script>
    
    <script type="text/javascript" src="<?php echo base_url().'assets/backend/lib/gritter/js/jquery.gritter.js';?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/backend/lib/gritter-conf.js';?>"></script>
    
    <script src="<?php echo base_url().'assets/backend/lib/sparkline-chart.js';?>"></script>
    <script src="<?php echo base_url().'assets/backend/lib/zabuto_calendar.js';?>"></script>
 
    
  
</body>

</html>