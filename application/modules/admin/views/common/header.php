<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Diaspora</title>

    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <link href="<?php echo base_url().'assets/backend/lib/bootstrap/css/bootstrap.min.css';?>" rel="stylesheet">
  
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/zabuto_calendar.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/lib/gritter/css/jquery.gritter.css';?>" />
    <link href="<?php echo base_url().'assets/backend/css/style.css';?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/backend/css/admin.css';?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/backend/css/style-responsive.css';?>" rel="stylesheet">
    <script src="<?php echo base_url().'assets/backend/lib/chart-master/Chart.js';?>"></script>
    <script>
      var site_url = '<?php echo site_url(); ?>';
    </script>
</head>

