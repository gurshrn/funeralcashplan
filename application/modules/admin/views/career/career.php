<?php 
    echo $header;
    echo $sidebar;
    $lang = getLanguageList();
?>
    <section id="main-content">
      	<section class="wrapper">
			<div class="row">
				<div class="col-lg-9 col-md-12 main-chart">
					<div class="border-head">
						<h3>Careers</h3>
					</div>
					<div class="tab-wrapper">
						<ul class="nav nav-tabs">
							<?php 
								$i=0;
								foreach($lang as $val){
							?>
									<li class="nav-item">
										<a class="nav-link <?php if($i == 0){ echo 'active';}?>" data-toggle="tab" href="#lang<?php echo $i;?>"><?php echo $val->language;?></a>
									</li>								

							<?php $i++;} ?>
						</ul>
						<div class="tab-content">
							<?php 
								$i=0;
								foreach($lang as $val){
							?>
									<div class="tab-pane <?php if($i == 0){ echo 'active';}?>" id="lang<?php echo $i;?>">
										<form id="add_career_page_detail" method="post" action="<?php echo base_url('add_career_page_detail');?>" enctype="multipart/form-data">
											<input type="hidden" name="lang" value="<?php echo $val->lang_code;?>">
											<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
											<div class="row">
												<div class="col-lg-12 col-md-12 col-12">
													<div class="form-group">
														<?php $bannerText = getPageDetail('banner_text',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlInput1">Banner Text</label>
														<input type="text" name="banner_text" class="form-control" value="<?php if($bannerText != ''){echo  $bannerText->page_meta_value;}?>" placeholder="Banner Text">
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-12 upload-image">
													<div class="form-group relative">
														<?php $bannerImage = getPageDetail('banner_image',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlSelect1">Banner Image</label>
														<input type="text" id="uploadFile" placeholder="Attach Image">
														<input type="file" class="form-control banner_image">
														<div id="banner_image" style="background-image: url('<?php if($bannerImage != ''){echo base_url().'assets/upload/career/'.$bannerImage->page_meta_value;}?>');"></div>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-12 upload-image">
													<div class="form-group relative">
														<?php $mainImage = getPageDetail('main_image',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlSelect1">Main Image</label>
														<input type="text" id="uploadFile" placeholder="Attach Image">
														<input type="file" class="main_image" name="main_image">
														<div id="banner_image" style="background-image: url('<?php if($mainImage != ''){ echo base_url().'assets/upload/career/'.$mainImage->page_meta_value;}?>');"></div>
													</div>
												</div>
												<div class="col-lg-12 col-md-12 col-12">
													<div class="form-group">
														<?php
														$block1_title = getPageDetail('block1_title',$pageId,$val->lang_code);
														?>
														<label for="exampleFormControlInput1">Block1 Title</label>
														<input type="text" class="form-control" value="<?php if($block1_title != ''){ echo $block1_title->page_meta_value;}?>" name="block1_title" placeholder="Block1 Title">
													</div>
												</div>
												<div class="col-lg-12 col-md-12 col-12">
													<div class="form-group">
														<?php $block1_description = getPageDetail('block1_description',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlInput1">Block1 Description</label>
														<textarea class="form-control block1-description" id="exampleFormControlTextarea1" rows="3" name="block1_description" placeholder="Block1 Description"><?php if($block1_description != ''){ $block1_description->page_meta_value;}?></textarea>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-12">
													<div class="form-group">
														<?php $block2_title = getPageDetail('block2_title',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlInput1">Block2 Title</label>
														<input type="text" class="form-control" value="<?php if($block2_title != ''){ $block2_title->page_meta_value;}?>" name="block2_title" placeholder="Block1 Title">
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-12">
													<div class="form-group">
														<?php $career_form_title = getPageDetail('career_form_title',$pageId,$val->lang_code); ?>
														<label for="exampleFormControlInput1">Career Form Title</label>
														<input type="text" class="form-control" value="<?php if($career_form_title != ''){ $career_form_title->page_meta_value;}?>" name="career_form_title" placeholder="Block1 Title">
													</div>
												</div>
												<div class="col-lg-12 col-md-12 col-12">
													<div class="form-group">
														<button type="submit">Submit</button>
													</div>
												</div>
											</div>
										</form>
									</div>
							<?php $i++;} ?>
							
							
						</div>
					</div>
					<!-- tab-wrapper -->
				</div>
			</div>
			<!-- row -->
    	</section>
  	</section>
<?php echo $footer; ?>
<script>
    tinymce.init({ 
      selector:'.block1-description',
      height: 200,
      /*menubar: false,
      toolbar: "| bold italic"
        //statusbar: false,
       // toolbar: false*/
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#banner_image').css('display','block');
          $('#banner_image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".banner_image").change(function() {
      jQuery(this).attr('name','');
      readURL(this);
    });
</script>
  
