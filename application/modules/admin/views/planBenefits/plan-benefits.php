<?php 
    echo $header;
    echo $sidebar;
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 col-md-12 main-chart">
				<div class="border-head">
					<h3>Plan Benefits</h3>
				</div>
				<div class="tab-wrapper">
					<ul class="nav nav-tabs">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#lang">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#lang1">Menu 1</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#lang2">Menu 2</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="lang">
							<form id="add_career_page_detail" method="post" action=<?php echo base_url('admin/add_plan_benefits');?>>
								<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
											<label for="exampleFormControlInput1">Banner Text</label>
											<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $backgroundImage = getPageDetail('background_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="background_image">
												<div id="background_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$backgroundImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $title = getPageDetail('title',$pageId); ?>
											<label for="exampleFormControlInput1">Title</label>
											<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
										<div class="form-group relative">
											<?php $leftFeaturedImage = getPageDetail('left_featured_image',$pageId); ?>
											<label for="exampleFormControlSelect1">Left Featured Image</label>
											<input type="text" id="uploadFile" placeholder="Attach Image">
											<input type="file" class="banner_image" name="left_featured_image">
											<div id="left_featured_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$leftFeaturedImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<table class="table repeater" >
											<thead>
												<tr>
													<th>
														<label>Features</label>
													</th>
													<th>
														<input data-repeater-create type="button" value="Add"/>
													</th>
												</tr>
											</thead>
											<tbody data-repeater-list="group">
												<?php 
													$features = getPageDetail('features',$pageId);
													$decodeFeatures = json_decode($features->page_meta_value);
													if(!empty($decodeFeatures)) {
														foreach($decodeFeatures as $val) {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" value="'.$val->features.'" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
														}
													}
													else {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<button type="submit">Submit</button>
										</div>  
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane fade" id="lang1">
							<form id="add_career_page_detail" method="post" action=<?php echo base_url('admin/add_plan_benefits');?>>
								<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
											<label for="exampleFormControlInput1">Banner Text</label>
											<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $backgroundImage = getPageDetail('background_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="background_image">
												<div id="background_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$backgroundImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $title = getPageDetail('title',$pageId); ?>
											<label for="exampleFormControlInput1">Title</label>
											<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
										<div class="form-group relative">
											<?php $leftFeaturedImage = getPageDetail('left_featured_image',$pageId); ?>
											<label for="exampleFormControlSelect1">Left Featured Image</label>
											<input type="text" id="uploadFile" placeholder="Attach Image">
											<input type="file" class="banner_image" name="left_featured_image">
											<div id="left_featured_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$leftFeaturedImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<table class="table repeater" >
											<thead>
												<tr>
													<th>
														<label>Features</label>
													</th>
													<th>
														<input data-repeater-create type="button" value="Add"/>
													</th>
												</tr>
											</thead>
											<tbody data-repeater-list="group">
												<?php 
													$features = getPageDetail('features',$pageId);
													$decodeFeatures = json_decode($features->page_meta_value);
													if(!empty($decodeFeatures)) {
														foreach($decodeFeatures as $val) {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" value="'.$val->features.'" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
														}
													}
													else {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<button type="submit">Submit</button>
										</div>  
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane fade" id="lang2">
							<form id="add_career_page_detail" method="post" action=<?php echo base_url('admin/add_plan_benefits');?>>
								<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
											<label for="exampleFormControlInput1">Banner Text</label>
											<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
											<div class="form-group relative">
												<?php $backgroundImage = getPageDetail('background_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="background_image">
												<div id="background_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$backgroundImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<?php $title = getPageDetail('title',$pageId); ?>
											<label for="exampleFormControlInput1">Title</label>
											<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="upload-image">
										<div class="form-group relative">
											<?php $leftFeaturedImage = getPageDetail('left_featured_image',$pageId); ?>
											<label for="exampleFormControlSelect1">Left Featured Image</label>
											<input type="text" id="uploadFile" placeholder="Attach Image">
											<input type="file" class="banner_image" name="left_featured_image">
											<div id="left_featured_image" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$leftFeaturedImage->page_meta_value;?>');"></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<table class="table repeater" >
											<thead>
												<tr>
													<th>
														<label>Features</label>
													</th>
													<th>
														<input data-repeater-create type="button" value="Add"/>
													</th>
												</tr>
											</thead>
											<tbody data-repeater-list="group">
												<?php 
													$features = getPageDetail('features',$pageId);
													$decodeFeatures = json_decode($features->page_meta_value);
													if(!empty($decodeFeatures)) {
														foreach($decodeFeatures as $val) {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" value="'.$val->features.'" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
														}
													}
													else {
														echo '<tr data-repeater-item>
															<td colspan="2">
																<input type="text" class="form-control" name="features" placeholder="Features" required>
																<label for="user_code" class="error userCodeError"></label>
															</td>
														</tr>';
													}
												?>
											</tbody>
										</table>
									</div>
									<div class="col-lg-12 col-md-12 col-12">
										<div class="form-group">
											<button type="submit">Submit</button>
										</div>  
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- tab-wrapper -->
			</div>
		</div>
	</section>
</section>
<?php echo $footer; ?>
<script>
    tinymce.init({ 
      selector:'.left_block_description',
      height: 200
    });

    var repeater = $('.repeater').repeater({
      initEmpty: false,
      show: function () {
        $('.location-email').val(' ');
        $('.locationId').val('');
        $(this).slideDown();
      },
      isFirstItemUndeletable: true
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#banner_image').css('display','block');
          $('#banner_image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".banner_image").change(function() {
      readURL(this);
    });
    $(".banner_image").change(function() {
      readURL(this);
    });
</script>
  
