<?php 
	echo $header;
	echo $sidebar;
?>

	<section id="main-content">
      	<section class="wrapper">
	        <div class="row">
	          	<div class="col-lg-9 col-md-12 main-chart">
	            	<div class="border-head">
	              		<h3>Pages</h3>
	            	</div>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if(!empty($pages))
								{
									foreach($pages as $val)
									{
										echo '<tr>';
										echo '<td>'.ucfirst($val->page_title).'</td>';
										echo '<td>
												<a href="'.base_url('/admin/'.$val->page_url.'/'.$val->id).'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												</td>';
										echo '</tr>';
									}
								}
							?>
						</tbody>
					</table>
	      		</div>
	        </div>
  		</section>
  	</section>
<?php echo $footer;?>