<?php 
    echo $header;
    echo $sidebar;
?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 col-md-12 main-chart">
				<div class="border-head">
					<h3>Options</h3>
				</div>
				<form id="header_footer_options" method="post" action=<?php echo base_url('admin/options');?>>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-12">
							<div class="form-group">
								<?php 
				                    $phone_number = getOptionDetail('phone_number');
				                    $decodePhone = json_decode($phone_number->meta_value); 
				                ?>
								<label for="exampleFormControlInput1">Phone Number</label>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<input type="text" name="phone_number[]" class="form-control" value="<?php echo $decodePhone[0];?>" placeholder="Phone Number">
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<input type="text" name="phone_number[]" class="form-control" value="<?php echo $decodePhone[1];?>" placeholder="Phone Number">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-12">
							<div class="form-group">
								<?php 
				                    $whatsAppNum = getOptionDetail('whats_app_number');
				                    $decodeWhatsAppNum = json_decode($whatsAppNum->meta_value);  
				                ?>
								<label for="exampleFormControlSelect1">WhatsApp Number</label>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<input type="text" name="whats_app_number[]" class="form-control" value="<?php echo $decodeWhatsAppNum[0];?>" placeholder="WhatsApp Number">
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<input type="text" name="whats_app_number[]" class="form-control" value="<?php echo $decodeWhatsAppNum[1];?>" placeholder="WhatsApp Number">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<div class="form-group">
								<?php $callMeText = getOptionDetail('call_me_btn_text'); ?>
								<label for="exampleFormControlInput1">Call me back button text</label>
								<input type="text" name="call_me_btn_text" class="form-control" value="<?php echo $callMeText->meta_value;?>" placeholder="Call me back button text">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<?php $pageList = getPagesList();?>
							<div class="form-group">
								<label for="exampleFormControlInput1">Call me back button link</label>
								<select class="form-control" name="call_me_btn_link">
									<option value="">Select Page...</option>
									<?php
										$callMeLink = getOptionDetail('call_me_btn_link');
										if(!empty($pageList))
										{ 
											foreach($pageList as $val)
											{
												if($callMeLink->meta_value == $val->page_url)
												{
													$sel = 'selected="selected"';
												}
												else
												{
													$sel = '';
												}
												echo '<option value="'.$val->page_url.'"'.$sel.'>'.ucfirst($val->page_title).'</option>';
											}
										}
									?>
									
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<div class="upload-image">
								<div class="form-group relative">

									<?php $headerLogo = getOptionDetail('header_logo'); ?>
									
									<label for="exampleFormControlSelect1">Header Logo</label>
									<input type="text" id="uploadFile" placeholder="Attach Image">
									<input type="file" class="header_logo" name="header_logo">
									<div id="banner_image" class="header-logo-image show-image" style="background-image: url('<?php echo base_url().'assets/upload/options/'.$headerLogo->meta_value;?>');"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<div class="upload-image">
							<div class="form-group relative">

								<?php $footerLogo = getOptionDetail('footer_logo'); ?>

									<label for="exampleFormControlSelect1">Footer Logo</label>
									<input type="text" id="uploadFile" placeholder="Attach Image">
									<input type="file" class="footer_logo" name="footer_logo">
									<div id="banner_image" class="footer-logo-image show-image" style="background-image: url('<?php echo base_url().'assets/upload/options/'.$footerLogo->meta_value;?>');"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-12">
							<div class="form-group">
								<button type="submit">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
  </section>
</section>

<?php echo $footer; ?>
<script>
    
    function headerLogo(input) {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        
        reader.onload = function(e) 
        {
          $('.header-logo-image').css("background-image", "url(" + e.target.result + ")");
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function footerLogo(input) {
      if (input.files && input.files[0]) 
      {
        var reader = new FileReader();
        
        reader.onload = function(e) 
        {
          $('.footer-logo-image').css("background-image", "url(" + e.target.result + ")");
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".header_logo").change(function() {
      headerLogo(this);
    });
    $(".footer_logo").change(function() {
      footerLogo(this);
    });
</script>
  
