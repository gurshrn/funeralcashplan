<?php 
    echo $header;
    echo $sidebar;
?>

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9 main-chart">
				<div class="border-head">
					<h3>Write A Review</h3>
                </div>
                <form>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-group">
                                <label>Message</label>
                                <textarea rows= "5"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12 upload-image">
                            <div class="form-group relative">
                                <label>Upload A File</label>
                                <input type="text" id="uploadFile" placeholder="Attach File">
                                <input type="file" class="form-control banner_image">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-group">
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

                <div class="border-head">
					<h3>Edit Your Detail</h3>
                </div>
                <form>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>User Name</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Your Name</label>
                                <input type="text" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Verify Password</label>
                                <input type="password" placeholder="" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Back-end Language</label>
                                <select>
                                    <option>- Select Language</option>
                                    <option>- Select Language</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Back-end Language</label>
                                <select>
                                    <option>- Front-end Language:</option>
                                    <option>- Front-end Language:</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>User Editor</label>
                                <select>
                                    <option>- Select Editor</option>
                                    <option>- Select Editor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="form-group">
                                <label>Help Site</label>
                                <select>
                                    <option>Local</option>
                                    <option>Local</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-group">
                                <label>Time Zone</label>
                                <select>
                                    <option>(UTC - 12:00)</option>
                                    <option>(UTC - 12:00)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-group">
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

               


                
               

               
				


                


            
			</div>
		</div>
	</section>
</section>
    
<?php echo $footer; ?>

  
