<?php 
    echo $header;
    echo $sidebar;
?>
	<section id="main-content">
		<section class="wrapper">
			<div class="row">
				<div class="col-lg-9 col-md-12 main-chart">
					<div class="border-head">
						<h3>Medical Insurance</h3>
					</div>
					<div class="tab-wrapper">
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#lang">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#lang1">Menu 1</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#lang2">Menu 2</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="lang">
								<form id="add_medical_insurance_page_detail" method="post" action=<?php echo base_url('admin/add_medical_insurance');?> enctype="multipart/form-data">
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/medical-insurance/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $leftFeaImage = getPageDetail('left_featured_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Left Featured Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="right_featured_image" name="left_featured_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/medical-insurance/'.$leftFeaImage->page_meta_value;?>" alt="your image"/>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $title = getPageDetail('title',$pageId); ?>
												<label for="exampleFormControlInput1">Title</label>
												<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
											</div>
											<div class="form-group">
												<?php $leftBlockDescription = getPageDetail('left_block_description',$pageId); ?>
												<label for="exampleFormControlInput1">Left Block Description</label>
												<textarea class="form-control left_block_description" id="exampleFormControlTextarea1" rows="3" name="left_block_description" placeholder="Block1 Description"><?php echo $leftBlockDescription->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $formTitle = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" name="form_title" class="form-control" value="<?php echo $formTitle->page_meta_value;?>" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="lang1">
								<form id="add_medical_insurance_page_detail" method="post" action=<?php echo base_url('admin/add_medical_insurance');?> enctype="multipart/form-data">
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/medical-insurance/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $leftFeaImage = getPageDetail('left_featured_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Left Featured Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="right_featured_image" name="left_featured_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/medical-insurance/'.$leftFeaImage->page_meta_value;?>" alt="your image"/>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $title = getPageDetail('title',$pageId); ?>
												<label for="exampleFormControlInput1">Title</label>
												<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
											</div>
											<div class="form-group">
												<?php $leftBlockDescription = getPageDetail('left_block_description',$pageId); ?>
												<label for="exampleFormControlInput1">Left Block Description</label>
												<textarea class="form-control left_block_description" id="exampleFormControlTextarea1" rows="3" name="left_block_description" placeholder="Block1 Description"><?php echo $leftBlockDescription->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $formTitle = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" name="form_title" class="form-control" value="<?php echo $formTitle->page_meta_value;?>" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="lang2">
								<form id="add_medical_insurance_page_detail" method="post" action=<?php echo base_url('admin/add_medical_insurance');?> enctype="multipart/form-data">
									<input type="hidden" name="pageId" value="<?php echo $pageId;?>">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $bannerText = getPageDetail('banner_text',$pageId); ?>
												<label for="exampleFormControlInput1">Banner Text</label>
												<input type="text" name="banner_text" class="form-control" value="<?php echo $bannerText->page_meta_value;?>" placeholder="Banner Text">
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $bannerImage = getPageDetail('banner_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Banner Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="banner_image" name="banner_image">
												<div id="banner_image" style="background-image: url('<?php echo base_url().'assets/upload/medical-insurance/'.$bannerImage->page_meta_value;?>');"></div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-12 upload-image">
											<div class="form-group relative">
												<?php $leftFeaImage = getPageDetail('left_featured_image',$pageId); ?>
												<label for="exampleFormControlSelect1">Left Featured Image</label>
												<input type="text" id="uploadFile" placeholder="Attach Image">
												<input type="file" class="right_featured_image" name="left_featured_image">
												<img id="banner_image" src="<?php echo base_url().'assets/upload/medical-insurance/'.$leftFeaImage->page_meta_value;?>" alt="your image"/>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $title = getPageDetail('title',$pageId); ?>
												<label for="exampleFormControlInput1">Title</label>
												<input type="text" name="title" class="form-control" value="<?php echo $title->page_meta_value;?>" placeholder="Title">
											</div>
											<div class="form-group">
												<?php $leftBlockDescription = getPageDetail('left_block_description',$pageId); ?>
												<label for="exampleFormControlInput1">Left Block Description</label>
												<textarea class="form-control left_block_description" id="exampleFormControlTextarea1" rows="3" name="left_block_description" placeholder="Block1 Description"><?php echo $leftBlockDescription->page_meta_value;?></textarea>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<?php $formTitle = getPageDetail('form_title',$pageId); ?>
												<label for="exampleFormControlInput1">Form Title</label>
												<input type="text" name="form_title" class="form-control" value="<?php echo $formTitle->page_meta_value;?>" placeholder="Form Title">
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-12">
											<div class="form-group">
												<button type="submit">Submit</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- tab-wrapper -->
				</div>
			</div>
		</section>
	</section>
<?php echo $footer; ?>
<script>
    tinymce.init({ 
      selector:'.left_block_description',
      height: 200
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#banner_image').css('display','block');
          $('#banner_image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".banner_image").change(function() {
      readURL(this);
    });
    $(".banner_image").change(function() {
      readURL(this);
    });
</script>
  
