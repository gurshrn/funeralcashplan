<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class PagesModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function getPages()
		{
			$this->db->select('*');
			$this->db->from('df_pages');
			$result = $this->db->get();		
			$result = $result->result();
			return $result;
		}
	}
?>

	