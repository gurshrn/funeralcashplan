<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class LoginModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function adminLogin($param)
		{
			$this->db->select('*');
	        $this->db->from('df_users');
	        $this->db->where('username', $param['username']);
	        $this->db->where('usertype','Super Administrator');
	        $result = $this->db->get();
	        $result = $result->row();
	        return $result;
        }
	}
?>

	