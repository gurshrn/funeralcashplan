<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class CountryOriginModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function addCountry($param)
		{
			if($param['id'] == '')
			{
				$this->db->insert('df_country_origin',$param);
			}
			else
			{
				$this->db->where('id',$param['id']);
				$this->db->update('df_country_origin',$param);
			}
			
			$db_error = $this->db->error();
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
		}

		public function getCountryName($countryId)
		{
			$this->db->select('*');
		    $this->db->from('df_country_origin');
		    $this->db->where('id',$countryId);
		    $query = $this->db->get();
		    $result = $query->row();
		    return $result;
		}

		public function addCountryDetail($param)
		{
			$data['country_id'] = $param['countryId'];
			foreach($param as $key=>$val)
			{
				$data['country_meta_key'] = $key;
				$data['country_meta_value'] = $val;
				$this->db->insert('df_country_detail',$data);

			}
			$db_error = $this->db->error();
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
			
		}
	}
?>

	