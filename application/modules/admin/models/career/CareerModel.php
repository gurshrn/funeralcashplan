<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class CareerModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function addCareerPage($param,$data)
		{
			$this->db->where('id',$data['pageId']);
			$this->db->update('df_pages',$param);
			$db_error = $this->db->error();
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
        }

        public function addCareerPageDetail($param)
        {
        	foreach($param as $key=>$val)
        	{
        		$data = array(
        			'page_meta_key' => $key,
        			'page_meta_value' => $val,
        		);
        		$this->db->where('page_id',$param['pageId']);
        		$this->db->where('page_meta_key',$key);
        		$this->db->update('df_page_detail',$data);
        	}
        }
	}
?>

	