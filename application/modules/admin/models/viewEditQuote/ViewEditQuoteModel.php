<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class ViewEditQuoteModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function getUserDetail($userId)
		{
			$this->db->select('*');
			$this->db->from('df_users');
			$this->db->where('id',$userId);
			$result = $this->db->get();		
			$result = $result->row();
			return $result;
		}
		public function editUserQuote($param)
		{
			$this->db->select('*');
			$this->db->from('df_users_quotes');
			$this->db->where('user_id',$param['user_id']);
			$this->db->where('relationship',0);
			$query = $this->db->get();
		    $result = $query->result();
		    if(count($result) > 0)
		    {
		    	$this->db->where('user_id',$param['user_id']);
				$this->db->update('df_users_quotes',$param);
		    }
		    else
		    {
		    	$this->db->insert('df_users_quotes',$param);
		    }
			$db_error = $this->db->error();
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
		}

	}
?>

	