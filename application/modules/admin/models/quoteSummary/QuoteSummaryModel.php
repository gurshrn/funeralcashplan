<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class QuoteSummaryModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function getQuoteSummary($userId)
		{
			$this->db->select('*');
			$this->db->from('df_users_quotes');
			$this->db->where('user_id',$userId);
			$query = $this->db->get();
		    $result = $query->result();
		    return $result;
		}

	}
?>

	