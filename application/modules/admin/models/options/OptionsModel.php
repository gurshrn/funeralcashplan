<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class OptionsModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function addOptionsDetail($param)
		{
			foreach($param as $key=>$val)
			{
				$data = array('meta_key'=>$key,'meta_value'=>$val);
				$this->db->select('*');
				$this->db->from('df_options');
				$this->db->where('meta_key',$key);
				$query = $this->db->get();
			    $result = $query->result();
			    if(count($result) == 0)
			    {
			    	$this->db->insert('df_options',$data);
			    	$db_error = $this->db->error();

			    }
			    else
			    {
			    	$this->db->where('meta_key',$key);
					$this->db->update('df_options',$data);
					$db_error = $this->db->error();
				}
			}
			if ($db_error['code'] != 0) 
	        {
	            return false;
	        } 
	        else 
	        {
	        	return true;
            }
		}

		public function getOptionsDetail()
		{
			$this->db->select('*');
			$this->db->from('df_options');
			$query = $this->db->get();
		    $result = $query->result();
		    return $result;
		}
	}
?>

	