<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CountryOrigin extends MX_Controller {

	function __construct()
	{
		$this->load->model('countryOrigin/countryOriginModel','country');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data = headerFooter();
			$this->load->view('countryOrigin/country-origin',$data);
		}
	}

/*===========Add country origin========*/

	public function addCountryOrigin()
	{
		$param = $_POST;
		$response = $this->country->addCountry($param);
		if($response)
		{
			$result['success'] = true;
	    	$result['url'] = base_url('/admin/country_origin');
	    	if($_POST['id'] == '')
	    	{
	    		$msg = 'Add successfully.';
	    	}
	    	else
	    	{
	    		$msg = 'Updated successfully.';
	    	}
	    	$result['msg'] = $msg;
	    }
		else
		{
			$result['success'] = false;
			if($_POST['id'] == '')
	    	{
	    		$msg = 'Error while add.';
	    	}
	    	else
	    	{
	    		$msg = 'Error while update.';
	    	}
	    	$result['msg'] = $msg;
	    }
		echo json_encode($result);exit;
	}

/*========get country origin by id======*/

	public function getCountryById()
	{
		$countryId = $_POST['countryId'];
		$getCountry = $this->country->getCountryName($countryId);
		echo json_encode($getCountry);exit;
	}

/*===========add and view country detail page========*/

	public function countryDetail($id=NULL)
	{
		$data = headerFooter($id);
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if (!empty($_FILES['banner_image']['size'])) 
			{
				$count = count($_FILES['banner_image']['size']);
				$path = 'assets/upload/pages/';
				$imagename = $_FILES['banner_image'];
				$date = time();
				$exps = explode('.', $imagename['name']);
				
				$_FILES['banner_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
				$_FILES['banner_image']['type'] = $imagename['type'];
				$_FILES['banner_image']['tmp_name'] = $imagename['tmp_name'];
				$_FILES['banner_image']['error'] = $imagename['error'];
				$_FILES['banner_image']['size'] = $imagename['size'];
				
				$config['upload_path'] = $path;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$this->upload->do_upload('banner_image');
				$datas = $this->upload->data();
				$name_array = $datas['file_name'];
				$imageName = $name_array;
				$_POST['banner_image'] = $imageName;
			}
			if (!empty($_FILES['banner_logo']['size'])) 
			{
				$count = count($_FILES['banner_logo']['size']);
				$path = 'assets/upload/pages/';
				$imagename = $_FILES['banner_logo'];
				$date = time();
				$exps = explode('.', $imagename['name']);
				
				$_FILES['banner_logo']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
				$_FILES['banner_logo']['type'] = $imagename['type'];
				$_FILES['banner_logo']['tmp_name'] = $imagename['tmp_name'];
				$_FILES['banner_logo']['error'] = $imagename['error'];
				$_FILES['banner_logo']['size'] = $imagename['size'];
				
				$config['upload_path'] = $path;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$this->upload->do_upload('banner_logo');
				$datas = $this->upload->data();
				$name_array = $datas['file_name'];
				$imageName = $name_array;
				$_POST['banner_logo'] = $imageName;
			}
			if (!empty($_FILES['country_image']['size'])) 
			{
				$count = count($_FILES['country_image']['size']);
				$path = 'assets/upload/pages/';
				$imagename = $_FILES['country_image'];
				$date = time();
				$exps = explode('.', $imagename['name']);
				
				$_FILES['country_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
				$_FILES['country_image']['type'] = $imagename['type'];
				$_FILES['country_image']['tmp_name'] = $imagename['tmp_name'];
				$_FILES['country_image']['error'] = $imagename['error'];
				$_FILES['country_image']['size'] = $imagename['size'];
				
				$config['upload_path'] = $path;
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
				$this->upload->do_upload('country_image');
				$datas = $this->upload->data();
				$name_array = $datas['file_name'];
				$imageName = $name_array;
				$_POST['country_image'] = $imageName;
			}
			$response = $this->country->addCountryDetail($_POST);
			if($response)
			{
				$result['success'] = true;
		    	$result['msg'] = 'Submit successfully';
		    	//$result['url'] = 'Submit successfully';
		    }
			else
			{
				$result['success'] = false;
				$result['msg'] = 'Error while submit.';
		    }
			echo json_encode($result);exit;
		}
		$this->load->view('countryOrigin/country-detail',$data);
	}
	
}
