<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuoteSummary extends MX_Controller {

	function __construct()
	{
		$this->load->model('quoteSummary/quoteSummaryModel','quoteSummary');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if($_POST['relationship'] == '')
				{
					$relationship = $_POST['family_type'];
				}
				else
				{
					$relationship = $_POST['relationship'];
				}
				$param = array(
					'user_id' => $sess_id,
					'relationship' => $relationship,
				);
				//print_r($_POST);die;
			}
			$data = headerFooter();
			$data['quoteSummary'] =  $this->quoteSummary->getQuoteSummary($sess_id);
			$this->load->view('quoteSummary/quote-summary',$data);
		}
	}
	
}
