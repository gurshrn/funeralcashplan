<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends MX_Controller {

	function __construct()
	{
		$this->load->model('options/optionsModel','options');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data = headerFooter();
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if (!empty($_FILES['header_logo']['size'])) 
				{
					$count = count($_FILES['header_logo']['size']);
					$path = 'assets/upload/options/';
					$imagename = $_FILES['header_logo'];
					$date = time();
					$exps = explode('.', $imagename['name']);
					
					$_FILES['header_logo']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
					$_FILES['header_logo']['type'] = $imagename['type'];
					$_FILES['header_logo']['tmp_name'] = $imagename['tmp_name'];
					$_FILES['header_logo']['error'] = $imagename['error'];
					$_FILES['header_logo']['size'] = $imagename['size'];
					
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$this->load->library('upload', $config);
					$this->upload->do_upload('header_logo');
					$datas = $this->upload->data();
					$name_array = $datas['file_name'];
					$imageName = $name_array;
					$_POST['header_logo'] = $imageName;
				}

				if (!empty($_FILES['footer_logo']['size'])) 
				{
					$count = count($_FILES['footer_logo']['size']);
					$path = 'assets/upload/medical-insurance/';
					$imagename = $_FILES['footer_logo'];
					$date = time();
					$exps = explode('.', $imagename['name']);
					
					$_FILES['footer_logo']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
					$_FILES['footer_logo']['type'] = $imagename['type'];
					$_FILES['footer_logo']['tmp_name'] = $imagename['tmp_name'];
					$_FILES['footer_logo']['error'] = $imagename['error'];
					$_FILES['footer_logo']['size'] = $imagename['size'];
					
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$this->load->library('upload', $config);
					$this->upload->do_upload('footer_logo');
					$datas = $this->upload->data();
					$name_array = $datas['file_name'];
					$imageName = $name_array;
					$_POST['footer_logo'] = $imageName;
				}
				$_POST['phone_number'] = json_encode($_POST['phone_number']);
				$_POST['whats_app_number'] = json_encode($_POST['whats_app_number']);
				$response = $this->options->addOptionsDetail($_POST);
				if($response)
				{
					$result['success'] = true;
			    	$result['url'] = base_url('/admin/options');
			    	$result['msg'] = 'Updated successfully.';
				}
				else
				{
					$result['success'] = false;
			    	$result['msg'] = 'Error while updated.';
				}
				echo json_encode($result);exit;
			}
			$this->load->view('options/options',$data);
		}
	}
	/*public function addPlanFeatures()
	{
		$param = array('page_title'=>'plan features');

		if (!empty($_FILES['banner_image']['size'])) 
		{
			$count = count($_FILES['banner_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['banner_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['banner_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['banner_image']['type'] = $imagename['type'];
			$_FILES['banner_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['banner_image']['error'] = $imagename['error'];
			$_FILES['banner_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('banner_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['banner_image'] = $imageName;
		}
		if (!empty($_FILES['left_featured_image']['size'])) 
		{
			$count = count($_FILES['left_featured_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['left_featured_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['left_featured_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['left_featured_image']['type'] = $imagename['type'];
			$_FILES['left_featured_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['left_featured_image']['error'] = $imagename['error'];
			$_FILES['left_featured_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('left_featured_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['left_featured_image'] = $imageName;
		}
		if (!empty($_FILES['background_image']['size'])) 
		{
			$count = count($_FILES['background_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['background_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['background_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['background_image']['type'] = $imagename['type'];
			$_FILES['background_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['background_image']['error'] = $imagename['error'];
			$_FILES['background_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('background_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['background_image'] = $imageName;
		}
		$_POST['features'] = json_encode($_POST['group']);
		unset($_POST['group']);
		$response = addPage($param,$_POST);
		if($response)
		{
			$response = addPageDetail($_POST);
			$result['success'] = true;
	    	$result['url'] = base_url('/admin/pages');
	    	$result['msg'] = 'Updated successfully.';
		}
		else
		{
			$result['success'] = false;
	    	$result['msg'] = 'Error while updated.';
		}
		echo json_encode($result);exit;
	}*/
}
