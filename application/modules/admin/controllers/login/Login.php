<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct()
	{
		$this->load->model('login/loginModel','login');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = headerFooter();
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$results = $this->login->adminLogin($_POST);
			if (count($results) > 0) 
			{
				if (password_verify($_POST['password'], $results->password)) 
				{
					$this->session->set_userdata('userId', $results->id);
					$result['success'] = true;
		            $result['success_message'] = 'Login Successfully';
		            $result['url'] = 'admin/pages';
				}
				else
				{
					$result['success'] = false;
	            	$result['error_message'] = 'Password is wrong,Please check';
				}
			}
			else
			{
				$result['success'] = false;
	            $result['error_message'] = 'Username or Password is wrong,Please check';
			}
			echo json_encode($result);exit;
		}
		$this->load->view('login/login',$data);
	}

/*===========Admin logout functionality==========*/

	public function logout()
	{
		$this->session->unset_userdata('userId');
		$this->session->sess_destroy();
		redirect('admin');
	}
}
