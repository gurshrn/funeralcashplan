<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewEditQuote extends MX_Controller {

	function __construct()
	{
		$this->load->model('viewEditQuote/viewEditQuoteModel','quote');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id = NULL)
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$dob = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['date'];
				$from = new DateTime($dob);
				$to   = new DateTime('today');
				$age = $from->diff($to)->y;
				$param = array(
					'user_id' => $sess_id,
					'title' => $_POST['title'],
					'fname' => $_POST['fname'],
					'sname' => $_POST['sname'],
					'mname' => $_POST['mname'],
					'mphone' => $_POST['mphone'],
					'date_of_birth' => $dob,
					'gender' => $_POST['gender'],
					'email' => $_POST['email'],
					'country_of_residency' => $_POST['country_of_residency'],
					'general_cash_cover' => $_POST['user_general_cash_cover'],
					'age' => $age,
				);
				$response = $this->quote->editUserQuote($param);
				if($response)
				{
					$result['success'] = true;
			    	$result['url'] = base_url('/admin/quote_summary');
			    	$result['msg'] = 'Quote updated successfully.';
				}
				else
				{
					$result['success'] = false;
			    	$result['msg'] = 'Quote not updated successfully.';
				}
				echo json_encode($result);exit;
			}
			$data = headerFooter();
			$data['userQuoteDetail'] = $this->quote->getUserDetail($id);
			$this->load->view('viewEditQuote/view-edit-quote',$data);
		}
	}
}
