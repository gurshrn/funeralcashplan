<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PlanBenefits extends MX_Controller {

	function __construct()
	{
		//$this->load->model('career/careerModel','career');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id)
	{
		$data = headerFooter($id);
		$this->load->view('planBenefits/plan-benefits',$data);
	}
	public function addPlanBenefits()
	{
		$param = array('page_title'=>'plan benefits');

		if (!empty($_FILES['banner_image']['size'])) 
		{
			$count = count($_FILES['banner_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['banner_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['banner_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['banner_image']['type'] = $imagename['type'];
			$_FILES['banner_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['banner_image']['error'] = $imagename['error'];
			$_FILES['banner_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('banner_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['banner_image'] = $imageName;
		}
		if (!empty($_FILES['left_featured_image']['size'])) 
		{
			$count = count($_FILES['left_featured_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['left_featured_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['left_featured_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['left_featured_image']['type'] = $imagename['type'];
			$_FILES['left_featured_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['left_featured_image']['error'] = $imagename['error'];
			$_FILES['left_featured_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('left_featured_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['left_featured_image'] = $imageName;
		}
		if (!empty($_FILES['background_image']['size'])) 
		{
			$count = count($_FILES['background_image']['size']);
			$path = 'assets/upload/pages/';
			$imagename = $_FILES['background_image'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['background_image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['background_image']['type'] = $imagename['type'];
			$_FILES['background_image']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['background_image']['error'] = $imagename['error'];
			$_FILES['background_image']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('background_image');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$_POST['background_image'] = $imageName;
		}
		$_POST['features'] = json_encode($_POST['group']);
		unset($_POST['group']);
		$response = addPage($param,$_POST);
		if($response)
		{
			$response = addPageDetail($_POST);
			$result['success'] = true;
	    	$result['url'] = base_url('/admin/pages');
	    	$result['msg'] = 'Updated successfully.';
		}
		else
		{
			$result['success'] = false;
	    	$result['msg'] = 'Error while updated.';
		}
		echo json_encode($result);exit;
	}
}
