<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {

	function __construct()
	{
		$this->load->model('pages/pagesModel','page');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$sess_id = $this->session->userdata('userId');
		if(empty($sess_id))
	   	{
	        redirect(base_url());
		}
		else
		{
			$data['header'] = Modules::run('admin/common/header/index');
			$data['sidebar'] = Modules::run('admin/common/sidebar/index');
			$data['footer'] = Modules::run('admin/common/footer/index');
			$data['pages'] = $this->page->getPages();
			$this->load->view('pages/pages',$data);
		}
	}
	
}
