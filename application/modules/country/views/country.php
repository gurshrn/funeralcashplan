<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');  
    if(!empty($countryDetail))
    {
        foreach($countryDetail as $key=>$val)
        {
            $country[$val->country_meta_key] = $val->country_meta_value;
            $countryName = $val->country;
        }
       
    }
    $countryOrigin = getCountryOrigin();
    $countryResidence = getCountryResidence();
    $insurancePlan = getInsurancePlan();
?> 
    <section class="banner-wrapper country-banner" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$country['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <div class="banner_left">
                    <h1><?php echo $country['banner_title'];?></h1>
                    <h5><?php echo $country['banner_sub_title'];?></h5>
                    <p><?php echo $country['banner_description'];?></p>
                    <ul>
                        <li>
                            <a href="#">
                                <img src="<?php echo base_url().'assets/upload/pages/'.$country['banner_logo'];?>" alt="logo" />
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('register');?>" class="theme-btn">Get A Quote</a>
                        </li>
                    </ul>
                </div>
                <div class="banner_right" style="background-image: url('assets/images/form-bg.png');">
                    <h3>Global Risk Solutions, Locally!</h3>
                    <form>
                        <div class="form-group">
                            <select name="country_origin" class="country_origin_select">
                                <option value="">Select county of Origin</option>
                                <?php 
                                    if(count($countryOrigin) > 0)
                                    {
                                        foreach($countryOrigin as $origin)
                                        {
                                            echo '<option value="'.$origin->id.'">'.$origin->country.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group continent_residence_select" style="display:none;">
                            <select name="country_residence" class="continent_residence_option">
                                <option value="">Select Continent of Residence</option>
                                <?php 
                                    if(count($countryResidence) > 0)
                                    {
                                        foreach($countryResidence as $res)
                                        {
                                            echo '<option value="'.$res->id.'">'.$res->country_residence    .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group country_residence_select" style="display:none;">
                            <select name="country_residence" class="country_residence_option">
                                <option value="">Select county of Residence</option>
                                <?php 
                                    if(count($countryResidence) > 0)
                                    {
                                        foreach($countryResidence as $res)
                                        {
                                            echo '<option value="'.$res->id.'">'.$res->country_residence    .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group insurance_plan_select" style="display:none;">
                           <select name="insurance_plan" class="insurance_plan_option">
                                <option value="">Select Insurance Plan</option>
                                <?php 
                                    if(count($insurancePlan) > 0)
                                    {
                                        foreach($insurancePlan as $plan)
                                        {
                                            echo '<option value="'.$plan->id.'">'.$plan->insurance_plan .'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="button-wrap quote_select_btn" style="display:none;">
                            <button type="submit" value="submit">Get A Quote</button> 
                        </div>
                    </form>
                </div>
            </div>
            <!-- banner-content -->
        </div>
        <!-- container -->
    </section>
    <!-- banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $countryName;?></li>
                    </ol>
                </nav>
            </div>
            <!-- container -->
        </div>
        <!-- breadcrumb-wrap -->
        <section class="country-wrapper section">
        <!-- <section class="country-wrapper section" style="background-image: url('assets/images/country-bg.jpg');"> -->
            <div class="container">
                <div class="country-section">
                    <figure>
                        <img src="<?php echo base_url().'/assets/upload/pages/'.$country['country_image'];?>" alt="flag" />
                    </figure>
                    <h4><?php echo $country['country_title'];?></h4>
                    <p><?php echo $country['country_description'];?></p>
                </div>
                <!-- country-section -->
            </div>
            <!-- container -->
        </section>
<?php echo Modules::run('common/footer/index'); ?> 
       