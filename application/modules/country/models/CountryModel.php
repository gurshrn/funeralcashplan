<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class CountryModel extends CI_Model {

		public function __construct() {

			parent::__construct();
		}

		public function getCountryDetailById($countryId)
		{
			$this->db->select('df_country_origin.country,df_country_detail.*');
			$this->db->from('df_country_detail');
			$this->db->join('df_country_origin','df_country_origin.id = df_country_detail.country_id','left');
			$this->db->where('df_country_detail.country_id',$countryId);
			$query = $this->db->get();
		    $result = $query->result();
		    return $result;
		}
	}
?>

	