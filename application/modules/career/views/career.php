<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
    $pageDetail = getPagesInfo('careers');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    }
?>
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/career/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Careers</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- breadcrumb-wrap -->
        <section class="careers-wrapper section">
            <div class="container">
                <div class="vacancy-wrap">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-12">
                            <div class="left_sec">
                               <div class="content">
                                    <h4><?php echo $pageDetails['block1_title'];?></h4> 
                                    <p><?php echo $pageDetails['block1_description'];?></p>
                               </div>
                               <div class="content">
                                    <h4><?php echo $pageDetails['block2_title'];?></h4>
                                    <ul>
                                        <li>
                                            <span>Sales Advisor, UK</span>
                                            <a href="#" class="theme-btn">Apply Now</a>
                                        </li>
                                        <li>
                                            <span>Business Development Officer, South Africa</span>
                                            <a href="#" class="theme-btn">Apply Now</a>
                                        </li>
                                        <li>
                                            <span>Customer Advisor, UK</span>
                                            <a href="#" class="theme-btn">Apply Now</a>
                                        </li>
                                    </ul>
                               </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-12">
                            <div class="right_sec">
                                <figure>
                                    <img src="<?php echo base_url().'assets/upload/career/'.$pageDetails['main_image'];?>" alt="featured-thumb" />
                                </figure>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div>
                <!-- vacancy-wrap -->
                <div class="apply-form-wrapper">
                    <div class="title-sec">
                        <h4><?php echo $pageDetails['career_form_title'];?></h4>
                    </div>
                    <form id="career_form" method="post" action="<?php echo base_url('/add_career_form');?>">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap alphabets" name="fname" type="text" value="" placeholder="First Name*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control firstCap alphabets" name="lname" type="text" value="" placeholder="Last Name*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control email" name="email" type="email" value="" placeholder="Your Email*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <select name="position">
                                        <option value="">Position*</option>
                                        <option value="1">Test1</option>
                                        <option value="2">Test2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control onlyvalidNumber careerTelPhone" type="tel" name="mnumber" id="telephone1" value="" placeholder="Mobile Number*" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" name="country_code" type="text" value="" placeholder="Country Code*" />
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" placeholder="Message*" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12 upload-image">
                                <div class="form-group relative">
                                    <input type="text" id="uploadFile" placeholder="Attach Resume*(accept doc|docx|pdf)">
                                    <input type="file" name="resume">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="button-wrap">
                                    <button class="submit-button" type="submit" value="submit">submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- apply-form-wrapper -->
            </div>
            <!-- container -->
        </section>
        
<?php echo Modules::run('common/footer/index'); ?>

<script>
    var careerTelPhone = document.querySelector(".careerTelPhone");
    window.intlTelInput(careerTelPhone);
</script>