<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends MX_Controller {

	function __construct()
	{
		$this->load->model('careerModel','career');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('career');
	}

/*======Add career form details=====*/
	public function addCareerForm()
	{
		$param = $_POST;
		if (!empty($_FILES['resume']['size'])) 
		{
			$count = count($_FILES['resume']['size']);
			$path = 'assets/upload/career/';
			$imagename = $_FILES['resume'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['resume']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['resume']['type'] = $imagename['type'];
			$_FILES['resume']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['resume']['error'] = $imagename['error'];
			$_FILES['resume']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$this->load->library('upload', $config);
			$this->upload->do_upload('resume');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$imageName = $name_array;
			$param['resume'] = $imageName;
		}
		$response = $this->career->addCareerFormDetail($param);
		if($response)
		{
			$result['success'] = true;
	    	$result['url'] = base_url('/career');
	    	$result['msg'] = 'Submit successfully.';
		}
		else
		{
			$result['success'] = false;
	    	$result['msg'] = 'Error while submit form.';
		}
		echo json_encode($result);exit;
	}
}
