<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');  
    $pageDetail = getPagesInfo('contact us');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    } 
?> 
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/contact-us/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="contactus-wrapper">
            <div class="contactus-sec section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-12">
                            <div class="left_sec">
                                <h4><?php echo $pageDetails['title'];?></h4>
                                <div class="info-sec">
                                    <h5>United Kingdom</h5>
                                    <ul>
                                        <li>
                                            <figure>
                                                <img src="assets/images/map-marker.png" alt="icon" />
                                            </figure>
                                            <address>4th Floor Spaces Crossway, 156 Great Charles Street Queensway, Birmingham,  B3 3HN</address>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/phone.png" alt="icon" />
                                            </figure>
                                            <a href="tel:441212951116">+44 121 295 1116</a>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/whatsap.png" alt="icon" />
                                            </figure>
                                            <a href="tel:447703838304">+44 770 3838 304</a>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/envelop.png" alt="icon" />
                                            </figure>
                                            <a href="mailto:claims@diasporainsurance.com">claims@diasporainsurance.com</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-sec">
                                    <h5>South Africa</h5>
                                    <ul>
                                        <li>
                                            <figure>
                                                <img src="assets/images/map-marker.png" alt="icon" />
                                            </figure>
                                            <address>2nd Floor, West Tower, Nelson Mandela Square Cnr Maude and 5th Street, Sandton, 2196</address>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/phone.png" alt="icon" />
                                            </figure>
                                            <a href="tel:27115495606">+27 115 495 606</a>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/whatsap.png" alt="icon" />
                                            </figure>
                                            <a href="tel:27730702701">+27 730 702 701</a>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="assets/images/envelop.png" alt="icon" />
                                            </figure>
                                            <a href="mailto:claims@diasporainsurance.com">claims@diasporainsurance.com</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-12">
                            <div class="right_sec custom-form-wrapper">
                                <div class="title-sec">
                                    <h4><?php echo $pageDetails['form_title'];?></h4>
                                </div>
                                <form id="add_contact_us_form" method="POST" action="<?php echo base_url('add_contact_us_form_detail');?>">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <input class="form-control firstCap" name="fname" type="text" value="" placeholder="First Name*" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <input class="form-control firstCap" name="lname" type="text" value="" placeholder="Last Name*" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <input class="form-control" type="email" name="email" value="" placeholder="Email*" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <input class="form-control contactUsTel" name="pnumber" type="tel" value="" placeholder="Tel/Mob*" />
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" placeholder="Message*" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="button-wrap">
                                                <div class="captcha">
                                                    <img src="assets/images/captcha.jpg" alt="captcha" />
                                                </div>
                                                <button type="submit" value="submit">submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map-sec">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2429.8808924224086!2d-1.9077416840020516!3d52.481292246849215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4870bd24745361b3%3A0x9b2a955928bd1cf0!2sSpaces+-+Birmingham%2C+Crossway!5e0!3m2!1sen!2sin!4v1559280168068!5m2!1sen!2sin" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');  ;?>

<script>
    var contactUsTel = document.querySelector(".contactUsTel");
    window.intlTelInput(contactUsTel);
</script>