	<!-- careers-wrapper -->
        <section class="support-wrap">
            <div class="container">
                <h4>We are here to support You:</h4>
                <ul>
                    <li>
                        <a href="<?php echo base_url('register');?>">Get A Quote</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('contactUs');?>">Call Me Back</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('claim');?>">Make A Claim</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="popup-close" data-popup-open="SignIn">Existing Clients Area</a>
                    </li>
                </ul>
            </div>
            <!-- container -->
        </section>
        <!-- support-wrap -->
    </div>
    <footer class="footer-wrapper">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="footer-sec">
                            <h5>Diaspora Insurance</h5>
                            <ul class="nav-menu">
                                <li>
                                    <a href="<?php echo base_url('careers');?>">Careers</a>
                                </li>
                                <li>
                                    <a href="#">About Our Plans</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('contactUs');?>">Contact Us</a>
                                </li>
                                <li>
                                    <a href="country.html">Country Of Origin</a>
                                </li>
                                <li>
                                    <a href="#">Choose Plan</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('register');?>">Get A Quote</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('faq');?>">F.A.Q.s</a>
                                </li>
                                <li>
                                    <a href="#">Download App</a>
                                </li>
                            </ul>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="footer-sec">
                            <h5>Compliance & Data Protection</h5>
                            <ul class="nav-menu">
                                <li>
                                    <a href="#">FCA </a>
                                </li>
                                <li>
                                    <a href="#">FSCA</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="#">Useful Documents</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('privacyPolicy');?>">Privacy policy</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('cookies');?>">Cookie policy</a>
                                </li>
                                <li>
                                    <a href="#">Site map</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('socialMedia');?>">Social Media</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="footer-sec">
                            <h5>Follow Us</h5>
                            <ul class="follow-link">
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow1.png';?>" alt="follow" />
                                    </figure>
                                    <a href="<?php echo base_url('blog');?>">Diaspora Insurance Blog</a>
                                </li>
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow2.png';?>" alt="follow" />
                                    </figure>
                                    <a href="https://twitter.com/diasporainsurance">Twitter</a>
                                </li>
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow3.png';?>" alt="follow" />
                                    </figure>
                                    <a href="https://www.facebook.com/diasporainsurance">Facebook</a>
                                </li>
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow4.png';?>" alt="follow" />
                                    </figure>
                                    <a href="https://www.youtube.com/user/diasporainsurance">YouTube</a>
                                </li>
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow5.png';?>" alt="follow" />
                                    </figure>
                                    <a href="#">Instagram</a>
                                </li>
                                <li>
                                    <figure>
                                        <img src="<?php echo base_url().'assets/frontend/images/follow6.png';?>" alt="follow" />
                                    </figure>
                                    <a href="https://www.linkedin.com/company/diaspora-insurance">Linkindin</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="footer-sec">
                            <h5>Choose Product</h5>
                            <div class="footer-logo">
                                <a href="#">
                                    <?php 
                                        $footerLogo = getOptionDetail('footer_logo');
                                    ?>
                                    <img src="<?php echo base_url().'assets/upload/options/'.$footerLogo->meta_value;?>" alt="logo" />
                                </a>
                            </div>
                            <!-- footer-logo -->
                            <h5>Event Organisers</h5>
                            <div class="invite-wrap">
                                <h6>Your Event</h6>
                                <a href="inviteus.html" class="theme-btn">invite Us</a>
                            </div>
                            <h5>Partners & Affiliates</h5>
                            <div class="login-wrap">
                                <ul>
                                    <li>
                                        <a href="#" class="theme-btn">Sign Up</a>
                                    </li>
                                    <li>
                                        <a href="#" class="theme-btn">Login</a>
                                    </li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-content">
                    <span class="copyright">Copyright &copy; 2019 Diaspora Funeral Cash Plan.</span>
                </div>
            </div><!-- footer-bottom -->
        </div><!-- container -->
    </footer><!-- footer-wrapper -->

      <!-- Modal -->
    <div class="popup" data-popup="SignIn">
        <div class="popup-inner">
            <div class="popup-inner-elem">
                <div class="popup-title">
                    <h3>log in</h3>
                </div>
                <div class="content">
                    <form id="frontend_login" method="POST" action="<?php echo base_url('register/userLogin');?>">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" id="login_email" name="email" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="login_password" name="password" class="form-control" placeholder="">
                        </div>
                        <div class="account-wrap">
                            <span>Don't have an account? Please <a href="<?php echo base_url('register');?>">Sign up</a> here</span>
                        </div>
                        <div class="forgot-password">
                            <a href="#" data-popup-open="ForgotPass" data-popup-close="SignIn">Forgot password?</a>
                        </div>
                        <div class="button-sec">
                            <button type="submit" class="button">Log in</button>
                        </div>
                    </form>
                </div> 
                <a class="popup-close" data-popup-close="SignIn" href="#">
                    <svg width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                        <g>
                            <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    
    <div class="popup" data-popup="ForgotPass">
        <div class="popup-inner">
            <div class="popup-inner-elem">
                <div class="popup-title">
                    <h3>Forgot Password?</h3>
                </div>
                <div class="content">
                    <form>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" id="email" class="form-control" placeholder="">
                        </div>
                        <div class="button-sec">
                            <button type="submit" class="button">Submit</button>
                        </div>
                    </form>
                </div>
                <a class="popup-close" data-popup-close="ForgotPass" href="#">
                    <svg width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                        <g>
                            <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url().'assets/frontend/js/jquery.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/aos.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/wow.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/popper.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/owl.carousel.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/intlTelInput.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/moment.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/combodate.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/jquery.validate.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/form-validation-script.js';?>"></script>
    <script src="<?php echo base_url().'assets/plugin/countrycode/build/js/intlTelInput.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/toastr.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/toastr.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/custom.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/common.js';?>"></script>
    <script src="<?php echo base_url().'assets/frontend/js/quote.js';?>"></script>
    
</body>
</html>