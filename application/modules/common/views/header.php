<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>DFCP | Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/frontend/images/favicon.png';?>" sizes="32x32" type="image/x-icon">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/css/toastr.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/plugin/countrycode/build/css/intlTelInput.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/css/style.css';?>">
</head>

<body>
    <header class="header">
        <div class="header_top">
            <div class="container">
                <ul>
                    <li>
                        <div class="links">
                            <?php 
                                $phone_number = getOptionDetail('phone_number');
                                $decodePhone = json_decode($phone_number->meta_value); 
                            ?>
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/icon1.png';?>" alt="icon" />
                            </figure>
                            <a href="tel:<?php echo $decodePhone[0];?>"><?php echo $decodePhone[0];?></a>
                            <span>or</span>
                            <a href="tel:<?php echo $decodePhone[1];?>"><?php echo $decodePhone[1];?></a>
                        </div>
                    </li>
                    <li>
                        <div class="links">
                            <?php 
                                $whatsAppNum = getOptionDetail('whats_app_number');
                                $decodeWhatsAppNum = json_decode($whatsAppNum->meta_value);  
                            ?>
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/icon2.png';?>" alt="icon" />
                            </figure>
                            <a href="tel:<?php echo $decodeWhatsAppNum[0];?>"><?php echo $decodeWhatsAppNum[0];?></a>
                            <span>or</span>
                            <a href="tel:<?php echo $decodeWhatsAppNum[0];?>"><?php echo $decodeWhatsAppNum[1];?></a>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                <figure style="background-image: url('<?php echo base_url().'assets/frontend/images/icon3.png';?>');"></figure>
                                Global – English
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Global – English</a>
                                <a class="dropdown-item" href="#">Global – English</a>
                                <a class="dropdown-item" href="#">Global – English</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="links">
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/icon4.png';?>" alt="icon" />
                            </figure>
                            <a href="#" class="popup-close" data-popup-open="SignIn">
                                login
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="links">
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/icon5.png';?>" alt="icon" />
                            </figure>
                            <a href="<?php echo base_url('register');?>">
                                sign up
                            </a>
                        </div>
                    </li>
                    <li class="relative">
                        <a href="#" class="search">
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/icon6.png';?>" alt="icon" />
                            </figure>
                        </a>
                        <form class="search-form" style="display: none;">
                            <input type="search" placeholder="Search">
                        </form>
                    </li>
                </ul>
            </div>
        </div>