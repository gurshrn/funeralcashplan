<?php 
	$getCountry = getAllCountryOrigin(); 
?>
<!-- header_top -->
	<div class="header_bottom">
		<nav class="navbar navbar-expand-md">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="<?php echo base_url();?>">
						<?php 
                            $headerLogo = getOptionDetail('header_logo');
                        ?>
						<img src="<?php echo base_url().'assets/upload/options/'.$headerLogo->meta_value;?>" alt="logo" />
					</a>
				</div>
				<div class="collapse navbar-collapse" id="collapsibleNavbar">
					<div class="navbar-sec">
						<ul class="nav navbar-nav ml-auto" id="menu-primary-menu">
							<li class="nav-item current-menu-item">
								<a class="nav-link" href="<?php echo base_url();?>">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									About Us
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</a>
								<ul>
									<li>
										<a class="nav-link" href="<?php echo base_url('diasporaInsurance');?>">Diaspora Insurance</a>
									</li>
									<li>
										<a class="nav-link" href="<?php echo base_url('career');?>">Careers</a>
									</li>
									<li>
										<a class="nav-link" href="<?php echo base_url('inviteUs');?>">Your Event - Invite Us</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Select Plan
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</a>
								<ul>
									<li>
										<a class="nav-link" href="<?php echo base_url('diasporaFuneralCashPlan');?>">Diaspora Funeral Cash Plan</a>
										<ul>

											<li>
												<a class="nav-link" href="<?php echo base_url('planBenefits');?>">Plan Benefits</a>
											</li>
											<li>
												<a class="nav-link" href="<?php echo base_url('planFeatures');?>">Plan Features</a>
											</li>

										</ul>
									</li>
									<li>
										<a class="nav-link" href="<?php echo base_url('medicalInsurance');?>">Diaspora Medical Insurance</a>
									</li>
								</ul>
							</li>
							<?php if(!empty($getCountry)){ ?>
								<li class="nav-item">
									<a class="nav-link" href="#">
										Country Of Origin
										<i class="fa fa-angle-down" aria-hidden="true"></i>
									</a>
									<ul>
										<?php foreach($getCountry as $val){ ?>
											<li>
												<a class="nav-link" href="<?php echo base_url('country-detail/'.$val->id);?>"><?php echo ucfirst($val->country);?></a>
											</li>
										<?php } ?>
									</ul>
								</li>
							<?php } ?>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('contactUs');?>">Contact Us</a>
							</li>
						</ul>
						<div class="call-me-back">
							<?php 
								$callMeBtnText = getOptionDetail('call_me_btn_text');
								$callMeBtnLink = getOptionDetail('call_me_btn_link');
							?>
							<a href="<?php echo base_url($callMeBtnLink->meta_value);?>"><?php echo $callMeBtnText->meta_value;?></a>
						</div>
					</div>
				</div>
			</div>
			<!-- container -->
		</nav>
	</div>
</header>