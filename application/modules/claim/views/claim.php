<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
   
?>
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/frontend/images/inner-banner.jpg'?>');">
        <div class="container">
            <div class="banner-content">
                <h1>Make a claim</h1>
            </div>
            <!-- banner-content -->
        </div>
        <!-- container -->
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Make A Claim</li>
                    </ol>
                </nav>
            </div>
            <!-- container -->
        </div>
        <!-- breadcrumb-wrap -->
        <section class="make-claim-wrapper section">
            <div class="container">
                <div class="content-sec">
                    <h4>Claim Process Is Very Simple:</h4>
                    <p>If you've recently lost someone close to you, please accept our condolences. Dealing with a death in the family is one of the hardest things many of us will face.</p>
                    <p>At Diaspora Insurance we understand that a claim is always made at a very emotional and difficult time for the family. That's why we have made our claim process seamlessly easy to guarantee quick financial intervention.</p>
                    <p>Our claims process has been simplified so that we can get your claims payment to you as soon as possible – usually within 24 to 48 hours of us receiving all the required claim support documents and information.</p>
                    <p>Simply download the <a href="#">Claim Form</a> fill it in, sign as required and send it back with all the claim support documents as listed on the form.</p>
                </div>
                <!-- content-sec -->
                <div class="content-sec">
                    <h4>Support is just a call, WhatsApp, text or email away:</h4>
                    <div class="contact-detail">
                        <div class="title">
                            <h3>Diaspora Insurance: Offices and Contact Details</h3>
                        </div>
                        <!-- title -->
                        <div class="info-wrap">
                            <div class="info-sec">
                                <h4>United Kingdom</h4>
                                <ul>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/map-marker.png';?>" alt="icon" />
                                        </figure>
                                        <address>4th Floor Spaces Crossway, 156 Great Charles Street Queensway, Birmingham,  B3 3HN</address>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/phone.png';?>" alt="icon" />
                                        </figure>
                                        <a href="tel:441212951116">+44 121 295 1116</a>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/whatsap.png';?>" alt="icon" />
                                        </figure>
                                        <a href="tel:447703838304">+44 770 3838 304</a>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/envelop.png';?>" alt="icon" />
                                        </figure>
                                        <a href="mailto:claims@diasporainsurance.com">claims@diasporainsurance.com</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="info-sec">
                                <h4>South Africa</h4>
                                <ul>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/map-marker.png';?>" alt="icon" />
                                        </figure>
                                        <address>2nd Floor, West Tower, Nelson Mandela Square Cnr Maude and 5th Street, Sandton, 2196</address>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/phone.png';?>" alt="icon" />
                                        </figure>
                                        <a href="tel:27115495606">+27 115 495 606</a>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/whatsap.png';?>" alt="icon" />
                                        </figure>
                                        <a href="tel:27730702701">+27 730 702 701</a>
                                    </li>
                                    <li>
                                        <figure>
                                            <img src="<?php echo base_url().'assets/frontend/images/envelop.png';?>" alt="icon" />
                                        </figure>
                                        <a href="mailto:claims@diasporainsurance.com">claims@diasporainsurance.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- contact-detail -->
                </div>
                <!-- content-sec -->
            </div>
            <!-- container -->
        </section>
<?php echo Modules::run('common/footer/index'); ?>

