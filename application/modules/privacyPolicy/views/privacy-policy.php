<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
    $pageDetail = getPagesInfo('careers');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    }
?>
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/career/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1>Privacy Policy</h1>
            </div>
        </div>
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
            </div>
        </div>
       
<?php echo Modules::run('common/footer/index'); ?>
