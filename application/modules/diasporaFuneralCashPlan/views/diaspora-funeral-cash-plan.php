<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');  
?> 
    <section class="banner-wrapper diaspora-banner" style="background-image: url('<?php echo base_url().'assets/frontend/images/banner.jpg';?>');">
        <div class="container">
            <div class="banner-content">
                <div class="banner_left">
                    <div class="banner-logo">
                        <a href="#">
                            <img src="<?php echo base_url().'assets/frontend/images/logo1.png';?>" alt="logo" />
                        </a>
                    </div>
                    <div class="banner-list">
                        <ul>
                            <li>
                                Guaranteed Acceptance, No Medicals At All
                            </li>
                            <li>
                                Up To US$20, 000 Cash Cover For Life
                            </li>
                            <li>
                                Immediate US$ Cash Pay Out On Claim
                            </li>
                            <li>
                                Worldwide Protection Without Boarders
                            </li>
                            <li>
                                Guaranteed US$ Cover For Life,
                            </li>
                        </ul>
                    </div>
                    <div class="more-btn-wrap">
                        <a href="#" class="theme-btn">More</a>
                    </div>
                </div>
                <div class="banner_right" style="background-image: url('<?php echo base_url().'assets/frontend/images/form-bg.png';?>');">
                    <h3>Get Indicative Quotes Based on desired cover amount</h3>
                    <form id="cash_plan_form" method="POST" action="<?php echo base_url('add_cash_plan_form');?>">
                        <div class="form-group">
                            <select>
                                <option>Select Currency Of Cover</option>
                                <option>Select Currency Of Cover</option>
                            </select>
                        </div>
                        <div class="range-wrap">
                            <div class="label-sec">
                                <span>$1000</span>
                                <span>$20000</span>
                            </div>
                            <input id="ex2" type="text" class="span2" data-slider-min="1" data-slider-max="20"/>
                            <span class="amount">Amount you require: <cite>$9,500</cite></span>
                        </div>
                        <div class="detail-sec">
                            <div class="form-group">
                                <label>Title<span>*</span></label>
                                <select>
                                    <option>Mr</option>
                                    <option>Mr</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>First Name<span>*</span></label>
                                <input class="form-control" name="fname" type="text" value="" />
                            </div>
                            <div class="form-group">
                                <label>Surname<span>*</span></label>
                                <input class="form-control" name="snmame" type="text" value="" />
                            </div>
                        </div>
                        <div class="info-wrap">
                            <div class="form-group">
                                <label>Telephone<span>*</span></label>
                                <input class="form-control tel_phone" name="telphone" type="tel" value="" />
                            </div>
                            <div class="form-group">
                                <label>Email<span>*</span></label>
                                <input class="form-control" type="email" name="email" value="" />
                            </div>
                        </div>
                        <div class="date-wrapper">
                            <label>Date of Birth<span>*</span></label>
                            <div class="date-wrap">
                                <div class="form-group">
                                    <select name="date">
                                        <option>DD</option>
                                        <option>DD</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="month">
                                        <option>MM</option>
                                        <option>MM</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="year">
                                        <option>YYYY</option>
                                        <option>YYYY</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <p>Mandatory fields. Clicking this button means you agree we can use your details to email and call you about our products in accordance with our <a href="#">privacy policy</a>.</p>
                        <div class="button-wrap">
                            <button type="submit" value="submit">Get A Quote</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="main">
        <section class="cover-yourself-wrap section">
            <div class="container">
                <div class="content">
                    <p>Cover Yourself, Cover your Loved Ones! Premiums from as little as <br> US$0.32 per month + Guaranteed FREE cover in future.</p>
                </div>
                <ul class="list">
                    <li>
                        <a href="<?php echo base_url('planFeatures');?>" class="theme-btn">Plan Features</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('planBenefits');?>" class="theme-btn">Plan Benefits</a>
                    </li>
                    <li>
                        <a href="endorsement-and-testimonials.html" class="theme-btn">Endorsements & Testimonials</a>
                    </li>
                    <li>
                        <a href="news-list.html" class="theme-btn">In the News</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('contact_us');?>" class="theme-btn">Call Me Back</a>
                    </li>
                </ul>
                <ul class="contact-detail">
                    <?php 
                        $whatsAppNum = getOptionDetail('whats_app_number');
                        $decodeWhatsAppNum = json_decode($whatsAppNum->meta_value);  
                    ?>
                    <li>
                        <div class="links">
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/whatsap1.png';?>" alt="icon" />
                            </figure>
                            <a href="tel:<?php echo $decodeWhatsAppNum[0];?>"><?php echo $decodeWhatsAppNum[0];?></a>
                            <span>or</span>
                            <a href="tel:<?php echo $decodeWhatsAppNum[1];?>"><?php echo $decodeWhatsAppNum[1];?></a>
                        </div>
                    </li>
                    <?php 
                        $phone_number = getOptionDetail('phone_number');
                        $decodePhone = json_decode($phone_number->meta_value); 
                    ?>
                    <li>
                        <div class="links">
                            <figure>
                                <img src="<?php echo base_url().'assets/frontend/images/tel.png';?>" alt="icon" />
                            </figure>
                            <a href="tel:<?php echo $decodePhone[0];?>"><?php echo $decodePhone[0];?></a>
                            <span>or</span>
                            <a href="tel:<?php echo $decodePhone[1];?>"><?php echo $decodePhone[1];?></a>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="uniquely-wrap">
            <div class="container">
                <div class="content-sec">
                    <h4>The uniquely designed Diaspora Funeral Cash Plan is underwritten by leading insurers and global re-insurers:</h4>
                    <div class="listing-wrap">
                        <ul>
                            <li>Dignified send-off, guaranteed</li>
                            <li>Peace of Mind, guaranteed</li>
                            <li>Celebrated life, guaranteed</li>
                            <li>Your Dignity, protected</li>
                            <li>Bereaving & Begging, never again, <a href="#">more...</a></li>
                        </ul>
                    </div>
                    <h5>Get A Quote</h5>
                    <div class="select-wrap">
                        <ul>
                            <li>
                                <figure>
                                    <a href="#">
                                        <img src="<?php echo base_url().'assets/frontend/images/footer-logo.png';?>" alt="featured-thumb" />
                                    </a>
                                </figure>
                            </li>
                            <li>
                                <form>
                                    <div class="form-group">
                                        <select>
                                            <option>Select Country of Origin</option>
                                            <option>Select Country of Origin</option>
                                        </select>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div class="button-wrap">
                        <ul>
                            <li>
                                <a href="<?php echo base_url('register');?>" class="theme-btn">Get a Quote</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="theme-btn popup-close" data-popup-open="SignIn">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="featured-thumb" style="background-image: url('assets/images/unique.jpg');"></div>
        </section>
        <section class="plan-wrapper section">
            <div class="container">
                <div class="plan-section">
                    <ul class="nav nav-tabs">
                        <li>
                            <a data-toggle="tab" href="#singleplan">Single Plan</a>
                        </li>
                        <li>
                            <a class="active" data-toggle="tab" href="#familyplan">Family Plan</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#groupplan">Group Plan</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="singleplan" class="tab-pane fade">
                            <p>By far the most popular Plan, it allows you to cover not just yourself but your loved ones as well. Under the Family Plan you can <br> cover yourself, spouse, children, parents and/or parents in law.</p>
                            <a href="<?php echo base_url('register');?>" class="theme-btn">Get a Quote</a>
                        </div>
                        <div id="familyplan" class="tab-pane fade active show">
                            <p>By far the most popular Plan, it allows you to cover not just yourself but your loved ones as well. Under the Family Plan you can <br> cover yourself, spouse, children, parents and/or parents in law.</p>
                            <a href="<?php echo base_url('register');?>" class="theme-btn">Get a Quote</a>
                        </div>
                        <div id="groupplan" class="tab-pane fade">
                            <p>By far the most popular Plan, it allows you to cover not just yourself but your loved ones as well. Under the Family Plan you can <br> cover yourself, spouse, children, parents and/or parents in law.</p>
                            <a href="<?php echo base_url('register');?>" class="theme-btn">Get a Quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');  ;?>

<script>
    var tel_phone = document.querySelector(".tel_phone");
    window.intlTelInput(tel_phone);
</script>