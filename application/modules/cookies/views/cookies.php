<?php 
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
?>
    <section class="inner-banner-wrapper" style="background-image: url('');">
        <div class="container">
            <div class="banner-content">
                <h1>Cookies</h1>
            </div>
        </div>
    </section>
    <!-- inner-banner-wrapper -->
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cookies</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- breadcrumb-wrap -->
        
        
<?php echo Modules::run('common/footer/index'); ?>

<script>
    var careerTelPhone = document.querySelector(".careerTelPhone");
    window.intlTelInput(careerTelPhone);
</script>