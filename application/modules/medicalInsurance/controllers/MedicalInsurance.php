<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MedicalInsurance extends MX_Controller {

	function __construct()
	{
		$this->load->model('medicalInsuranceModel','medical');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('medical-insurance');
	}

/*======Add medical insurance interest form details=====*/

	public function addMedicalInsuranceInterest()
	{
		$param = $_POST;
		$response = $this->medical->addMedicalInsuranceFormDetail($param);
		if($response)
		{
			$result['success'] = true;
	    	$result['url'] = base_url('/medicalInsurance');
	    	$result['msg'] = 'Submit successfully.';
		}
		else
		{
			$result['success'] = false;
	    	$result['msg'] = 'Error while submit form.';
		}
		echo json_encode($result);exit;
	}
}
