<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index'); 
    $pageDetail = getPagesInfo('medical insurance');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    } 
?> 
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/medical-insurance/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Diaspora Medical Insurance</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="diasporamedicalinsurance-wrapper section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-12">
                        <div class="left_sec">
                            <h2><?php echo $pageDetails['title'];?></h2>
                            <?php echo $pageDetails['left_block_description'];?>
                            <div class="figure-wrap">
                                <img src="<?php echo base_url().'assets/upload/medical-insurance/'.$pageDetails['left_featured_image'];?>" alt="featured-thumb" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-12">
                        <div class="right_sec custom-form-wrapper">
                            <div class="title-sec">
                                <h4><?php echo $pageDetails['form_title'];?></h4>
                            </div>
                            <form id="medical_insurance_interest_form" method="POST" action="<?php echo base_url('add_medical_insurance_interest');?>">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="title" placeholder="Title*" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control firstCap" name="fname" type="text" value="" placeholder="First Name*" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control firstCap" name="sname" type="text" value="" placeholder="Surname*" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control medicalTelPhone" type="tel" name="mobile_country_code" value="" placeholder="Country Code  I  Mobile Number*" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <input class="form-control" type="email" name="email" placeholder="Email*" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="form-group">
                                            <select name="country">
                                                <option value="1">Nigeria*</option>
                                                <option value="2">Nigeria</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="button-wrap">
                                            <div class="captcha">
                                                <img src="assets/images/captcha.jpg" alt="captcha" />
                                            </div>
                                            <button type="submit" value="submit">submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');  ;?>

<script>
    var medicalTelPhone = document.querySelector(".medicalTelPhone");
    window.intlTelInput(medicalTelPhone);
</script>