<?php
    echo Modules::run('common/header/index');
    echo Modules::run('common/sidebar/index');
    $pageDetail = getPagesInfo('plan features');
    foreach($pageDetail as $val)
    {
        $pageDetails[$val->page_meta_key] = $val->page_meta_value;
    }  
?> 
    <section class="inner-banner-wrapper" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$pageDetails['banner_image'];?>');">
        <div class="container">
            <div class="banner-content">
                <h1><?php echo $pageDetails['banner_text'];?></h1>
            </div>
        </div>
    </section>
    <div class="main">
        <div class="breadcrumb-wrap">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Plan Features</li>
                    </ol>
                </nav>
            </div>
        </div>
        <section class="plan-wrapper section">
            <div class="container">
                <div class="plan-section" style="background-image: url('<?php echo base_url().'assets/upload/pages/'.$pageDetails['background_image'];?>');">
                    <div class="title">
                        <h4><?php echo $pageDetails['title'];?></h4>
                    </div>
                    <div class="plans">
                        <div class="left_sec">
                            <figure>
                                <img src="<?php echo base_url().'assets/upload/pages/'.$pageDetails['left_featured_image'];?>" alt="featured-thumb" />
                            </figure>
                        </div>
                        <div class="right_sec">
                            <div class="list-wrap">
                                <ul>
                                    <?php 
                                        $features = json_decode($pageDetails['features']);
                                        if(!empty($features))
                                        {
                                            foreach($features as $val)
                                            {
                                                echo '<li>'.$val->features.'</li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <div class="button-wrap">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('planBenefits');?>" class="theme-btn">Plan Benefits</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('register');?>" class="theme-btn">Get A Quote</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php echo Modules::run('common/footer/index');  ;?>